<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('projects')->insert([
            'title' => 'Orci varius natoque penatibus',
            'description' => 'Nam dapibus eros feugiat turpis aliquet, eget sagittis massa placerat. Curabitur ornare nulla dolor, ut ullamcorper magna mollis ac. Cras aliquam iaculis egestas. Pellentesque rhoncus tortor quis magna fermentum convallis. Nunc nec consectetur dolor. Ut bibendum turpis mauris, sit amet sagittis elit ultricies et. Curabitur maximus ut orci id rutrum. Nunc ullamcorper viverra consequat. Proin tristique at lorem sed consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

In hac habitasse platea dictumst. Sed ac sodales lacus, ultrices commodo felis. Morbi diam magna, varius et lacus nec, dapibus molestie nibh. Cras semper ac massa sed suscipit. Sed id tortor ac purus consequat sollicitudin. Nam vitae eros et urna tempor iaculis at ac nunc. Suspendisse sed metus volutpat, facilisis turpis ut, ullamcorper nisi. Duis facilisis eros ut maximus egestas. Sed sit amet quam ultricies, pretium nulla volutpat, auctor metus. Curabitur gravida urna sit amet auctor consectetur. Aenean semper hendrerit elit vitae tristique. Praesent dictum aliquet libero, at hendrerit quam sollicitudin et. Duis fermentum urna at egestas congue. Aenean lectus metus, efficitur sed porttitor finibus, cursus id nulla.

Fusce interdum ac sapien eget viverra. Phasellus gravida placerat tellus quis egestas. Vivamus scelerisque, nisi nec efficitur hendrerit, sem nisi ultrices quam, ac tincidunt arcu mauris non dui. Nam faucibus sit amet purus id efficitur. Aenean aliquam elementum metus, in maximus ante efficitur a. Nullam lobortis, eros a interdum bibendum, dolor sapien efficitur urna, ac mattis enim est sed erat. Nullam feugiat euismod mauris. Phasellus dignissim metus vitae lorem dapibus venenatis. Sed eu cursus diam.',
            'budget' => 1400,
            'deadline' => 'as_soon_as_possible',
            'city_id' => 6,
            'user_id' => 1,
            'project_category_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('projects')->insert([
            'title' => 'Mauris dapibus dignissim accumsan',
            'description' => 'Donec nec pulvinar nunc, vitae ornare velit. Proin pretium id justo vitae finibus. Vestibulum accumsan erat quis gravida consectetur. Nunc luctus dolor a pulvinar sollicitudin. Etiam scelerisque enim non massa pharetra, rhoncus posuere ipsum porttitor. Nullam nunc mauris, lacinia ac dui sit amet, pretium cursus urna. Nullam a enim diam. In et iaculis ipsum, vitae condimentum odio. Integer eros quam, mollis in ullamcorper in, interdum eu elit. Nunc elementum neque tortor, in dictum dui consequat luctus.

Nam nec laoreet ex. In maximus quam et ipsum venenatis, sed pulvinar diam scelerisque. Maecenas aliquam ante pellentesque, consectetur odio id, convallis nisi. Ut et facilisis mi. Duis ultrices libero eu massa scelerisque, a pellentesque eros egestas. Nulla ac mauris eget felis posuere aliquam. Proin aliquam, nunc auctor dapibus tincidunt, risus augue ultricies leo, ac lacinia libero leo at mauris. Donec lobortis enim lacus, nec consequat eros fermentum quis. Nulla nec finibus libero. Nullam laoreet ullamcorper ultricies. Mauris euismod orci ante, sagittis ullamcorper nisl laoreet ac. Donec posuere finibus elit, non porttitor ipsum dignissim a. Nullam vitae sapien enim.

Sed a nunc viverra enim pharetra varius eget a dolor. Donec porttitor finibus purus, vel gravida nulla pharetra in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut libero lectus, venenatis vel facilisis id, euismod eget nisl. Vestibulum leo est, bibendum ut lectus quis, blandit imperdiet leo. Duis feugiat ipsum eget felis convallis, a lacinia tellus condimentum. Etiam ut lacus non dui porttitor porta. Duis id tempor justo. Nunc sit amet mauris vestibulum, aliquet magna sit amet, bibendum lacus. Nullam tincidunt metus vitae tempor venenatis. Mauris dapibus dignissim accumsan. Quisque tincidunt libero a maximus facilisis. Praesent dictum interdum risus, sed sollicitudin justo interdum at. Curabitur felis sapien, accumsan at tincidunt eu, pellentesque a tortor. Quisque dapibus vel quam vitae porttitor. Integer tempor convallis lacus vitae laoreet.',
            'budget' => 700,
            'deadline' => 'as_soon_as_possible',
            'city_id' => 3,
            'user_id' => 1,
            'project_category_id' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('projects')->insert([
            'title' => 'Quisque tincidunt libero a maximus facilisis',
            'description' => 'Sed a nunc viverra enim pharetra varius eget a dolor. Donec porttitor finibus purus, vel gravida nulla pharetra in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut libero lectus, venenatis vel facilisis id, euismod eget nisl. Vestibulum leo est, bibendum ut lectus quis, blandit imperdiet leo. Duis feugiat ipsum eget felis convallis, a lacinia tellus condimentum. Etiam ut lacus non dui porttitor porta. Duis id tempor justo. Nunc sit amet mauris vestibulum, aliquet magna sit amet, bibendum lacus. Nullam tincidunt metus vitae tempor venenatis. Mauris dapibus dignissim accumsan. Quisque tincidunt libero a maximus facilisis. Praesent dictum interdum risus, sed sollicitudin justo interdum at. Curabitur felis sapien, accumsan at tincidunt eu, pellentesque a tortor. Quisque dapibus vel quam vitae porttitor. Integer tempor convallis lacus vitae laoreet.',
            'budget' => 9700,
            'deadline' => 'as_soon_as_possible',
            'city_id' => 3,
            'user_id' => 2,
            'project_category_id' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('projects')->insert([
            'title' => 'Nulla nec finibus libero. Nullam laoreet ullamcorper ultricies.',
            'description' => 'Nam nec laoreet ex. In maximus quam et ipsum venenatis, sed pulvinar diam scelerisque. Maecenas aliquam ante pellentesque, consectetur odio id, convallis nisi. Ut et facilisis mi. Duis ultrices libero eu massa scelerisque, a pellentesque eros egestas. Nulla ac mauris eget felis posuere aliquam. Proin aliquam, nunc auctor dapibus tincidunt, risus augue ultricies leo, ac lacinia libero leo at mauris. Donec lobortis enim lacus, nec consequat eros fermentum quis. Nulla nec finibus libero. Nullam laoreet ullamcorper ultricies. Mauris euismod orci ante, sagittis ullamcorper nisl laoreet ac. Donec posuere finibus elit, non porttitor ipsum dignissim a. Nullam vitae sapien enim.',
            'budget' => 100,
            'deadline' => 'as_soon_as_possible',
            'city_id' => 3,
            'user_id' => 2,
            'project_category_id' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('projects')->insert([
            'title' => 'Praesent a eleifend augue. Mauris in pellentesque turpis.',
            'description' => 'Aliquam at enim ex. Nam sed pharetra erat. Vivamus sit amet ultricies dolor. Sed eu gravida odio. Nullam feugiat nulla sed dictum feugiat. Praesent lorem dolor, malesuada in ultricies vel, maximus sit amet libero. Nunc ullamcorper neque iaculis, viverra sapien vitae, sollicitudin nisl. Donec condimentum posuere nisi, ut consectetur mi feugiat vitae.',
            'budget' => 800,
            'deadline' => 'as_soon_as_possible',
            'city_id' => 10,
            'user_id' => 3,
            'project_category_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('projects')->insert([
            'title' => 'Curabitur tristique dignissim dolor nec finibus.',
            'description' => 'In arcu massa, pretium sit amet placerat ac, molestie ac libero. Duis gravida accumsan turpis, at vulputate lorem malesuada in. Maecenas rutrum eu purus eget blandit. Duis lacus ligula, tristique id erat non, tempus sagittis nisi. Nunc at odio odio. Aenean egestas euismod ipsum, suscipit iaculis ante vestibulum at. Curabitur tristique dignissim dolor nec finibus.

Praesent a eleifend augue. Mauris in pellentesque turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi finibus nisi sit amet dolor mattis efficitur et ac elit. Nullam euismod ullamcorper aliquam. Praesent ultrices, nisl nec tempus pulvinar, leo sapien congue enim, ut cursus nulla elit at leo. Aliquam iaculis metus in cursus posuere. Sed vel orci id urna auctor pellentesque id vitae augue. Nullam porta tincidunt scelerisque. Fusce vel venenatis lacus, a porta metus. Suspendisse dapibus sem id gravida iaculis. Nunc metus ipsum, mollis at sem sit amet, elementum tincidunt diam. Fusce diam massa, accumsan quis consequat et, viverra vel odio. Cras ac mi leo. Phasellus suscipit euismod lobortis.',
            'budget' => 800,
            'deadline' => 'as_soon_as_possible',
            'city_id' => 15,
            'user_id' => 3,
            'project_category_id' => 13,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

    }
}
