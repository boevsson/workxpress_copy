<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->user->business) {
            return $this->from(env('MAIL_FROM'))->subject('Velkommen til WorkXpress.dk, ' . $this->user->first_name)->markdown('emails.welcome_business',
                ['user' => $this->user]);
        } else {
            return $this->from(env('MAIL_FROM'))->subject('Velkommen til WorkXpress.dk, ' . $this->user->first_name)->markdown('emails.welcome',
                ['user' => $this->user]);
        }

    }
}
