<?php

namespace App\Http\Controllers;

use App\BusinessSetting;
use App\City;
use App\Mail\NewProject;
use App\Mail\Welcome;
use App\Notifications\CanceledProposal;
use App\Notifications\ChosenProposal;
use App\Notifications\GivenUpProject;
use App\Notifications\NewProposal;
use App\Notifications\ProjectCompleted;
use App\Project;
use App\ProjectCategory;
use App\ProjectPhoto;
use App\ProjectProposal;
use App\Rating;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Auth\RegistersUsers;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'store']]);
    }

    public function index()
    {
        if (!Auth::user()->businessSettings) {
            return redirect()->route('home');
        }

        $projects = Project::notPickedProposal()->notCompleted()->fromUserCategories()->orderBy('created_at', 'desc')->paginate(10);

        $project_categories = ProjectCategory::topLevel()->userCategories()->get();

        $cities = City::all();

        foreach ($projects as $project) {

            //prepare deadline text
            switch ($project->deadline) {
                case "as_soon_as_possible":
                    $project->deadline = __('project.as_soon_as_possible');
                    break;
                case "within_one_month":
                    $project->deadline = __('project.within_one_month');
                    break;
                case "within_two_months":
                    $project->deadline = __('project.within_two_months');
                    break;
                case "within_three_months":
                    $project->deadline = __('project.within_three_months');
                    break;
                case "more_than_three_months":
                    $project->deadline = __('project.more_than_three_months');
                    break;
            }

            //shorten description
            $project->description = strlen($project->description) > 300 ? substr($project->description, 0, 300) . "..." : $project->description;
            $project->title = strlen($project->title) > 100 ? substr($project->title, 0, 100) . "..." : $project->title;
        }

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.jobs'),
                'link' => route('projects.index')
            ]
        ];

        return view('projects.index', compact('projects', 'project_categories', 'cities', 'breadcrumbs'));
    }

    public function show($id)
    {
        $project = Project::findOrFail($id);

        //prepare deadline text
        switch ($project->deadline) {
            case "as_soon_as_possible":
                $project->deadline = __('project.as_soon_as_possible');
                break;
            case "within_one_month":
                $project->deadline = __('project.within_one_month');
                break;
            case "within_two_months":
                $project->deadline = __('project.within_two_months');
                break;
            case "within_three_months":
                $project->deadline = __('project.within_three_months');
                break;
            case "more_than_three_months":
                $project->deadline = __('project.more_than_three_months');
                break;
        }

        $canSubmitProposal = true;

        //check if the user has already made a proposal for this project
        foreach ($project->proposals as $proposal) {
            if ($proposal->user_id == Auth::id()) {
                $canSubmitProposal = false;
            }
        }

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.jobs'),
                'link' => route('projects.index')
            ],
            [
                'name' => $project->title,
                'link' => ''
            ]
        ];

        return view('projects.show', compact('project', 'canSubmitProposal', 'breadcrumbs'));
    }

    public function filter()
    {
        $filterFields = Input::all();

        if (isset($filterFields['project_sub_category_id'])) {
            $projectCategoryId = $filterFields['project_sub_category_id'];
        } else {
            $projectCategoryId = $filterFields['project_category_id'];
        }

        $city = $filterFields['city_id'];
        $deadline = $filterFields['deadline'];
        $budget = ($filterFields['budget']) ? $filterFields['budget'] : 0;

        return redirect()->route('projects.showFilter',
            ['projectCategoryId' => $projectCategoryId,
                'city' => $city,
                'deadline' => $deadline,
                'budget' => $budget]);
    }

    public function showFilter($projectCategoryId, $city, $deadline, $budget)
    {

        $projects = Project::projectCategory($projectCategoryId)
            ->city($city)
            ->deadline($deadline)
            ->minBudget($budget)
            ->notPickedProposal()
            ->notCompleted()
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $project_categories = ProjectCategory::topLevel()->userCategories()->get();
        $cities = City::all();

        foreach ($projects as $project) {

            //prepare deadline text
            switch ($project->deadline) {
                case "as_soon_as_possible":
                    $project->deadline = __('project.as_soon_as_possible');
                    break;
                case "within_one_month":
                    $project->deadline = __('project.within_one_month');
                    break;
                case "within_two_months":
                    $project->deadline = __('project.within_two_months');
                    break;
                case "within_three_months":
                    $project->deadline = __('project.within_three_months');
                    break;
                case "more_than_three_months":
                    $project->deadline = __('project.more_than_three_months');
                    break;
            }

            //shorten description
            $project->description = strlen($project->description) > 300 ? substr($project->description, 0, 300) . "..." : $project->description;
            $project->title = strlen($project->title) > 100 ? substr($project->title, 0, 100) . "..." : $project->title;
        }

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.jobs'),
                'link' => route('projects.index')
            ]
        ];

        return view('projects.index', compact('projects', 'project_categories', 'cities', 'filterFields', 'breadcrumbs'));
    }

    public function create($categoryId)
    {

        $project_categories = ProjectCategory::topLevel()->get();
        $cities = City::all();
        $project = new Project();

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.your_tasks'),
                'link' => route('profile.projects')
            ],
            [
                'name' => __('project.create_task'),
                'link' => ''
            ]
        ];

        return view('projects.create', compact('project_categories', 'cities', 'project', 'breadcrumbs', 'categoryId'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if (!Auth::check()) {
            //not logged, create account and then the project.

            $messages = [
                'title.required' => __('validate.title.required'),
                'title.max:255' => __('validate.title.max:255'),
                'description.required' => __('validate.description.required'),
                'description.max:2500' => __('validate.description.max:2500'),
                'budget.numeric' => __('validate.budget.numeric'),
                'city_id.required' => 'Region er obligatorisk.',
                'project_category_id.required' => __('validate.project_category_id.required'),
                'deadline.required' => __('validate.deadline.required'),
                'first_name.required' => __('validate.first_name.required'),
                'first_name.max:255' => __('validate.first_name.max:255'),
                'last_name.required' => __('validate.last_name.required'),
                'last_name.max:255' => __('validate.last_name.max:255'),
                'email.required' => __('validate.email.required'),
                'email.max:255' => __('validate.email.max:255'),
                'phone.required' => __('validate.phone.required'),
                'phone.max:255' => __('validate.phone.max:255'),
                'password.required' => __('validate.password.required'),
                'password.min:6' => __('validate.password.min:6'),
                'password.confirmed' => __('validate.password.confirmed'),

                'company_cvr.required' => __('validate.cvr.required'),
                'company_cvr.max:255' => __('validate.cvr.max:255'),
                'company_name.required' => __('validate.name.required'),
                'company_name.max:255' => __('validate.name.max:255'),
                'owner_first_name.required' => __('validate.owner_first_name.required'),
                'owner_first_name.max:255' => __('validate.owner_first_name.max:255'),
                'owner_last_name.required' => __('validate.owner_last_name.required'),
                'owner_last_name.max:255' => __('validate.owner_last_name.max:255'),
                'company_address.max:255' => __('validate.address.required'),
                'company_address.max:255' => __('validate.address.max:255'),
                'company_email.max:255' => __('validate.email.required'),
                'company_email.max:255' => __('validate.email.max:255'),
                'company_phone.max:255' => __('validate.phone.required'),
                'company_phone.max:255' => __('validate.phone.max:255'),
            ];

            if (isset($data['isCompany']) && $data['isCompany'] == 'on') {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|string|max:255',
                    'description' => 'required|string|max:2500',
                    'budget' => 'numeric|nullable',
                    'deadline' => 'required',
                    'city_id' => 'required',
                    'project_category_id' => 'required',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'phone' => 'required|string|max:255',
                    'password' => 'required|string|min:6|confirmed',
                    'company_cvr' => 'required|string|max:255',
                    'company_name' => 'required|string|max:255',
                    'owner_first_name' => 'required|string|max:255',
                    'owner_last_name' => 'required|string|max:255',
                    'company_address' => 'required|string|max:255',
                    'company_email' => 'required|string|max:255',
                    'company_phone' => 'required|string|max:15',
                ], $messages);
            } else {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|string|max:255',
                    'description' => 'required|string|max:2500',
                    'budget' => 'numeric|nullable',
                    'deadline' => 'required',
                    'city_id' => 'required',
                    'project_category_id' => 'required',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'phone' => 'required|string|max:255',
                    'password' => 'required|string|min:6|confirmed',
                ], $messages);
            }

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $user = $this->createUser($data);

            event(new Registered($user));

            Auth::guard()->login($user);

            $userId = $user->id;
        } else {
            $messages = [
                'title.required' => __('validate.title.required'),
                'title.max:255' => __('validate.title.max:255'),
                'description.required' => __('validate.description.required'),
                'description.max:2500' => __('validate.description.max:2500'),
                'budget.numeric' => __('validate.budget.numeric'),
                'city_id.required' => 'Region er obligatorisk.',
                'project_category_id.required' => __('validate.project_category_id.required'),
                'deadline.required' => __('validate.deadline.required'),
            ];


            $validator = Validator::make($request->all(), [
                'title' => 'required|string|max:255',
                'description' => 'required|string|max:2500',
                'budget' => 'numeric|nullable',
                'deadline' => 'required',
                'city_id' => 'required',
                'project_category_id' => 'required'
            ], $messages);

            $userId = Auth::id();

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $projectFields = $request->all();

        $project = new Project();
        $project->title = $request->get('title');
        $project->description = $request->get('description');
        if ($request->get('budget')) {
            $project->budget = $request->get('budget');
        }
        $project->deadline = $request->get('deadline');
        $project->city_id = $request->get('city_id');
        if ($request->get('project_sub_category_id')) {
            $project->project_category_id = $request->get('project_sub_category_id');
        } else {
            $project->project_category_id = $request->get('project_category_id');
        }
        $project->user_id = $userId;
        $project->save();

        //if has photos to upload
        if (isset($projectFields['photos'])) {

            //check if the photos limit is not reached
            if (count($projectFields['photos']) > 10) {
                return redirect()->route('projects.edit', ['id' => $project->id])->with('error',
                    __('project.max_number'));
            }

            foreach ($projectFields['photos'] as $photo) {
                self::addPhotoToProject($photo, $project->id);
            }
        }

        //send emails to all users interested for tasks in this project category
        $projectCategory = ProjectCategory::findOrFail($project->project_category_id);
        $users = $projectCategory->users()->subscribedForNotifications()->get();

        foreach ($users as $user) {
            Mail::to($user->email)->send(new NewProject($project, $projectCategory));
        }

        return redirect()->route('profile.projects')->with('status', __('project.your_project'));


    }

    public function edit($id)
    {
        $project = Project::findOrFail($id);

        if ($project->user_id != Auth::id()) {
            return __('project.cant_edit');
        }

        $project_categories = ProjectCategory::topLevel()->get();
        $cities = City::all();

        $categoryId = $project->categoryId;

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.your_tasks'),
                'link' => route('profile.projects')
            ],
            [
                'name' => $project->title,
                'link' => ''
            ]
        ];

        return view('projects.edit', compact('project_categories', 'cities', 'project', 'breadcrumbs', 'categoryId'));

    }

    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        if ($project->user_id != Auth::id()) {
            return "You cant edit this!";
        }

        $projectFields = $request->all();

        if (isset($projectFields['completed']) && $projectFields['completed'] == 'on' && !$project->rating && $project->executor) {
            $messages = [
                'title.required' => __('validate.title.required'),
                'title.max:255' => __('validate.title.max:255'),
                'description.required' => __('validate.description.required'),
                'description.max:2500' => __('validate.description.max:2500'),
                'budget.numeric' => __('validate.budget.numeric'),
                'city_id.required' => 'Region er obligatorisk.',
                'project_category_id.required' => __('validate.project_category_id.required'),
                'deadline.required' => __('validate.deadline.required'),
                'rating.required' => __('validate.rating.required'),
                'rating_comment.max:300' => __('validate.rating_comment.max:300')
            ];

            $validator = Validator::make($request->all(), [
                'title' => 'required|string|max:255',
                'description' => 'required|string|max:2500',
                'budget' => 'numeric|nullable',
                'deadline' => 'required',
                'city_id' => 'required',
                'project_category_id' => 'required',
                'rating' => 'required',
                'rating_comment' => 'max:300'
            ], $messages);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }


            //create the rating
            $rating = new Rating();
            $rating->rating = $projectFields['rating'];
            $rating->comment = $projectFields['rating_comment'];
            $rating->user_id = $project->executor->user_id;
            $rating->user_evaluator_id = $project->user_id;
            $rating->project_id = $project->id;
            $rating->save();
        }

        $messages = [
            'title.required' => __('validate.title.required'),
            'title.max:255' => __('validate.title.max:255'),
            'description.required' => __('validate.description.required'),
            'description.max:2500' => __('validate.description.max:2500'),
            'budget.numeric' => __('validate.budget.numeric'),
            'city_id.required' => 'Region er obligatorisk.',
            'project_category_id.required' => __('validate.project_category_id.required'),
            'deadline.required' => __('validate.deadline.required'),
        ];

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:2500',
            'budget' => 'numeric|nullable',
            'deadline' => 'required',
            'city_id' => 'required',
            'project_category_id' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $project->title = $projectFields['title'];
        $project->description = $projectFields['description'];
        $project->budget = $projectFields['budget'];
        $project->deadline = $projectFields['deadline'];
        $project->city_id = $projectFields['city_id'];
        if ($request->get('project_sub_category_id')) {
            $project->project_category_id = $request->get('project_sub_category_id');
        } else {
            $project->project_category_id = $request->get('project_category_id');
        }
        $project->user_id = Auth::id();
        if ($project->rating) {
            $project->completed = true;
        } else {
            if ($project->executor) {
                $project->completed = (isset($projectFields['completed']) && $projectFields['completed'] == 'on') ? true : null;
                $project->completed_by_user_id = $project->executor->user_id;
                $project->executor->user->notify(new ProjectCompleted($project));
            }
        }
        $project->save();

        //if has photos to upload
        if (isset($projectFields['photos'])) {

            //check if the photos limit is not reached
            if ($project->photos->count() + count($projectFields['photos']) > 10) {
                return redirect()->route('projects.edit', ['id' => $project->id])->with('error',
                    __('project.max_number'));
            }

            foreach ($projectFields['photos'] as $photo) {
                self::addPhotoToProject($photo, $project->id);
            }
        }

        return redirect()->route('profile.projects')->with('status', __('project.project_updated'));

    }

    private function addPhotoToProject($photo, $projectId)
    {
        $uploadedPhotoPath = $photo->getPathName();
        $uploadedPhotoExtension = $photo->getClientOriginalExtension();

        $imagesDir = public_path() . '/images/projects/';
        $newFilenameLarge = 'large_' . uniqid(rand()) . '.' . $uploadedPhotoExtension;
        $newFilenameThumb = 'thumb_' . uniqid(rand()) . '.' . $uploadedPhotoExtension;

        File::exists($imagesDir) or File::makeDirectory($imagesDir, 0775, true, true);

        //make the large photo
        $newFileFullPathLargePhoto = $imagesDir . $newFilenameLarge;

        File::copy($uploadedPhotoPath, $newFileFullPathLargePhoto);

        $imgLarge = Image::make($newFileFullPathLargePhoto)->resize(800, 800, function ($constraint) {
            $constraint->aspectRatio(); //retain ratio
            $constraint->upsize(); //don't make the image larger...
        })->save();

        //make the thumb photo
        $newFileFullPathThumbPhoto = $imagesDir . $newFilenameThumb;

        File::copy($uploadedPhotoPath, $newFileFullPathThumbPhoto);

        $imgThumb = Image::make($newFileFullPathThumbPhoto)->resize(120, 120, function ($constraint) {
            $constraint->aspectRatio(); //retain ratio
            $constraint->upsize(); //don't make the image larger...
        })->save();

        ProjectPhoto::create([
            'large_image' => $newFilenameLarge,
            'thumb_image' => $newFilenameThumb,
            'project_id' => $projectId
        ]);

    }

    public function deletePhoto($id)
    {
        $photo = ProjectPhoto::findOrFail($id);
        $project = $photo->project()->get();
        $user = User::find($project[0]->user_id);

        if ($user->id != Auth::id()) {
            return __('project.cant_delete');
        }


        //delete the files
        unlink('images/projects/' . $photo->large_image);
        unlink('images/projects/' . $photo->thumb_image);

        //delete the record from db.
        $photo->delete();

        return redirect()->route('projects.edit', ['id' => $project[0]->id]);
    }

    public function chooseProposal(Request $request)
    {
        $project = Project::findOrFail($request->get('project_id'));

        $project->picked_proposal_id = $request->get('proposal_id');
        $project->save();

        $proposal = ProjectProposal::findOrFail($request->get('proposal_id'));
        $proposal->user->notify(new ChosenProposal($proposal));

        return redirect()->route('projects.edit', ['id' => $project->id]);
    }

    public function cancelProposal(Request $request)
    {
        $project = Project::findOrFail($request->get('project_id'));


        if ($project->user->id != Auth::id()) {
            return "you cant be here";
        }

        $project->executor->user->notify(new CanceledProposal($project));

        $project->picked_proposal_id = null;

        $project->save();

        return redirect()->route('projects.edit', ['id' => $project->id]);
    }

    public function giveUpProject(Request $request)
    {
        $project = Project::findOrFail($request->get('project_id'));

        if ($project->executor->user->id != Auth::id()) {
            return __('project.cant_here');
        }

        $project->user->notify(new GivenUpProject($project));

        $project->picked_proposal_id = null;

        $project->save();

        return redirect()->route('profile.ongoingProjects');
    }

    public function search(Request $request)
    {
        $title = $request->get('title');
        $projects = Project::notPickedProposal()->notCompleted()->fromUserCategories()->title($title)->paginate(3);

        $project_categories = ProjectCategory::topLevel()->userCategories()->get();
        $cities = City::all();

        foreach ($projects as $project) {

            //prepare deadline text
            switch ($project->deadline) {
                case "as_soon_as_possible":
                    $project->deadline = __('project.as_soon_as_possible');
                    break;
                case "within_one_month":
                    $project->deadline = __('project.within_one_month');
                    break;
                case "within_two_months":
                    $project->deadline = __('project.within_two_months');
                    break;
                case "within_three_months":
                    $project->deadline = __('project.within_three_months');
                    break;
                case "more_than_three_months":
                    $project->deadline = __('project.more_than_three_months');
                    break;
            }

            //shorten description
            $project->description = strlen($project->description) > 300 ? substr($project->description, 0, 300) . "..." : $project->description;
            $project->title = strlen($project->title) > 100 ? substr($project->title, 0, 100) . "..." : $project->title;
        }

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.jobs'),
                'link' => route('projects.index')
            ],
            [
                'name' => 'Søgeresultater',
                'link' => ''
            ]
        ];

        return view('projects.index', compact('projects', 'project_categories', 'cities', 'title', 'breadcrumbs'));
    }

    public function destroy(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        //if user is not the owner of the task
        if ($project->user->id != Auth::id()) {
            //if is not admin
            if (!Auth()->user()->admin) {
                return __('project.cant_delete');
            }
        }

        //delete all project proposals
        foreach ($project->proposals as $proposal) {
            $proposal->delete();
        }

        //delete all project images
        foreach ($project->photos as $photo) {
            //delete the files
            unlink('images/projects/' . $photo->large_image);
            unlink('images/projects/' . $photo->thumb_image);
            $photo->delete();
        }

        //delete the project
        $project->delete();

        //redirect to projects index
        $redirectToProjectsIndex = $request->get('redirectToProjectsIndex');
        if (isset($redirectToProjectsIndex)) {
            return redirect()->route('projects.index')->with('status', __('project.delete_task'));
        }

        //redirect to profile projects
        return redirect()->route('profile.projects')->with('status', __('project.delete_task'));
    }

    protected function createUser(array $data)
    {

        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->password = bcrypt($data['password']);
        $user->receive_calls = (isset($data['receive_calls']) && $data['receive_calls'] == 'on') ? 1 : 0;
        $user->save();

        Mail::to($data['email'])->send(new Welcome($user));

        if (isset($data['isCompany']) && $data['isCompany'] == 'on') {
            $businessSettings = new BusinessSetting();
            $businessSettings->cvr = $data['company_cvr'];
            $businessSettings->name = $data['company_name'];
            $businessSettings->owner_first_name = $data['owner_first_name'];
            $businessSettings->owner_last_name = $data['owner_last_name'];
            $businessSettings->address = $data['company_address'];
            $businessSettings->email = $data['company_email'];
            $businessSettings->phone = $data['company_phone'];
            $businessSettings->user_id = $user->id;
            $businessSettings->save();
        }

        return $user;
    }
}