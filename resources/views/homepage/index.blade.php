@extends('layouts.master')

@section('content')
    <header class="header-one" id="header">
        <div class="container">
            <div class="content">
                <div class="row">
                    <h1 class="name revealOnScroll" data-animation="fadeInDown">
                        <a href="{{ url('/') }}" class="home-slider-img"><img src="{{url('img/Work-Express.png')}}"
                                                                              class="slider-img"/></a>
                    </h1>
                    <p class="temp">{{__('homepage.moto')}}</p>
                    <h1 class="hire revealOnScroll" data-animation="fadeInUp"
                        data-timeout="400">{{__('homepage.hire_the_best')}}</h1>
                    <div id="search_category">
                        <search-category></search-category>
                    </div>
                </div><!--./row -->
            </div><!--./content -->
        </div><!--./container -->
    </header><!--./header -->

    <section id="how-to-register">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 services-holder">
                    <h2 style="margin-top: 70px; text-transform: inherit;">WorkXpress - Din serviceportal med kvalificeret arbejdskraft.</h2>
                    <hr class="mint">
                    <div class="col-md-12 ">
                        <p class="step_description" style="text-align: center;">Hos WorkXpress finder du kompetence mennesker til, at udføre et stykke
                            arbejde eller en opgave. Med mere end 30 kategorier, kan du uploade en opgavebeskrivelse, som vil blive distribueret ud i
                            vores netværk. Vores samarbejdspartnere er alle verificeret til, at give dig den bedste oplevelse. Det er underordnet
                            hvilken slags opgave du mangler hjælp til. Du finder både advokathjælp, gartner hjælp og meget andet på WordXpress. Når
                            din opgave er uploadet, vil du begynde at modtage tilbud på din opgave, så vælger du det bedste bud for dig. Med
                            WorkXpress er det slut med, at forsømme opgaver du ikke kan eller vil lave. Vi er her for dig.
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 services-holder">
                    <h2>Sådan gør du</h2>
                    <hr class="mint">
                    <div class="col-md-4 step">
                        <img src="{{asset('img/step_1.png')}}">
                        <p class="step_text">Opret din opgave</p>
                        <p class="step_description">Udfyld detaljer om din opgave.</p>
                    </div>
                    <div class="col-md-4 step">
                        <img src="{{asset('img/step_2.png')}}">
                        <p class="step_text">Send din opgave</p>
                        <p class="step_description">Afvent det bedste tilbud for dig.</p>
                    </div>
                    <div class="col-md-4 step">
                        <img src="{{asset('img/step_3.png')}}">
                        <p class="step_text">Acceptere tilbud</p>
                        <p class="step_description">Få udført din opgave.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="services" class="services">
        <div class="main-container container text-center">

            <div class="row">
                <div class="col-lg-12 services-holder">
                    <h3>{{__('homepage.browse_freelance')}}</h3>
                    <hr class="mint">

                    @foreach($project_categories as $category)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="{{route('projects.create', ['id' => $category->id])}}" class="project-category">
                                <div class="icon-circle">
                                    <div class="project-category-icon" style="background-image: url(../img/cat/{{$category->icon}});"></div>
                                </div>
                                <h4>{{strlen($category->category_name) > 20 ? substr($category->category_name, 0, 20) . "..." : $category->category_name}}</h4>
                            </a>
                        </div>
                    @endforeach
                </div><!-- /.col-lg-12 -->
                <div class="clearfix"></div>
                <div class="col-lg-12"><a href="{{route('catalog.index')}}"
                                          class="kafe-btn kafe-btn-default btn-gradient revealOnScroll bounceIn animated"
                                          data-animation="bounceIn"
                                          data-timeout="400">VIS ALLE ERHVERV</a></div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <section class="banner-comment">
        <div class="banner-overlay">
            <div class="container">
                <div id="homepage-text"></div>
            </div><!-- /container -->
        </div>
    </section><!-- /section -->
    <div id="preload-1"></div>
    <div id="preload-2"></div>
    <div id="preload-3"></div>
    <div id="preload-4"></div>
    <div id="preload-5"></div>
@endsection

@section('scripts')
    <script type="text/javascript">

        var imageId = 1

        function headerChanger() {
            var header = document.getElementById('header');
            var imagesUrl = "{{asset('img/slider/')}}";
            $("#header").css({
                "background-image": "url('" + imagesUrl + "/" + imageId + ".jpg')",
                "background-color": "#474747"
            });
            imageId = imageId + 1
            if (imageId > 5) {
                imageId = 1;
            }
            setTimeout(headerChanger, 4000);
        }

        headerChanger();

        var texts = [
            "<div><strong>ANDERS FRA NÆSTVED SØGER PLADESMED</strong>" +
            "<p>Jeg har fået en bule i min højre fordør. Kan den rettes eller skal jeg have en ny dør? Det er en Peugeot 206 fra 2007. Jeg har vedlagt billeder af bulen og døren.</p></div>",
            "<div><strong>THEIS FRA KØGE SØGER TØMRER</strong>" +
            "<p>Hejsa! Jeg vil gerne udvide min carport med ca. 10 m2, og samtidig have installeret et fjernbetjeningssystem til den. Jeg sender et billede med. I må gerne kontakte mig ang. pris og tidshorisont.</p></div>",
            "<div><strong>CAMILLA FRA ALBERTSLUND SØGER MALER</strong>" +
            "<p>Hej WorkXpress. Jeg har lige fået en ny lejlighed og skal flytte. Min nuværende lejlighed skal males. Den er 72 m2, og alle vægge og lofter skal males hvide. Jeg vil gerne høre, hvad det vil koste :)</p></div>",
            "<div><strong>JENS FRA ROSKILDE SØGER VÆRKSTED</strong>" +
            "<p>Jeg vil gerne finde et autoværksted, som jeg kan komme hos fast. Selvfølgelig til en rimelig pris og uden at skulle vente alt for længe på min tur. Min bil er en Mercedes e220 diesel fra 2009, reg. Ah 35 301. Jeg skal have fuld service og højre bærekugle er vist defekt. Pris og tid?</p></div>",
            "<div><strong>ANETTE FRA KØBENHAVN K SØGER MURER\n</strong>" +
            "<p>Jeg har et badeværelse på 10 m2, og jeg vil gerne have nye fliser på gulvet og væggene. Jeg har allerede købt fliserne, så jeg skal bare have hjælp til at rive de gamle ned og sætte nye op. Afventer jeres bud :)</p></div>"
        ];

        var textId = 0;

        function textChanger() {
            $("#homepage-text").animate({
                opacity: 0
            }, 1000, function () {
                $("#homepage-text").empty();
                $("#homepage-text").append(texts[textId]);
                $("#homepage-text").animate({
                    opacity: 1
                }, 1000, function () {
                    textId++;
                    if (textId > 4) {
                        textId = 0;
                    }
                    setTimeout(textChanger, 4000);
                });
            });
        }

        textChanger();

    </script>
@endsection

