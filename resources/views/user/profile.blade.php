@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.profile')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <form class="form-horizontal white-form" method="POST" action="{{ route('profile.update') }}"
                      enctype="multipart/form-data">
                    <div class="col-lg-8 white-2">
                        <div class="row">

                            {{ csrf_field() }}
                            <div class="col-md-8">

                                <input name="first_name" type="text" placeholder="Fornavn" size="30"
                                       value="{{ $user->first_name }}" required
                                       autofocus/>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif

                                <input id="last_name" type="text" name="last_name" placeholder="Efternavn"
                                       value="{{ $user->last_name }}" required
                                       autofocus>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif

                                <input id="email" type="email" name="email"
                                       value="{{ $user->email }}" required disabled>

                                <input id="phone" type='tel' placeholder="Telefon" name="phone"
                                       value="{{ $user->phone }}"
                                       required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif

                                <textarea id="about" type='text' placeholder="Om mig"
                                          name="about">{{$user->about}}</textarea>

                                @if(!Auth::user()->businessSettings)
                                    <label style="margin-bottom: 15px; float: left;" class="label dark-label">Jeg vil gerne ringes op af
                                        virksomhederne. <input
                                                type="checkbox"
                                                name="receive_calls" {{
                                ($user->receive_calls) ? 'checked' : ''}}></label>
                                @endif

                                <label class="label dark-label">Modtag e-mail-meddelelser. <input type="checkbox"
                                                                                                  name="receive_email_notifications" {{
                                ($user->receive_email_notifications) ? 'checked' : ''}}></label>

                            </div>
                            <div class="col-md-4">
                                <div class="profile-image-holder" style="margin-top: 15px;">
                                    <button id="image-change">
                                        <div class="hover-thumb">{{__('user.upload')}}</div>
                                        <img id="image" alt="" class="profile-image"
                                             src="{{ URL::to('images/profile/'. $user->image) }}"/>
                                    </button>
                                </div>
                                <input id="image-input" type="file" name="image" accept="image/*" style="display: none"
                                       onchange="readImage(this); ">
                            </div>


                        </div>

                        <div class="row">
                            @if(Auth::user()->businessSettings)
                                <div class="col-lg-12 no-padding-on-mobile">
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <h3>{{__('user.question')}}</h3>
                                            <input type="text" class="filter-select" onkeyup="filterCategory(this)"/>
                                        </div>
                                        <div class="project-category-select col-lg-12">

                                            @foreach($project_categories as $category)
                                                <div class="category_checkbox" title="{{$category->category_name}}">
                                                    <label><input type="checkbox" name="categories[]"
                                                                  value="{{$category->id}}" {{in_array($category->id, $user_categories) ? 'checked' : ''}}>
                                                        {{$category->category_name}}
                                                    </label>
                                                </div>
                                                @if($category->children($category->id)->count())
                                                    @foreach($category->children($category->id)->get() as $subCategory)
                                                        <div class="category_checkbox sub_category" title="{{$subCategory->category_name}}">
                                                            <label><input type="checkbox" name="categories[]"
                                                                          value="{{$subCategory->id}}" {{in_array($subCategory->id, $user_categories) ? 'checked' : ''}}>
                                                                {{$subCategory->category_name}}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <h3>{{__('user.question2')}}</h3>

                                            <input type="text" class="filter-select" onkeyup="filterCity(this)"/>
                                        </div>
                                        <div class="project-category-select col-lg-12">
                                            @foreach($cities as $city)
                                                <div class="city_checkbox" title="{{$city->city_name}}">
                                                    <label><input type="checkbox" name="cities[]"
                                                                  value="{{$city->id}}" {{in_array($city->id, $user_cities) ? 'checked' : ''}}>
                                                        {{$city->city_name}}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-lg-12">

                                <!-- Send button -->
                                <button type="submit" class="kafe-btn kafe-btn-mint btn-green full-width update">{{__('user.update')}}</button>
                                <a class="kafe-btn kafe-btn-mint full-width change-password"
                                   href="{{ route('password.change.show') }}">{{__('user.change_password')}}</a>
                            </div>
                        </div>

                    </div>
                </form>
            </div><!-- /.col-lg-8 -->
            <!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection

@section('scripts')
    <script type="text/javascript">

        var imageChange = document.getElementById('image-change');

        imageChange.addEventListener("click", function (event) {

            event.preventDefault();

            var imageInput = document.getElementById("image-input");
            imageInput.click();
        });

        function readImage(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function filterCity(input) {
            var cities_checkboxes = document.getElementsByClassName('city_checkbox');
            for (var i = 0; i < cities_checkboxes.length; i++) {
                var string = cities_checkboxes[i].title;
                string = string.toLocaleLowerCase();
                substring = input.value;
                substring = substring.toLocaleLowerCase();
                if (string.indexOf(substring) == -1) {
                    cities_checkboxes[i].style = "display:none";
                } else {
                    cities_checkboxes[i].style = "display:block";
                }
            }
        }

        function filterCategory(input) {
            var cities_checkboxes = document.getElementsByClassName('category_checkbox');
            for (var i = 0; i < cities_checkboxes.length; i++) {
                var string = cities_checkboxes[i].title;
                substring = input.value;
                string = string.toLocaleLowerCase();
                substring = input.value;
                substring = substring.toLocaleLowerCase();
                if (string.indexOf(substring) == -1) {
                    cities_checkboxes[i].style = "display:none";
                } else {
                    cities_checkboxes[i].style = "display:block";
                }
            }
        }
    </script>
@endsection
