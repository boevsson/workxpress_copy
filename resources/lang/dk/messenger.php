<?php

return [
    'create' => 'Opret ny besked',
    'send_msg' => 'Send besked til',
    'msg' => 'Besked',
    'msgs' => 'Beskeder',
    'submit' => 'Send',
    'add_msg' => 'Tilføj en ny besked',
    'threads' => 'Der er ingen beskeder i din indbakke.',
    'thread_id' => 'Besked med ID: ',
    'not_found' => 'blev ikke fundet.',
];