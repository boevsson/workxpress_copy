@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('admin.multy_message')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h3>{{__('admin.multy_message')}}</h3>

                <form action="{{ route('admin.multy-message.store') }}" method="post">
                {{ csrf_field() }}

                <!-- Subject Form Input -->
                    <div class="form-group">
                        <label class="control-label">{{__('admin.subject')}}</label>
                        <input type="text" class="form-control" name="subject" placeholder="">
                    </div>

                    <!-- Message Form Input -->
                    <div class="form-group">
                        <label class="control-label">{{__('admin.message')}}</label>
                        <textarea name="message" class="form-control"></textarea>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="form-group">
                        <button type="submit" class="kafe-btn kafe-btn-mint btn-green full-width">{{__('admin.submit')}}</button>
                    </div>

                </form>


            </div>
        </div>
    </div>
@endsection
