@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('project.create_task')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container project-create">
        <div class="col-md-12 white-2">
            @include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
            <form onsubmit="lockSubmit()"
                  method="POST" enctype="multipart/form-data" class="white-form"
                  action="{{route('projects.store')}}">
                @include('projects.form', ['project' => $project])
            </form>
        </div>
    </div>
@endsection
