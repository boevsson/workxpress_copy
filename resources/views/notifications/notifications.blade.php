<ul id="notifications-list" class="dropdown-menu notifications-dropdown">
    @if(Auth()->user()->notifications->count())
        @foreach(Auth()->user()->lastNotifications as $notification)
            @if($notification->type == 'App\Notifications\NewProposal')
                <li class="notification-block {{(!$notification->read_at) ? 'not-read' : ''}}">
                    <a onclick="markAsReadNotification('{{$notification->id}}', '{{route('proposal.show', ['id' => $notification->data['project_proposal_id']])}}')">
                        <div class="notification-user-image"><img width="50" height="50" src="{{ URL::to('images/profile/'.
            $notification->data['from_user']['image']) }}" class="img-thumbnail img-responsive"></div>
                        <div class="notification-content">
                            <p>
                <span class="notification-user-name">{{$notification->data['from_user']['first_name']}}
                    {{$notification->data['from_user']['last_name']}}</span>
                                {{__('notifications.submitted_new_proposal')}} <span
                                        style="font-weight: bold;">{{ strlen($notification->data['project_title']) > 25 ? substr($notification->data['project_title'], 0, 25) . "..." : $notification->data['project_title'] }}</span>
                            </p>
                            <span class="notification-time">{{$notification->created_at->diffForHumans()}}</span>
                        </div>
                    </a>

                </li>
            @elseif($notification->type == 'App\Notifications\ChosenProposal')
                <li class="notification-block {{(!$notification->read_at) ? 'not-read' : ''}}">
                    <a onclick="markAsReadNotification('{{$notification->id}}', '{{route('proposal.show', ['id' => $notification->data['project_proposal_id']])}}')">
                        <div class="notification-user-image"><img width="50" height="50" src="{{ URL::to('images/profile/'.
            $notification->data['from_user']['image']) }}" class="img-thumbnail img-responsive"></div>
                        <div class="notification-content">
                            <p>
                <span class="notification-user-name">{{$notification->data['from_user']['first_name']}}
                    {{$notification->data['from_user']['last_name']}}</span>
                                {{__('notifications.chose_proposal')}} <span
                                        style="font-weight: bold;"> {{ strlen($notification->data['project_title']) > 25 ? substr($notification->data['project_title'], 0, 25) . "..." : $notification->data['project_title'] }}</span>
                            </p>
                            <span class="notification-time">{{$notification->created_at->diffForHumans()}}</span>
                        </div>
                    </a>
                </li>
            @elseif($notification->type == 'App\Notifications\NewProposalMessage')
                <li class="notification-block {{(!$notification->read_at) ? 'not-read' : ''}}">
                    <a onclick="markAsReadNotification('{{$notification->id}}', '{{route('proposal.show', ['id' =>
                    $notification->data['proposal_id']])}}')">
                        <div class="notification-user-image"><img width="50" height="50" src="{{ URL::to('images/profile/'.
            $notification->data['from_user']['image']) }}" class="img-thumbnail img-responsive"></div>
                        <div class="notification-content">
                            <p>
                                Ny besked fra
                                <span class="notification-user-name">{{$notification->data['from_user']['first_name']}}
                                    {{$notification->data['from_user']['last_name']}}</span>
                            </p>
                            <span class="notification-time">{{$notification->created_at->diffForHumans()}}</span>
                        </div>
                    </a>
                </li>
            @elseif($notification->type == 'App\Notifications\ProjectCompleted')
                <li class="notification-block {{(!$notification->read_at) ? 'not-read' : ''}}">
                    <a onclick="markAsReadNotification('{{$notification->id}}', '{{route('profile.completedProjects')}}')">
                        <div class="notification-user-image"><img width="50" height="50" src="{{ URL::to('images/profile/'.
            $notification->data['from_user']['image']) }}" class="img-thumbnail img-responsive"></div>
                        <div class="notification-content">
                            <p>
                <span class="notification-user-name">{{$notification->data['from_user']['first_name']}}
                    {{$notification->data['from_user']['last_name']}}</span>
                                {{__('notifications.completed')}} <span
                                        style="font-weight: bold;">{{ strlen($notification->data['project_title']) > 25 ? substr($notification->data['project_title'], 0, 25) . "..." : $notification->data['project_title'] }}</span>
                            </p>
                            <span class="notification-time">{{$notification->created_at->diffForHumans()}}</span>
                        </div>
                    </a>
                </li>
            @elseif($notification->type == 'App\Notifications\CanceledProposal')
                <li class="notification-block {{(!$notification->read_at) ? 'not-read' : ''}}">
                    <a onclick="markAsReadNotification('{{$notification->id}}', '{{route('profile.ongoingProjects')}}')">
                        <div class="notification-user-image"><img width="50" height="50" src="{{ URL::to('images/profile/'.
            $notification->data['from_user']['image']) }}" class="img-thumbnail img-responsive"></div>
                        <div class="notification-content">
                            <p>
                <span class="notification-user-name">{{$notification->data['from_user']['first_name']}}
                    {{$notification->data['from_user']['last_name']}}</span>
                                {{__('notifications.removed')}} <span
                                        style="font-weight: bold;"> {{ strlen($notification->data['project_title']) > 25 ? substr($notification->data['project_title'], 0, 25) . "..." : $notification->data['project_title'] }}</span>
                            </p>
                            <span class="notification-time">{{$notification->created_at->diffForHumans()}}</span>
                        </div>
                    </a>
                </li>
            @elseif($notification->type == 'App\Notifications\GivenUpProject')
                <li class="notification-block {{(!$notification->read_at) ? 'not-read' : ''}}">
                    <a onclick="markAsReadNotification('{{$notification->id}}', '{{route('projects.edit', ['id' => $notification->data['project_id']])}}')">
                        <div class="notification-user-image"><img width="50" height="50" src="{{ URL::to('images/profile/'.
            $notification->data['from_user']['image']) }}" class="img-thumbnail img-responsive"></div>
                        <div class="notification-content">
                            <p>
                <span class="notification-user-name">{{$notification->data['from_user']['first_name']}}
                    {{$notification->data['from_user']['last_name']}}</span>
                                {{__('notifications.give_up')}} <span
                                        style="font-weight: bold;">{{ strlen($notification->data['project_title']) > 25 ? substr($notification->data['project_title'], 0, 25) . "..." : $notification->data['project_title'] }}</span>
                            </p>
                            <span class="notification-time">{{$notification->created_at->diffForHumans()}}</span>
                        </div>
                    </a>
                </li>
            @endif
        @endforeach
        <li><a class="all-notifications-button" href="{{route('notifications.index')}}">Se alle notifikationer</a></li>
    @else
        <li class="no-notify" style="line-height: 20px !important;">{{__('notifications.no_notification')}}</li>
    @endif
</ul>
