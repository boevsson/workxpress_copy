<?php

namespace App\Http\Controllers;

use App\City;
use App\ProjectCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $user = Auth::user();

        $project_categories = ProjectCategory::topLevel()->get();

        $user_categories = [];

        foreach ($user->projectCategories as $category) {
            $user_categories[] = $category->id;
        }

        $user_cities = [];

        foreach ($user->cities as $city) {
            $user_cities[] = $city->id;
        }

        $cities = City::all();

        return view('user.profile', compact('user', 'project_categories', 'user_categories', 'cities', 'user_cities'));
    }

    public function show($userId)
    {
        $user = User::findOrFail($userId);

        return view('user.show', ['user' => $user]);
    }

    public function showCompletedProjects($userId)
    {
        $user = User::findOrFail($userId);

        return view('user.showCompletedProjects', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $messages = [
            'first_name.required' => __('validate.first_name.required'),
            'first_name.max:255' => __('validate.first_name.max:255'),
            'last_name.required' => __('validate.last_name.required'),
            'last_name.max:255' => __('validate.last_name.max:255'),
            'phone.max:255' => __('validate.phone.required'),
            'phone.max:255' => __('validate.phone.max:255'),
        ];


        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->about = $request->get('about');
        $user->receive_email_notifications = ($request->get('receive_email_notifications') == 'on') ? 1 : 0;
        $user->receive_calls = ($request->get('receive_calls') == 'on') ? 1 : 0;

        if ($request->hasFile('image')) {

            $user->image = self::addImage($request);
        }

        if ($request->get('categories')) {

            $user->projectCategories()->sync($request->get('categories'));
        }

        if ($request->get('cities')) {

            $user->cities()->sync($request->get('cities'));
        }

        $user->save();

        return redirect()->back()->with('status', 'Profile updated!');
    }

    public function addImage(Request $request)
    {
        $uploaded_photo_path = $request->file('image')->getPathName();
        $uploaded_photo_extension = $request->file('image')->getClientOriginalExtension();

        return $this->uploadImage(
            $uploaded_photo_path,
            $uploaded_photo_extension
        );
    }

    public function uploadImage(
        $uploadedPhotoPath,
        $uploadedPhotoExtension
    ) {
        //handle the photo upload
        $new_filename = uniqid(rand()) . '.' . $uploadedPhotoExtension;
        $imagesDir = public_path() . '/images/profile/';

        //move original
        File::exists($imagesDir) or File::makeDirectory($imagesDir,
            0775, true, true);

        $newFileFullPath = $imagesDir . $new_filename;

        File::copy($uploadedPhotoPath, $newFileFullPath);

        $img = Image::make($newFileFullPath)->fit(200, 200)->save();

        return $new_filename;
    }

    public function projects()
    {
        $user = Auth::user();
        $projects = $user->projects()->orderBy('created_at', 'desc')->get();

        foreach ($projects as $project) {

            //prepare deadline text
            switch ($project->deadline) {
                case "as_soon_as_possible":
                    $project->deadline = __('project.as_soon_as_possible');
                    break;
                case "within_one_month":
                    $project->deadline = __('project.within_one_month');
                    break;
                case "within_two_months":
                    $project->deadline = __('project.within_two_months');
                    break;
                case "within_three_months":
                    $project->deadline = __('project.within_three_months');
                    break;
                case "more_than_three_months":
                    $project->deadline = __('project.more_than_three_months');
                    break;
            }

            //shorten description
            $project->description = strlen($project->description) > 50 ? substr($project->description, 0, 50) . "..." : $project->description;
        }

        return view('user.projects', compact('user', 'projects'));
    }

    public function proposals()
    {
        return view('user.proposals');
    }

    public function ongoingProjects()
    {
        return view('user.onGoingProjects');
    }

    public function completedProjects()
    {
        return view('user.completedProjects');
    }
}
