@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{{ csrf_field() }}
<div class="form-group">
    <label for="title">{{__('admin.title')}}</label>
    <input type="text" class="form-control" id="title" placeholder="Title"
           name="title" value="{{$information_page->title}}">
</div>

<div class="form-group">
    <label for="content">{{__('admin.content')}}</label>
    <textarea style="display: none;" id="content" name="content">{{$information_page->content}}</textarea>
    <div id="editor"></div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        <label for="columns"> {{__('admin.display_footer')}}</label>
        <select class="form-control" name="column">
            <option value="1" {{($information_page->column == 1) ? 'selected' : ''}}>Første kolonne</option>
            <option value="2" {{($information_page->column == 2) ? 'selected' : ''}}>Anden Kolonne</option>
            <option value="3" {{($information_page->column == 3) ? 'selected' : ''}}>Tredje kolonne</option>
        </select>
    </div>
    <div class="col-md-4">
        <label>
            Vis på siden
            @if(!isset($information_page->visible))
                <input type="checkbox" name="visible" checked>
            @else
                <input type="checkbox" name="visible" {{($information_page->visible) ? 'checked' : ''}}>
            @endif
        </label>
    </div>
</div>

<button type="submit" class="btn btn-primary">{{__('admin.save')}}</button>

@section('scripts')
    <script>
        $('#editor').trumbowyg({
            btnsDef: {
                // Create a new dropdown
                image: {
                    dropdown: ['insertImage', 'upload'],
                    ico: 'insertImage'
                }
            },
            btns: [
                ['viewHTML'],
                ['formatting'],
                ['strong', 'em', 'del'],
                ['superscript', 'subscript'],
                ['link'],
                ['image'], // Our fresh created dropdown
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen']
            ],
            plugins: {
                // Add imagur parameters to upload plugin for demo purposes
                upload: {
                    serverPath: '/admin/information-pages/upload-photo',
                    fileFieldName: 'image',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    urlPropertyName: 'link',
                    error: function (error) {
                        console.log(error)
                    }
                }
            },
            autogrow: true
        }).on('tbwchange', function () {
            console.log($('#editor').trumbowyg('html'));
            document.getElementById("content").value = $('#editor').trumbowyg('html');
        });

        $('#editor').trumbowyg('html', document.getElementById("content").value);
    </script>
@endsection