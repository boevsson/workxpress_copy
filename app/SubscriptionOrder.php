<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionOrder extends Model
{
    protected $fillable = [
        'date',
        'time',
        'txnfee',
        'paymenttype',
        'cardno',
        'hash',
        'status',
        'type',
        'orderid',
        'subscriptionid',
        'user_id',
        'expire_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
