<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Georgi',
            'last_name' => 'Ivanov',
            'email' => 'bbcto91@gmail.com',
            'phone' => '0898974673',
            'password' => bcrypt('samsung91'),
            'admin' => 1,
            'receive_email_notifications' => true,
            'subscription_type_id' => 1
        ]);

        DB::table('users')->insert([
            'first_name' => 'Peshi',
            'last_name' => 'Boevsson',
            'email' => 'pgboev@gmail.com',
            'phone' => '0878878537',
            'password' => bcrypt('admin123'),
            'admin' => 1,
            'receive_email_notifications' => true,
            'subscription_type_id' => 1
        ]);

        DB::table('users')->insert([
            'first_name' => 'Architechs',
            'last_name' => 'Ltd',
            'email' => 'info@architechs.dk',
            'phone' => '898989898989',
            'password' => bcrypt('windows81'),
            'admin' => 1,
            'receive_email_notifications' => true,
            'subscription_type_id' => 1
        ]);

        DB::table('users')->insert([
            'first_name' => 'Velislav',
            'last_name' => 'Marinov',
            'email' => 'marinov@viste.bg',
            'phone' => '0898974673',
            'password' => bcrypt('123456'),
            'admin' => 1,
            'receive_email_notifications' => true,
            'subscription_type_id' => 1
        ]);
    }
}
