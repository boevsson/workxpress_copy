<?php

return [
    'notification' => 'Meddelelser',
    'submitted_new_proposal' => 'fremlagde et nyt forslag til',
    'chose_proposal' => 'Vælg dit forslag til ',
    'removed' => 'fjernede dig fra sin opgave',
    'give_up' => 'opgivet din opgave',
    'no_notification' => 'Ingen meddelelser',
    'completed' => 'fuldførte opgaven',
];