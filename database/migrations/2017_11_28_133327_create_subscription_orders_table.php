<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('txnfee')->nullable();
            $table->string('paymenttype')->nullable();
            $table->string('cardno')->nullable();
            $table->string('hash')->nullable();
            $table->string('status')->nullable();
            $table->string('type')->nullable();
            $table->string('orderid')->nullable();
            $table->string('subscriptionid')->nullable();
            $table->integer('user_id');
            $table->timestamp('expire_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_orders');
    }
}
