<?php

return [
    'contact' => 'Kontakt os',
    'questions' => 'Har du spørgsmål?',
    'info' => 'Vi er altid klar ved tasterne! Skriv til os, så vender vi hurtigt tilbage.',
    'msg_sent' => 'Tak for din henvendelse. Din forespørgsel er nu blevet afsendt. Så snart vi har behandlet din forespørgsel, vil du blive kontaktet.',
];