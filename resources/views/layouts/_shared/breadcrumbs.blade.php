<ul class="breadcrumbs">
    @foreach($breadcrumbs as $breadcrumb)
        @if(!$loop->last)
            <li><a href="{{$breadcrumb['link']}}">{{$breadcrumb['name']}}</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
        @else
            <li><span>{{$breadcrumb['name']}}</span></li>
        @endif
    @endforeach
</ul>
