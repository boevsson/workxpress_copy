<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
    public function index()
    {
        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('contacts.contact'),
                'link' => ''
            ]
        ];

        return view('contacts.index', compact('breadcrumbs'));
    }

    public function sendMail(Request $request)
    {
        $messages = [
            'name.required' => __('validate.name.required'),
            'name.max:255' => __('validate.name.max:255'),
            'email.required' => __('validate.email.required'),
            'email.email' => __('validate.email.email'),
            'email.max:255' => __('validate.email.max:255'),
            'phone.max:20' => __('validate.phone.max:20'),
            'message.required' => __('validate.message.required'),
            'message.max:3000' => __('validate.message.max:3000'),
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'max:20',
            'message' => 'required|max:3000'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $email = new \stdClass();

        $email->name = $request->get('name');
        $email->email = $request->get('email');
        $email->phone = $request->get('phone');
        $email->message = $request->get('message');

        Mail::to('noreply@workxpress.dk')->send(new ContactFormMessage($email));
        return redirect()->route('contacts.index')->with('status', __('contacts.msg_sent'));
    }
}
