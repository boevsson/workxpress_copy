@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('admin.edit_type')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('admin.subscriptionTypes.update', ['id' => $subscription_type->id])}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="subscription_name">{{__('admin.sub_name')}}</label>
                        <input type="text" class="form-control" id="subscription_name" placeholder="{{__('admin.sub_name')}}"
                               name="subscription_name" value="{{$subscription_type->subscription_name}}">
                    </div>
                    <div class="form-group">
                        <label for="price">Abonnementspris</label>
                        <input type="text" class="form-control" id="price" placeholder="Abonnementspris"
                               name="price" value="{{$subscription_type->price}}">
                    </div>
                    <button type="submit" class="btn btn-primary">{{__('admin.save')}}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
