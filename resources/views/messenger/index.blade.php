    @extends('layouts.master')

@section('inner_header')
    <div class="col-md-12 contact-header">
        <h1 style="text-align: center"> {{__('messenger.msgs')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container messenger-index">
        @include('messenger.partials.flash')
        <div class="col-md-12 white-2">
            <div class="">
                @each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
            </div>
        </div>
    </div>
@stop
