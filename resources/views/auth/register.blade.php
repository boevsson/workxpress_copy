@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">TILMELD VIRKSOMHED</h1>
    </div>
@endsection

@section('content')
    <section class="banner-login">
        <div class="main-container container">
            <div class="row">

                <main class="main main-signup col-lg-12">
                    @if(!$subscriptionTypeId)
                        <div class="col-lg-10 col-lg-offset-1 text-center">
                            <p style="margin-bottom: 15px;">
                                <img src="img/register-banner.png" class="img-responsive"/>
                            </p>

                            <div class="row">
                                @foreach($subscription_types as $subscription_type)

                                    <div class="col-lg-6">
                                        <ul class="pricing">
                                            <li>
                                                <div class="price-heading  {{($subscription_type->id == 1) ? 'blue' : 'red'}}">
                                                    <span><i class="fa fa-cogs" aria-hidden="true"></i></span>
                                                    <h2>{{$subscription_type->subscription_name}}</h2>
                                                </div>
                                            </li>
                                            @if($subscription_type->months == 1)
                                                <li>Modtag ubegrænset opgaver</li>
                                                <li>Direkte kontakt til dine kunder</li>
                                                <li>Opgave styring</li>
                                                <li>&nbsp;</li>
                                            @else
                                                <li>Modtag ubegrænset opgaver</li>
                                                <li>Direkte kontakt til dine kunder</li>
                                                <li>Opgave styring</li>
                                                <li>Spar 20% ved køb af 1 år.</li>
                                            @endif
                                            <li class="price-figures">
                                                <h2 class=" {{($subscription_type->id == 1) ? 'blue' : 'red'}}">
                                                    {{floor($subscription_type->price * ((100-20) / 100))}}
                                                </h2>
                                                @if($subscription_type->months == 1)
                                                    <small>{{__('user.per_month')}} (ex. moms)</small>
                                                @else
                                                    <small>per år (ex. moms)</small>
                                                @endif
                                            </li>
                                            <li>
                                                <a type="button"
                                                   href="{{ route('register', ['$subscriptionTypeId' => $subscription_type->id]) }}"
                                                   class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}"
                                                        {{--                                                    onclick="createSubscription({{getenv('EPAY_MERCHANT_NUMBER')}}, {{$subscription_type->id}})"--}}
                                                >
                                                    {{__('subscription.subs_choose')}}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="col-lg-6 col-lg-offset-3 text-center">
                            <div class="form-sign">
                                <form method="post" class="white-form" action="{{route('register')}}" id="registerForm">
                                    {{ csrf_field() }}
                                    <div class="form-body">

                                        <input id="subscription_type_id" type="hidden"
                                               value="{{$subscriptionTypeId ?: old('subscriptionTypeId')}}"
                                               name="subscription_type_id" hidden>

                                        <input onchange="bindFirstName(this)" value="{{old('first_name')}}"
                                               name="first_name"
                                               placeholder="{{__('auth.first_name')}}"
                                               class="form-control"
                                               type="text">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif

                                        <input onchange="bindLastName(this)" value="{{old('last_name')}}"
                                               name="last_name"
                                               placeholder="{{__('auth.last_name')}}"
                                               class="form-control"
                                               type="text">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif

                                        <input onchange="bindPhone(this)" id="phone" value="{{old('phone')}}"
                                               name="phone"
                                               placeholder="{{__('auth.phone')}}" class="form-control"
                                               type="text">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif

                                        <input onchange="bindEmail(this)" id="email" value="{{old('email')}}"
                                               name="email"
                                               placeholder="{{__('auth.email')}}"
                                               class="form-control" type="text">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif

                                        <input name="password" placeholder="{{__('auth.password')}}"
                                               class="form-control"
                                               type="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif

                                        <input name="password_confirmation"
                                               placeholder="{{__('auth.confirm_password')}}"
                                               class="form-control"
                                               type="password">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                        @endif
                                        <label style="display:none;" class="company-label">Dette er en firmakonto.
                                            <input
                                                    checked id="isCompany"
                                                    onchange="toggleCompanyForm()"
                                                    name="isCompany"
                                                    type="checkbox"></label>

                                        <input id="cvr" type="text" class="form-control" value="{{old('company_cvr')}}"
                                               name="company_cvr"
                                               placeholder="CVR">

                                        @if ($errors->has('company_cvr'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('company_cvr') }}</strong>
                                    </span>
                                        @endif

                                        <input id="name" type="text" class="form-control"
                                               value="{{old('company_name')}}"
                                               name="company_name"
                                               placeholder="{{__('user.company_name')}}">

                                        @if ($errors->has('company_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                        @endif

                                        <input style="display:none;" id="owner_first_name" type="text"
                                               class="form-control"
                                               value="{{old('owner_first_name')}}" name="owner_first_name"
                                               placeholder="{{__('user.owner_first_name')}}">

                                        @if ($errors->has('owner_first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('owner_first_name') }}</strong>
                                    </span>
                                        @endif

                                        <input style="display:none;" id="owner_last_name" type="text"
                                               class="form-control"
                                               value="{{old('owner_last_name')}}" name="owner_last_name"
                                               placeholder="{{__('user.owner_last_name')}}">

                                        @if ($errors->has('owner_last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('owner_last_name') }}</strong>
                                    </span>
                                        @endif

                                        <input id="address" type="text" class="form-control"
                                               value="{{old('company_address')}}"
                                               name="company_address"
                                               placeholder="Adresse, Postnummer og By">

                                        @if ($errors->has('company_address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('company_address') }}</strong>
                                    </span>
                                        @endif

                                        <input style="display:none;" id="company_email" type="email"
                                               class="form-control"
                                               value="{{old('company_email')}}"
                                               name="company_email"
                                               placeholder="{{__('auth.email')}}">
                                        @if ($errors->has('company_email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('company_email') }}</strong>
                                    </span>
                                        @endif

                                        <input style="display:none;" id="owner_phone" type='tel' class="form-control"
                                               value="{{old('company_phone')}}"
                                               name="company_phone"
                                               placeholder="{{__('auth.phone')}}">

                                        @if ($errors->has('company_phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('company_phone') }}</strong>
                                    </span>
                                        @endif
                                        <div class="row">
                                            <div class="col-lg-12 no-padding-on-mobile">
                                                <div class="col-md-6">
                                                    <div class="row">

                                                        <h3 class="text-left">{{__('user.question')}}</h3>

                                                        <div class="project-category-select col-lg-12">

                                                            @foreach($project_categories as $category)
                                                                <div class="category_checkbox" title="{{$category->category_name}}">
                                                                    <label><input type="checkbox" name="categories[]"
                                                                                  value="{{$category->id}}">
                                                                        {{$category->category_name}}
                                                                    </label>
                                                                </div>
                                                                @if($category->children($category->id)->count())
                                                                    @foreach($category->children($category->id)->get() as $subCategory)
                                                                        <div class="category_checkbox sub_category"
                                                                             title="{{$subCategory->category_name}}">
                                                                            <label><input type="checkbox" name="categories[]"
                                                                                          value="{{$subCategory->id}}">
                                                                                {{$subCategory->category_name}}
                                                                            </label>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <strong>{{ $errors->first('categories') }}</strong>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">

                                                        <h3 class="text-left">{{__('user.question2')}}</h3>

                                                        <div class="project-category-select col-lg-12">
                                                            @foreach($cities as $city)
                                                                <div class="city_checkbox" title="{{$city->city_name}}">
                                                                    <label><input type="checkbox" name="cities[]"
                                                                                  value="{{$city->id}}">
                                                                        {{$city->city_name}}
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <strong>{{ $errors->first('cities') }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <p style="margin-top: 20px;" class="text-right">
                                            <label><input id="termsAgreement" type="checkbox"> Jeg accepterer (<a
                                                        href="/information-page/2">Vilkår og betingelser</a>)</label>
                                        </p>
                                    </div><!-- /.form-body -->

                                    <div class="form-foot">
                                        <div class="form-actions">
                                            <input value="{{__('auth.register')}}" class="form-btn" type="submit">
                                        </div><!-- /.form-actions -->
                                    </div><!-- /.form-foot -->

                                </form>
                            </div><!-- /.form-sign -->
                        </div><!-- /.col-lg-6 -->
                    @endif
                </main>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /section -->
@endsection

@section('scripts')
    <script charset="UTF-8" src="https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/paymentwindow.js"
            type="text/javascript"></script>
    <script>

        // function toggleCompanyForm() {
        //     var completedCheckbox = document.getElementById("isCompany").checked;
        //
        //     var ratingForm = document.getElementById("company-form");
        //     if (completedCheckbox) {
        //         ratingForm.style.display = 'block';
        //     } else {
        //         ratingForm.style.display = 'none';
        //     }
        // }
        //
        // toggleCompanyForm();

        function bindFirstName(firstNameInput) {
            var ownerFirstName = document.getElementById('owner_first_name');
            ownerFirstName.value = firstNameInput.value;
        }

        function bindLastName(lastNameInput) {
            var ownerLastName = document.getElementById('owner_last_name');
            ownerLastName.value = lastNameInput.value;
        }

        function bindPhone(phone) {
            var ownerPhone = document.getElementById('owner_phone');
            ownerPhone.value = phone.value;
        }

        function bindEmail(email) {
            var ownerEmail = document.getElementById('company_email');
            ownerEmail.value = email.value;
        }

        var registerForm = document.getElementById("registerForm");

        registerForm.addEventListener("submit", function (event) {

            event.preventDefault();

            var termsAgreementCheckbox = document.getElementById('termsAgreement');

            if (!termsAgreementCheckbox.checked) {

                alert('Vilkår og betingelser skal accepteres for at kunne gennemføre din ordre.');
            } else {

                registerForm.submit();
            }

        });
    </script>
@endsection
