<?php

return [
    'contact' => 'Kontakt os',
    'jobs' => 'Alle Opgaver',
    'catalog' => 'Erhverv',
    'create_task' => 'Opret opgave',
    'login' => 'Log ind',
    'register' => 'Tilmeld',
    'admin_panel' => 'Administrator',
    'profile' => 'Din profil',
    'logout' => 'Log ud',
    'about' => 'Om os',
    'company' => 'Information',
    'other_services' => 'Support',
    'browse' => 'Erhverv',
];