<?php

return [
    'connect' => 'Kom i kontakt med os:',
    'contact' => 'Kontaktoplysninger:',
    'question_form' => 'Spørgsmål:',
    'email' => 'Email:',
    'phone' => 'Telefon:',
];