@extends('layouts.master')

@section('content')
    <div class="main-container container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 text-center">
                <div class="form-sign">

                    <form class="white-form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-body">
                            <input placeholder="{{__('auth.email')}}" id="email" type="email" class="form-control" name="email"
                                   value="{{ $email or old('email') }}"
                                   required
                                   autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                            <input placeholder="{{__('auth.password')}}" id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif

                            <input placeholder="{{__('auth.confirm_password')}}" id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif

                            <div class="form-foot">
                                <button type="submit" class="form-btn btn-gradient">
                                    {{__('auth.reset_password')}}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
