<?php

use Illuminate\Database\Seeder;

class ProjectCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            0 => [
                'name' => 'Advokat',
                'icon' => 'law.png',
                'parentId' => 0,
                'subCategories' => [
                    0 => [
                        'name' => 'Skilsmisseadvokat',
                        'parentId' => 1
                    ],
                    1 => [
                        'name' => 'Forsvarsadvokat',
                        'parentId' => 1,
                    ],
                    2 => [
                        'name' => 'Boligadvokat',
                        'parentId' => 1
                    ],
                    3 => [
                        'name' => 'Personskade advokat',
                        'parentId' => 1
                    ],
                    4 => [
                        'name' => 'Erhvervsadvokat',
                        'parentId' => 1
                    ],
                    5 => [
                        'name' => 'Jurisdisk rådgivning',
                        'parentId' => 1
                    ]
                ]
            ],
            1 => [
                'name' => 'Alarm og tyverisikring',
                'icon' => 'guard.png',
                'parentId' => 0,
                'subCategories' => [
                    0 => [
                        'name' => 'Låsesmed',
                        'parentId' => 8
                    ]
                ]
            ],
            2 => [
                'name' => 'Arkitekt',
                'icon' => 'architect.png',
                'parentId' => 0
            ],
            3 => [
                'name' => 'Brolægger',
                'icon' => 'build.png',
                'parentId' => 0
            ],
            4 => [
                'name' => 'Catering',
                'icon' => 'catering.png',
                'parentId' => 0
            ],
            5 => [
                'name' => 'Ejendomsservice',
                'icon' => 'property.png',
                'parentId' => 0
            ],
            6 => [
                'name' => 'Elektriker',
                'icon' => 'electricity.png',
                'parentId' => 0
            ],
            7 => [
                'name' => 'Elevatorservice',
                'icon' => 'lift.png',
                'parentId' => 0
            ],
            8 => [
                'name' => 'Facade- og tagvask',
                'icon' => 'laundry.png',
                'parentId' => 0
            ],
            9 => [
                'name' => 'Flyttefirma',
                'icon' => 'moving-company.png',
                'parentId' => 0
            ],
            10 => [
                'name' => 'Gartner',
                'icon' => 'gardener.png',
                'parentId' => 0
            ],
            11 => [
                'name' => 'Glarmester',
                'icon' => 'glazier.png',
                'parentId' => 0
            ],
            12 => [
                'name' => 'Gulv',
                'icon' => 'tiles.png',
                'parentId' => 0,
                'subCategories' => [
                    0 => [
                        'name' => 'Gulvsliber',
                        'parentId' => 20
                    ],
                    1 => [
                        'name' => 'Gulvmontør',
                        'parentId' => 20
                    ],
                    2 => [
                        'name' => 'Gulvlægger',
                        'parentId' => 20
                    ],
                    3 => [
                        'name' => 'Gulvafslibning',
                        'parentId' => 20
                    ],
                    4 => [
                        'name' => 'Gulvbelægning',
                        'parentId' => 20
                    ],
                ]
            ],
            15 => [
                'name' => 'Isolatør',
                'icon' => 'isolation.png',
                'parentId' => 0
            ],
            16 => [
                'name' => 'Kloakservice',
                'icon' => 'sewing.png',
                'parentId' => 0
            ],
            17 => [
                'name' => 'Kontorforsyning',
                'icon' => 'stabler.png',
                'parentId' => 0
            ],
            18 => [
                'name' => 'Maler',
                'icon' => 'painter.png',
                'parentId' => 0
            ],
            19 => [
                'name' => 'Murer',
                'icon' => 'brick.png',
                'parentId' => 0
            ],
            20 => [
                'name' => 'Nedrivning',
                'icon' => 'demolition.png',
                'parentId' => 0
            ],
            21 => [
                'name' => 'Rengøring',
                'icon' => 'cleaning.png',
                'parentId' => 0
            ],
            22 => [
                'name' => 'Revisor',
                'icon' => 'account.png',
                'parentId' => 0
            ],
            23 => [
                'name' => 'Skadedyrsbekæmpelse',
                'icon' => 'pest-control.png',
                'parentId' => 0
            ],
            24 => [
                'name' => 'Skrædder',
                'icon' => 'tailor.png',
                'parentId' => 0
            ],
            25 => [
                'name' => 'Snerydning',
                'icon' => 'snowremove.png',
                'parentId' => 0
            ],
            26 => [
                'name' => 'Tagdækker',
                'icon' => 'roofs.png',
                'parentId' => 0
            ],
            27 => [
                'name' => 'Tømrer',
                'icon' => 'carpet.png   ',
                'parentId' => 0
            ],
            28 => [
                'name' => 'Træfældning',
                'icon' => 'tree-felling.png',
                'parentId' => 0
            ],
            29 => [
                'name' => 'Trappevask',
                'icon' => 'stars-wash.png',
                'parentId' => 0
            ],
            30 => [
                'name' => 'Autoværksted',
                'icon' => 'mechanik.png',
                'parentId' => 0,
                'subCategories' => [
                    0 => [
                        'name' => 'Pladesmed',
                        'parentId' => 41
                    ],
                    1 => [
                        'name' => 'Mekanisk',
                        'parentId' => 41
                    ]
                ]
            ],
            31 => [
                'name' => 'Vinduespudser',
                'icon' => 'window-clean.png',
                'parentId' => 0
            ],
            32 => [
                'name' => 'VVS',
                'icon' => 'plumbing.png',
                'parentId' => 0
            ],
            33 => [
                'name' => 'Byggetilsyn',
                'icon' => 'house-inspector.png',
                'parentId' => 0
            ],
            34 => [
                'name' => 'Fotograf',
                'icon' => 'photography.png',
                'parentId' => 0
            ],
            35 => [
                'name' => 'Totalentreprise',
                'icon' => 'turnkey.png',
                'parentId' => 0
            ],
            36 => [
                'name' => 'Gardiner og Markiser',
                'icon' => 'Awnin-Gardiner-og-Markiser.png',
                'parentId' => 0
            ],
        ];

        foreach ($categories as $category) {
            DB::table('project_categories')->insert([
                'category_name' => $category['name'],
                'icon' => $category['icon'],
                'parent_id' => $category['parentId'],
            ]);

            if (isset($category['subCategories'])) {
                foreach ($category['subCategories'] as $subCategory) {
                    DB::table('project_categories')->insert([
                        'category_name' => $subCategory['name'],
                        'parent_id' => $subCategory['parentId'],
                    ]);
                }
            }
        }
    }
}
