<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_order_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount');
            $table->string('currency');
            $table->string('status');
            $table->string('orderid');
            $table->string('transactionid');
            $table->string('subscription_order_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_order_payments');
    }
}
