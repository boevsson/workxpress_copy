<div id="sidebar" class="col-lg-4">
    <div class="list">
        <div class="list-group">
            @if(Auth::user()->businessSettings)
                <a class="list-group-item active cat-list" href="{{ route('profile.subscription') }}">
                    @if(Auth::user()->subscription != 'null' && isset(Auth::user()->subscription->subscription_name))
                        <span>Abonnement: <strong>{{Auth::user()->subscription->subscription_name}}</strong></span>
                    @else
                        <span>Abonnement: <strong>{{__('user.no_active_sub')}}</strong></span>
                    @endif
                </a>
            @else
                <a class="list-group-item active cat-list">
                    Profilnavigation
                </a>
            @endif
            <a href="{{ route('profile') }}" class="list-group-item cat-list">
                <i class="fa fa-fw fa-user-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{__('user.profile')}}
            </a>
            <a href="{{ route('profile.business') }}" class="list-group-item cat-list">
                <i class="fa fa-fw fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{__('user.business')}}
            </a>
            <a href="{{route('profile.projects')}}" class="list-group-item cat-list">
                <i class="fa fa-fw fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{__('user.your_tasks')}}
            </a>
            @if(Auth::user()->businessSettings)
                <a href="{{route('profile.proposals')}}" class="list-group-item cat-list">
                    <i class="fa fa-fw fa-money" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{__('user.your_proposals')}}
                </a>
                <a href="{{route('profile.ongoingProjects')}}" class="list-group-item cat-list">
                    <i class="fa fa-fw fa-hourglass-half"
                       aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{__('user.on_going_tasks')}}
                </a>
                <a href="{{route('profile.completedProjects')}}" class="list-group-item cat-list">
                    <i class="fa fa-fw fa-hourglass-end"
                       aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{__('user.completed_tasks')}}
                </a>
            @endif
        </div><!-- ./.list-group -->
    </div><!-- ./.list -->
</div><!-- ./.col-lg-4 -->