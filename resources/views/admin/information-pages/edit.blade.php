@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('admin.edit_infopage')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                <form action="{{route('admin.informationPages.update', ['id' => $information_page->id])}}" method="POST">
                    @include('admin.information-pages.form', ['information_page' => $information_page])
                </form>
            </div>
        </div>
    </div>
@endsection
