@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.edit_proposal')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    <div class="row">
                        <form class="form-horizontal white-form" method="POST" action="{{ route('profile.proposals.update', ['id' =>
                        $proposal->id]) }}">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <input name="bid" type="text" placeholder="Bid" size="30"
                                       value="{{ $proposal->bid }}" required
                                       autofocus/>
                                @if ($errors->has('bid'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bid') }}</strong>
                                    </span>
                                @endif

                                <textarea type='text' placeholder="About me"
                                          name="content">{{$proposal->content}}</textarea>
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif

                                <div class="text-center">
                                    <!-- Send button -->
                                    <button type="submit" class="kafe-btn kafe-btn-mint btn-green full-width">{{__('user.update')}}</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection
