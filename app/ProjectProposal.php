<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectProposal extends Model
{
    protected $fillable = [
        'bid',
        'content',
        'user_id',
        'project_id',
        'picked'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

    public function messages()
    {
        return $this->hasMany('App\ProjectProposalMessage', 'proposal_id');
    }
}
