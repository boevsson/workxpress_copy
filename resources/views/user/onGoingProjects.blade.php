@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.going_tasks')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-hover table-project">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Titel</th>
                            <th>Beskrivelse</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Auth()->user()->onGoingProjects as $project)
                            <tr>
                                <td>{{$project->id}}</td>
                                <td>{{$project->title}}</td>
                                <td>{{strlen($project->description) > 50 ? substr($project->description, 0, 50) . "..." : $project->description}}</td>
                                <td style="text-align: right;">
                                    <form style="display: inline-block" method="POST"
                                          action="{{route('project.giveUpProject')}}" onSubmit="return confirm('Are' +
                                 ' you sure you wish ' + 'to' + ' give up on this task?');">
                                        {{csrf_field()}}
                                        <input type="hidden" name="project_id" value="{{$project->id}}">
                                        <button class="btn btn-danger btn-xs">{{__('user.give_up')}}</button>
                                    </form>
                                    <a href="{{route('projects.show', ['id' => $project->id])}}"
                                       class="btn btn-primary btn-xs">{{__('user.view')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection