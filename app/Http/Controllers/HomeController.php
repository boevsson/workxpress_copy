<?php

namespace App\Http\Controllers;

use App\ProjectCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project_categories = ProjectCategory::topLevel()->orderBy('category_name', 'asc')->paginate(8);

        return view('homepage.index', compact('project_categories'));
    }
}
