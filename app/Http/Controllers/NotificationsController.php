<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function index()
    {
        $notifications = Auth()->user()->notifications()->paginate(10);

        return view('notifications.index', compact('notifications'));
    }

    public function markAsRead($id)
    {
        foreach (Auth::user()->unreadNotifications as $notification) {
            if ($notification->id == $id) {
                $notification->markAsRead();
            }
        }
    }

    public function checkForNotifications()
    {

        $notifications = new \stdClass();
        $notifications->unreadNotifications = Auth()->user()->unreadNotifications->count();
        $notifications->unredMessages = Auth::user()->newThreadsCount();

        return json_encode($notifications);
    }

    public function loadNotifications()
    {
        return view('notifications.notifications');
    }
}
