<?php $count = Auth::user()->newThreadsCount(); ?>
@if(Auth()->user()->unreadNotifications->count())
    <span class="notifications-count label label-danger">{{Auth()->user()->unreadNotifications->count()}}</span>
@endif
