@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.sub')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    <div class="row">
                        <div>
                            @if ($errors->has('response'))
                                <div class="alert alert-danger">
                                    <strong>{{ $errors->first('response') }}</strong>
                                </div>
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if (session('subscriptionTypeId'))
                                <div id="subscription_type_id" style="display: none;"
                                     data-merchant-number="{{getenv('EPAY_MERCHANT_NUMBER')}}"></div>
                            @endif
                        </div>
                        @foreach($subscription_types as $subscription_type)

                            <div class="col-lg-6">
                                <ul class="pricing">
                                    <li>
                                        <div class="price-heading  {{($subscription_type->id == 1) ? 'blue' : 'red'}}">
                                            <span><i class="fa fa-cogs" aria-hidden="true"></i></span>
                                            <h2>{{$subscription_type->subscription_name}}</h2>
                                        </div>
                                    </li>
                                    @if($subscription_type->months == 1)
                                        <li>Modtag ubegrænset opgaver</li>
                                        <li>Direkte kontakt til dine kunder</li>
                                        <li>Opgave styring</li>
                                        <li>&nbsp;</li>
                                    @else
                                        <li>Modtag ubegrænset opgaver</li>
                                        <li>Direkte kontakt til dine kunder</li>
                                        <li>Opgave styring</li>
                                        <li>Spar 20% ved køb af 1 år.</li>
                                    @endif
                                    <li class="price-figures">
                                        <h2 class=" {{($subscription_type->id == 1) ? 'blue' : 'red'}}">
                                            {{floor($subscription_type->price * ((100-20) / 100))}}
                                        </h2>
                                        @if($subscription_type->months == 1)
                                            <small>{{__('user.per_month')}} (ex. moms)</small>
                                        @else
                                            <small>per år (ex. moms)</small>
                                        @endif
                                    </li>
                                    <li>

                                        @if(!$user->subscription)

                                            <button type="button"
                                                    class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}"
                                                    onclick="pay({{getenv('EPAY_MERCHANT_NUMBER')}}, {{$subscription_type->price}})">
                                                {{__('user.upgrade')}}
                                            </button>
                                        @elseif($user->subscription && $user->subscription->id != $subscription_type->id)

                                            @if(!$later_subscription_order || $later_subscription_order->subscription->id !== $subscription_type->id)

                                                @if($subscription_order->type === 'payed' || $subscription_order->subscriptionid !== '' && $subscription_order->subscriptionid !== null)

                                                    <a href="{{ route('profile.subscription.upgrade.later', ['subscription_type_id' => $subscription_type->id]) }}"
                                                       class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}">
                                                        {{__('user.upg_later')}}
                                                    </a>
                                                @endif
                                            @else

                                                <div class="alert alert-info">
                                                    {{__('user.set_upgrade')}}
                                                </div>

                                                <a href="{{ route('profile.subscription.upgrade.later.delete') }}"
                                                   class="kafe-btn kafe-btn-mint full-width btn-blue-bg">{{__('user.cancel')}}</a>
                                            @endif

                                            {{--If this subscription is more expensive than the user's current one--}}
                                            @if($subscription_type->price >= $user->subscription->price)

                                                @if($subscription_order->type === 'payed')

                                                    <a href="{{ route('profile.subscription.change', ['subscription_type_id' => $subscription_type->id]) }}"
                                                       class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}"
                                                       onclick="return upgradeNowConfirm({{$subscription_type->price - $user->subscription->price}})">
                                                        {{__('user.upg_now')}}
                                                    </a>
                                                @else
                                                    @if($subscription_order->subscriptionid !== null && $subscription_order->subscriptionid !== '')

                                                        <a href="{{ route('profile.subscription.change', ['subscription_type_id' => $subscription_type->id]) }}"
                                                           class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}"
                                                           onclick="return upgradeNowConfirm({{$subscription_type->price}})">
                                                            {{__('user.upg_now')}}
                                                        </a>
                                                    @else

                                                        <button type="button"
                                                                class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}"
                                                                onclick="pay({{getenv('EPAY_MERCHANT_NUMBER')}}, {{$subscription_type->price}})">
                                                            {{__('user.upgrade')}}
                                                        </button>
                                                    @endif
                                                @endif
                                            @endif
                                        @else

                                            @if($subscription_order->type === 'free')

                                                <div class="alert alert-info">
                                                    {{__('user.sub_free')}}
                                                </div>
                                            @endif
                                            <a href="{{ route('profile.subscription.delete') }}"
                                               class="kafe-btn kafe-btn-mint full-width {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}"
                                               onclick="return cancelConfirm()">{{__('user.cancel')}}</a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script charset="UTF-8" src="https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/paymentwindow.js"
            type="text/javascript"></script>
    <script type="text/javascript">

        function upgradeNowConfirm(price) {

            return confirm('You will be charged extra ' + price + ' upon the price of the current subscription. Are you sure you want to upgrade to this subscription?');
        }

        function cancelConfirm() {

            return confirm('Your money will not be refunded and you will have the subscription until it expires. Are you sure you want to cancel this subscription?');
        }

        var subscriptionTypeId = document.getElementById("subscription_type_id");

        if (subscriptionTypeId) {

            createSubscription(subscriptionTypeId.dataset.merchantNumber);
        }

        function createSubscription(merchantNumber) {

            paymentwindow = new PaymentWindow({
                'merchantnumber': merchantNumber,
                'amount': 0,
                'currency': "DKK",
                'subscription': "1",
                'instantcapture': "1",
                'cancelurl': '{{ route('profile.subscription.cancel') }}',
                'accepturl': '{{ route('profile.subscription.accept') }}'
            });

            paymentwindow.open();
        }

        function pay(merchantNumber, amount) {

            paymentwindow = new PaymentWindow({
                'merchantnumber': merchantNumber,
                'amount': amount * 100,
                'currency': "DKK",
                'subscription': "1",
                'instantcapture': "1",
                'cancelurl': '{{ route('profile.subscription.cancel') }}',
                'accepturl': '{{ route('profile.subscription.accept') }}'
            });

            paymentwindow.open();
        }
    </script>
@endsection
