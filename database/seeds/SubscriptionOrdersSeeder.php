<?php

use Illuminate\Database\Seeder;

class SubscriptionOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_orders')->insert([
            'date' => '',
            'time' => '',
            'txnfee' => '',
            'paymenttype' => '',
            'cardno' => '',
            'hash' => '',
            'status' => 'active',
            'type' => 'free',
            'orderid' => '',
            'subscriptionid' => null,
            'user_id' => 1
        ]);

        DB::table('subscription_orders')->insert([
            'date' => '',
            'time' => '',
            'txnfee' => '',
            'paymenttype' => '',
            'cardno' => '',
            'hash' => '',
            'status' => 'active',
            'type' => 'free',
            'orderid' => '',
            'subscriptionid' => null,
            'user_id' => 2
        ]);

        DB::table('subscription_orders')->insert([
            'date' => '',
            'time' => '',
            'txnfee' => '',
            'paymenttype' => '',
            'cardno' => '',
            'hash' => '',
            'status' => 'active',
            'type' => 'free',
            'orderid' => '',
            'subscriptionid' => null,
            'user_id' => 3
        ]);

        DB::table('subscription_orders')->insert([
            'date' => '',
            'time' => '',
            'txnfee' => '',
            'paymenttype' => '',
            'cardno' => '',
            'hash' => '',
            'status' => 'active',
            'type' => 'free',
            'orderid' => '',
            'subscriptionid' => null,
            'user_id' => 4
        ]);
    }
}
