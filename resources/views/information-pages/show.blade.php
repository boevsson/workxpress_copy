@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{$information_page->title}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container" style="margin-top: 15px;">
        {!! $information_page->content !!}
    </div>
@endsection