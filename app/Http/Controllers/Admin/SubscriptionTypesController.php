<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SubscriptionType;
use Illuminate\Http\Request;

class SubscriptionTypesController extends Controller
{
    public function index()
    {
        $subscription_types = SubscriptionType::all();

        return view('admin.subscription-types.index', compact('subscription_types'));
    }

    public function edit($id)
    {
        $subscription_type = SubscriptionType::findOrfail($id);

        return view('admin.subscription-types.edit', compact('subscription_type'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'subscription_name' => 'required|max:255',
            'price' => 'required|numeric',
        ]);

        $subscriptionType = SubscriptionType::findOrfail($id);
        $subscriptionType->subscription_name = $request->get('subscription_name');
        $subscriptionType->price = $request->get('price');
        $subscriptionType->save();

        return redirect()->route('admin.subscriptionTypes.edit', ['id' => $id])->with('status', __('subscription.subs_type_updated'));
    }
}
