<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProjectCategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SubscriptionTypesSeeder::class);
        $this->call(SubscriptionOrdersSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(ProjectsSeeder::class);
//        $this->call(RatingsSeeder::class);
    }
}
