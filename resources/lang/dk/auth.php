<?php

return [
    'login' => 'Log ind',
    'register' => 'Tilmeld',
    'remember' => 'Husk mig',
    'forgot_password' => 'Glemt adgangskode',
    'reset_password' => 'Nulstil adgangskode',
    'password' => 'adgangskode',
    'email' => 'Email adresse',
    'confirm_password' => 'Bekræft adgangskode',
    'pass_not_correct' => 'Nuværende adgangskode er ikke korrekt!',
    'pass_changed_successfully' => 'Adgangskoden er ændret!',
    'first_name' => 'Fornavn',
    'last_name' => 'Efternavn',
    'phone' => 'Telefon',
    'send_resed' => 'Send link til at nulstille adgangskode',
    'failed' => 'Vi kan ikke finde disse login oplysninger i vores system. kontrollere venligst at du har indtastet dem korrekt',
    'throttle' => 'For mange login forsøg. Prøv igen om :seconds seconds.',
];