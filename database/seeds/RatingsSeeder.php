<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class RatingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ratings')->insert([
            'comment' => 'Well done job.',
            'rating' => 4,
            'user_id' => 1,
            'user_evaluator_id' => 2,
            'project_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('ratings')->insert([
            'comment' => 'Not so well done job.',
            'rating' => 2,
            'user_id' => 1,
            'user_evaluator_id' => 3,
            'project_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('ratings')->insert([
            'comment' => 'Too good to be true.',
            'rating' => 1,
            'user_id' => 2,
            'user_evaluator_id' => 1,
            'project_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('ratings')->insert([
            'comment' => 'Not bad...',
            'rating' => 3,
            'user_id' => 2,
            'user_evaluator_id' => 3,
            'project_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('ratings')->insert([
            'comment' => 'The best software company!',
            'rating' => 5,
            'user_id' => 3,
            'user_evaluator_id' => 1,
            'project_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);

        DB::table('ratings')->insert([
            'comment' => 'The greatest software company of all times!',
            'rating' => 5,
            'user_id' => 3,
            'user_evaluator_id' => 2,
            'project_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s.u'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s.u')
        ]);
    }
}
