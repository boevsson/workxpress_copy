<?php

namespace App\Http\Controllers;

use App\Notifications\NewProposal;
use App\Notifications\NewProposalMessage;
use App\Project;
use App\ProjectProposal;
use App\ProjectProposalMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ProjectProposalsController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'bid' => 'required|numeric',
            'project_id' => 'required',
            'content' => 'required|max:2000'
        ]);

        $proposal = new ProjectProposal();

        $proposal->bid = $request->get('bid');
        $proposal->content = $request->get('content');
        $proposal->user_id = Auth::id();
        $proposal->project_id = $request->get('project_id');
        $proposal->save();

        $project = Project::findOrFail($proposal->project_id);
        $project->user->notify(new NewProposal($proposal));

        if ($project->user->receive_email_notifications) {
            Mail::to($project->user->email)->send(new \App\Mail\NewProposal($proposal));
        }

        return redirect()->route('projects.show', ['id' => $proposal->project_id])->with('status', 'Dit forslag er indsendt!');
    }

    public function show($id)
    {
        $proposal = ProjectProposal::findOrFail($id);

        if ($proposal->user_id != Auth::id() && $proposal->project->user_id != Auth::id()) {
            return redirect()->route('home');
        }

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.jobs'),
                'link' => route('projects.index')
            ],
            [
                'name' => $proposal->project->title,
                'link' => route('projects.show', ['id' => $proposal->project->id])
            ],
            [
                'name' => __('project.view_proposal'),
                'link' => ''
            ]
        ];

        return view('proposals.show', compact('proposal', 'breadcrumbs'));
    }

    public function edit($id)
    {
        $proposal = ProjectProposal::findOrFail($id);

        //check if the proposal belongs to the user.
        if ($proposal->user_id != Auth::id()) {
            return "You cant edit this proposal!";
        }

        return view('user.edit_proposal', compact('proposal'));
    }

    public function update(Request $request, $id)
    {
        $proposal = ProjectProposal::findOrFail($id);

        //check if the proposal belongs to the user.
        if ($proposal->user_id != Auth::id()) {
            return __('project.cant_edit_proposal');
        }

        $messages = [
            'bid.required' => __('validate.bid.required'),
            'bid.numeric' => __('validate.bid.numeric'),
            'content.required' => __('validate.content.required'),
            'content.max:2000' => __('validate.content.max:2000'),
        ];


        $validator = Validator::make($request->all(), [
            'bid' => 'required|numeric',
            'content' => 'required|max:2000'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $proposal->bid = $request->get('bid');
        $proposal->content = $request->get('content');
        $proposal->save();

        return redirect()->route('proposal.show', ['id' => $proposal->id])->with('status', __('project.proposal_updated'));
    }

    public function destroy($id)
    {
        $proposal = ProjectProposal::findOrFail($id);

        //check if not admin.
        if (Auth::user()->admin != 1) {
            //check if the proposal belongs to the user.
            if ($proposal->user_id != Auth::id()) {
                return __('project.cant_edit_proposal');
            }
        }

        //check if the proposal is chosen for a project, if it is, remove it from the project
        if ($proposal->project->picked_proposal_id == $proposal->id) {
            $project = Project::findOrFail($proposal->project->id);
            $project->picked_proposal_id = null;
            $project->save();
        }

        $proposal->delete();

        return redirect()->back()->with('status', __('project.proposal_deleted'));
    }

    public function storeMessage(Request $request)
    {

        $errorMessates = [
            'message.required' => 'Besked er obligatorisk.'
        ];

        $request->validate([
            'message' => 'required|max:5000',
            'proposal_id' => 'required'
        ], $errorMessates);

        $message = new ProjectProposalMessage();

        $message->user_id = Auth::id();
        $message->proposal_id = $request->get('proposal_id');
        $message->message = $request->get('message');
        $message->save();

        $proposal = ProjectProposal::findOrFail($message->proposal_id);

        if (Auth::id() == $proposal->user_id) {
            //Im the creator of the proposal and sending message to the owner of the project.
            $proposal->project->user->notify(new NewProposalMessage($message));
        } else {
            //Im the owner of the project and sending message to the creator of the proposal
            $proposal->user->notify(new NewProposalMessage($message));
        }

        return redirect()->route('proposal.show', ['id' => $message->proposal_id]);
    }
}
