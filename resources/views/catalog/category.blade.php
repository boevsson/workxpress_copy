@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('catalog.catalog')}} - {{$category->category_name}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container catalog-category">
        <div class="row">
            <div class="col-lg-3">
                <div class="list">
                    <div class="list-group">
           <span class="list-group-item active cat-top">
            <em class="fa fa-fw fa-coffee text-white"></em>{{__('catalog.filters')}}
		   </span>
                        <form method="POST" action="{{route('catalog.filter')}}" onsubmit="return validateFilter();">
                            {{csrf_field()}}

                            <div class="list-group-item cat-list">
                                <div class="form-group ">
                                    <select onchange="getSubCategories()" name="project_category_id"
                                            class="form-control" id="projectCategory">
                                        <option value="0">{{__('catalog.choose_category')}}</option>
                                        @foreach($project_categories as $projectCategory)
                                            <option value="{{$projectCategory->id}}" {{(isset($categoryId) && $projectCategory->id ==
                                    $categoryId) || $category->parent_id == $projectCategory->id ? 'selected' : ''}}>
                                                {{$projectCategory->category_name}}</option>
                                        @endforeach
                                    </select>
                                    <p id="category_error" class="error-text">{{__('catalog.select_category')}}</p>
                                </div>

                                <div class="form-group">
                                    <select name="project_sub_category_id" class="form-control" id="subCategorySelect">

                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="city_id" class="form-control" id="projectCity">
                                        <option value="0">{{__('catalog.choose_city')}}</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}" {{(isset($cityId) && $city->id == $cityId) ?
                                    'selected' :
                                    ''}}>{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary btn-sm">{{__('catalog.filter')}}</button>
                            </div>
                        </form>

                    </div><!-- /.list-group -->
                </div><!-- /.list -->

            </div><!-- /.col-lg-4 -->
            <div class="col-lg-9 white white-no-margin">
                @include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
                @if(!count($users))
                    <h3>{{__('catalog.no_freelancers')}}</h3>
                @endif
                @foreach($users as $user)
                    <div class="job">
                        <div class="row top-sec">
                            <div class="col-lg-12">
                                <div class="col-lg-2 col-md-1 col-sm-2 col-xs-2">
                                    <a class="gold-icon-holder"
                                       href="{{route('profile.show', ['userId' => $user->id])}}">
                                        <img class="img-responsive" src="{{ URL::to('images/profile/'. $user->image) }}"
                                             alt="">
                                    </a>
                                </div><!-- /.col-lg-2 -->
                                <div class="col-lg-10 col-md-11 col-sm-10 col-xs-10">
                                    @if($user->businessSettings)
                                        <h5>
                                            <a href="{{route('profile.show', ['id' => $user->id])}}">{{$user->businessSettings->name}}</a>
                                            <small>{{$user->businessSettings->email}}</small>
                                        </h5>
                                    @else
                                        <h5>
                                            <a href="{{route('profile.show', ['id' => $user->id])}}">{{$user->first_name}}
                                                {{$user->last_name}}</a>
                                            <small>{{$user->email}}</small>
                                        </h5>
                                    @endif
                                    @if($user->subscription_type_id == 1)
                                        <span class="business_icon">
                                    <img width="20" src="{{asset('img/business.png')}}">
                                </span>
                                    @endif
                                    @foreach($user->projectCategories as $category)
                                        <span class="label label-success">{{$category->category_name}}</span>
                                    @endforeach
                                </div><!-- /.col-lg-10 -->
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->

                        @if($user->about)
                            <div class="row mid-sec">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <hr class="small-hr">
                                        <p style="white-space: pre-wrap; margin-bottom: 0;">{{strlen($user->about) > 300 ? substr($user->about, 0, 300) . "..." : $user->about}}</p>
                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.col-lg-12 -->
                            </div><!-- /.row -->
                        @endif
                    </div><!-- /.job -->
                @endforeach

                @if($users->count())
                    <div class="page text-center">
                        {{ $users->links() }}
                    </div><!-- /.page -->
                @endif
            </div><!-- /.col-lg-8 -->
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function getSubCategories() {
            var subCategorySelect = document.getElementById('subCategorySelect');
            var select = document.getElementById('projectCategory');

                    @if($category->parent_id > 0)
            var projectCategoryId = {{$category->id}};
                    @else
            var projectCategoryId = null;
            @endif

            $('#subCategorySelect').empty();

            //check for sub categories
            if (select.value > 0) {
                axios.get('/catalog/get-sub-categories/' + select.value)
                    .then(function (response) {
                        if (response.data.length) {
                            for (var i = 0; i < response.data.length; i++) {
                                if (projectCategoryId == response.data[i].id) {
                                    $('#subCategorySelect').prepend(
                                        '<option selected value=' + response.data[i].id + ' class="project-image-holder">' +
                                        response.data[i].category_name
                                        +
                                        '</option>'
                                    );
                                } else {
                                    $('#subCategorySelect').prepend(
                                        '<option value=' + response.data[i].id + ' class="project-image-holder">' +
                                        response.data[i].category_name
                                        +
                                        '</option>'
                                    );
                                }

                            }
                            subCategorySelect.style = "display:block;";
                        } else {
                            subCategorySelect.style = "display:none;";
                            $('#subCategorySelect').empty();
                        }
                    })
                    .catch(function (error) {
                    });
            } else {
                //hide the subcategories <select>
                subCategorySelect.style = "display:none;";
                $('#subCategorySelect').empty();
            }
        }

        getSubCategories();

        function validateFilter() {
            var projectCategorySelect = document.getElementById('projectCategory');
            var categoryError = document.getElementById('category_error');

            if (projectCategorySelect.value == '0') {
                categoryError.style = "display:block";
                return false;
            } else {
                categoryError.style = "display:none;";
                return true;
            }
        }
    </script>
@endsection
