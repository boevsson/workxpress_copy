<?php

return [
    'title' => [
        'required' => 'Titel skal være udfyldt.',
        'max:255' => 'Titlen må ikke være længere end 255 tegn.',
    ],
    'description' => [
        'required' => 'Beskrivelse skal være udfyldt.',
        'max:2500' => 'Beskrivelsen må ikke være længere end 2500 tegn.',
    ],
    'budget' => [
        'numeric' => 'Budgettet skal være tal.',
    ],
    'city_id' => [
        'required' => 'Region skal være udfyldt.',
    ],
    'project_category_id' => [
        'required' => 'Kategori skal være valgt.',
    ],
    'deadline' => [
        'required' => 'Deadline skal være angivet.',
    ],
    'rating' => [
        'required' => 'Anmeldelse skal være udfyldt.',
    ],
    'rating_comment' => [
        'max:300' => 'Kommentaren må ikke overstige 300 tegn.',
    ],
    'name' => [
        'required' => 'Navn skal være udfyldt.',
        'max:255' => 'Navnet må ikke være længere end 255 tegn.',
    ],
    'email' => [
        'required' => 'Email skal være udfyldt.',
        'max:255' => 'Emailen må ikke være længere end 255 tegn.',
        'email' => 'Emailen skal være en gyldig email-adresse.',
    ],
    'phone' => [
        'max:20' => 'Telefonen må ikke være længere end 20 tegn.'
    ],
    'message' => [
        'required' => 'Meddelelsesfeltet er obligatorisk.',
        'max:3000' => 'Meddelelsen må ikke være længere end 3000 tegn.',
    ],
    'bid' => [
        'required' => 'Bud skal være angivet.',
        'numeric' => 'Budgettet skal være tal.',
    ],
    'content' => [
        'required' => 'Indhold skal være udfyldt.',
        'max:2000' => 'Indholdet må ikke være længere end 2000 tegn.',
    ],
    'first_name' => [
        'required' => 'Fornavn skal være udfyldt.',
        'max:255' => 'Fornavnet må ikke være længere end 255 tegn.',
    ],
    'last_name' => [
        'required' => 'Efternavn skal være udfyldt.',
        'max:255' => 'Efternavnet må ikke være længere end 255 tegn.',
    ],
    'phone' => [
        'required' => 'Telefonnummer skal være angivet.',
        'max:255' => 'Telefonen må ikke være længere end 255 tegn.',
    ],
    'cvr' => [
        'required' => 'Cvr. nr. skal være angivet.',
        'numeric' => 'Den cvr skal være et tal.',
        'digits' => 'Cvr. nr. skal være 8 cifre.'
    ],
    'owner_first_name' => [
        'required' => 'Ejeren navn skal være udfyldt.',
        'max:255' => 'Navnet må ikke være længere end 255 tegn.',
    ],
    'owner_first_name' => [
        'required' => 'Ejeren navn skal være udfyldt.',
        'max:255' => 'Navnet må ikke være længere end 255 tegn.',
    ],
    'owner_last_name' => [
        'required' => 'Ejerens efternavn skal være udfyldt.',
        'max:255' => 'Navnet må ikke være længere end 255 tegn.',
    ],
    'owner_last_name' => [
        'required' => 'Ejerens efternavn skal være udfyldt.',
        'max:255' => 'Navnet må ikke være længere end 255 tegn.',
    ],
    'address' => [
        'required' => 'Adresse skal være udfyldt.',
        'max:255' => 'Adressen må ikke være længere end 255 tegn.',
    ],
    'password' => [
        'required' => 'Adgangskode skal være angivet.',
        'min:6' => 'Adgangskoden må ikke være mindre end 6 tegn.',
        'confirmed' => 'Adgangskode passer ikke.',
    ],
];