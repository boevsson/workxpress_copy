@extends('layouts.master')

@section('content')
    <section class="banner-login">
        <div class="main-container container">
            <div class="row">
                <main class="main main-signup col-lg-12">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('email'))
                        <div class="alert alert-success">
                            {{ session('email') }}
                        </div>
                    @endif
                    <div class="col-lg-6 col-lg-offset-3 text-center">
                        <div class="form-sign">
                            <form method="post" class="white-form" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                <div class="form-head">
                                    <h3>{{__('auth.reset_password')}}</h3>
                                </div><!-- /.form-head -->
                                <div class="form-body">
                                    <input name="email" placeholder="{{__('auth.email')}}" class="form-control" type="text">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                    <div class="form-foot">
                                        <div class="form-actions">
                                            <input value="Send" class="form-btn" type="submit">
                                        </div><!-- /.form-actions -->
                                    </div><!-- /.form-foot -->
                                </div>
                            </form>
                        </div><!-- /.form-sign -->
                    </div><!-- /.col-lg-6 -->
                </main>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /section -->
@endsection
