<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Adgangskoden skal være mindst seks tegn.',
    'reset' => 'Din adgangskode er nulstillet!',
    'sent' => 'Vi sendt en email med et link til at nulstille din adgangskode!',
    'token' => 'Dette link til at nulstille din adgangskode er ikke længere gyldig.',
    'user' => "Vi kan ikke finde en bruger med denne email.",

];
