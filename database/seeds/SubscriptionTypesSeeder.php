<?php

use Illuminate\Database\Seeder;

class SubscriptionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_types')->insert([
            'subscription_name' => 'Business',
            'months' => 1,
            'price' => 249.00,
        ]);

        DB::table('subscription_types')->insert([
            'subscription_name' => 'Business+',
            'months' => 12,
            'price' => 999.00,
        ]);
    }
}
