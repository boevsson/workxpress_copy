<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>WorkXpress</title>
    <link rel="shortcut icon" href="/favicon.png"/>
    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('trumbowyg/ui/trumbowyg.min.css') }}" rel="stylesheet">
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('js/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('js/modernizr-custom.js') }}"></script>
    <script src="{{ asset('trumbowyg/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('trumbowyg/plugins/upload/trumbowyg.upload.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
</head>
<body class="{{(Route::currentRouteName() == 'projects.index' || Route::currentRouteName() == 'projects.filter' || Route::currentRouteName() == 'projects.search') ? 'projects' : 'greybg'}}">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <a href="{{ url('/') }}" class="navbar-brand"><img src="{{url('img/logo.png')}}" class="logo"/></a>
        <input type="checkbox" id="toggle"/>
        <label for="toggle" class="toggle"></label>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <ul class="menu">
            <li><a href="{{ route('catalog.index') }}"><i class="fa fa-list-ul"
                                                          aria-hidden="true"></i> {{__('master.catalog')}}</a>
            </li>
            @if(Auth::check() && Auth::user()->businessSettings)
                <li><a href="{{ route('projects.index') }}"><i class="fa fa-flag"
                                                               aria-hidden="true"></i> {{__('master.jobs')}}</a>
            @endif
            @guest
                <li><a href="{{ route('register') }}"> <i class="fa fa-sign-in"
                                                          aria-hidden="true"></i> Tilmeld Virksomhed</a>
                </li>
            @endguest
            <li><a href="{{route('contacts.index')}}"><i class="fa fa-mobile"
                                                         aria-hidden="true"></i> {{__('master.contact')}}</a></li>

            @auth
                <li id="notifications-button" onclick="loadNotifications()" class="dropdown notifications-button"><a
                            href="#" class="dropdown-toggle"
                            data-toggle="dropdown">
                        <span style="display: {{(Auth()->user()->unreadNotifications->count()) ? 'block' : 'none'}};"
                              id="notifications-count"
                              class="notifications-count label label-danger">{{Auth()->user()->unreadNotifications->count()}}</span>
                        <i class="fa fa-bell user-register" aria-hidden="true"></i>
                        <b class="caret"></b></a>
                    @include('notifications.notifications')
                </li>
            @endauth
            <li id="login-dropdown" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user user-register" aria-hidden="true"></i>
                    <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    @guest
                        <li><a href="{{ route('login') }}">{{__('master.login')}}</a></li>
                        <li><a href="{{ route('register') }}">Tilmeld Virksomhed</a></li>
                    @else
                        @if(Auth::user()->admin)
                            <li>
                                <a href="{{ route('admin') }}"> {{__('master.admin_panel')}}</a>
                            </li>
                        @endif
                        <li><a href="{{ route('profile.projects') }}">{{__('master.profile')}}</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{__('master.logout')}}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endguest
                </ul>
            </li>
            <li class="create-task-mobile clear-left"><a href="{{ route('projects.create', ['id' => 0]) }}"
                                                         class="kafe-btn kafe-btn-default btn-gradient revealOnScroll bounceIn animated">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    {{__('master.create_task')}} </a></li>
        </ul><!-- /.ul.menu -->
    </div><!-- /.container -->
</nav><!-- /.nav -->

@if(Route::currentRouteName() != 'home' && Route::currentRouteName() != 'profile.show' && Route::currentRouteName() != 'profile.showCompletedProjects')
    @include('layouts._shared.inner_header')
@endif

@yield('content')
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 text-left">
                <h4 class="heading no-margin">{{__('contacts.contact')}}</h4>
                <hr class="mint">
                <p>Linkøpingvej 22 2. Tv<br>
                    4000 Roskilde<br>
                    CVR: 38769650
                    <a href="mailto:support@workxpress.dk">support@workxpress.dk</a>
                </p>
            </div><!-- /.col-md-4 -->

            <div class="col-md-3 col-sm-6 text-left">
                <h4 class="heading no-margin">{{__('master.company')}}</h4>
                <hr class="mint">
                <div class="no-padding">
                    @foreach($information_pages_footer as $page)
                        @if($page->column == 1)
                            <a href="{{route('informationPages.show', ['id' => $page->id])}}">{{$page->title}}</a>
                        @endif
                    @endforeach
                </div>
            </div><!-- /.col-md-2 -->

            <div class="col-md-3 col-sm-6 text-left">
                <h4 class="heading no-margin">{{__('master.other_services')}}</h4>
                <hr class="mint">
                <div class="no-padding">
                    @foreach($information_pages_footer as $page)
                        @if($page->column == 2)
                            <a href="{{route('informationPages.show', ['id' => $page->id])}}">{{$page->title}}</a>
                        @endif
                    @endforeach
                </div>
            </div><!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6 text-left">
                <h4 class="heading no-margin">{{__('master.browse')}}</h4>
                <hr class="mint">
                <div class="no-padding">
                    @foreach($information_pages_footer as $page)
                        @if($page->column == 3)
                            <a href="{{route('informationPages.show', ['id' => $page->id])}}">{{$page->title}}</a>
                        @endif
                    @endforeach
                </div>
            </div><!-- /.col-md-3 -->

        </div><!-- /.row -->
        <div class="clearfix"></div>
    </div><!-- /.container-->
</div><!-- /.footer -->

<footer id="main-footer" class="main-footer">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right revealOnScroll copyright"
                 data-animation="slideInRight"
                 data-timeout="200">
                <p>&copy;2018 <a href="{{url('/')}}">WorkXpress.dk</a></p>
            </div>
            <!-- /.col-sm-4 -->

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 revealOnScroll" data-animation="bounceIn"
                 data-timeout="200">
                <div class="img-responsive text-center">
                    <img src="{{url('img/logo_email.png')}}"></div><!-- End image-->
            </div>
            <!-- /.col-sm-4 -->


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="payment_icons" src="{{url('img/payment_icons.png')}}"></div><!-- End image-->
        </div>
        <!-- /.col-sm-4 -->

    </div><!-- /.row -->
    </div><!-- /.container -->
</footer><!-- /.footer -->

<a id="scrollup"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

<!-- Scripts -->

<script src="{{ asset('js/lightbox.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/waypoints.min.js') }}" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="{{ asset('js/kafe.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
@yield('scripts')
<script>
    function markAsReadNotification(id, href) {
        return axios.get('/notifications/mark-as-read/' + id).then(function () {
            window.location = href;
        });
    }

    @if(Auth::check())
    function checkForNotifications() {

        var messagesButton = document.getElementById('messages-button');
        var notificationsButton = document.getElementById('notifications-button');

        var unreadMessagesElement = document.getElementById('messages-count');
        var notificationsElement = document.getElementById('notifications-count');

        axios.get('/notifications/check-for-notifications').then(function (response) {

            if (response.data.unreadNotifications > 0) {
                notificationsElement.innerHTML = response.data.unreadNotifications;
                notificationsElement.style = "display: block";
                notificationsButton.classList.add('new-message-notification');
            }

            setTimeout(checkForNotifications, 5000);

        }).catch(function (error) {

            checkForNotifications();
        });
    }

    checkForNotifications();

    function loadNotifications() {
        var notificationsListElement = document.getElementById('notifications-list');

        axios.get('/notifications/load-notifications').then(function (response) {
            var newNotificationsList = $.parseHTML(response.data);
            notificationsListElement.innerHTML = newNotificationsList[0].innerHTML;
        }).catch(function (error) {
            console.log(error);
        });
    }
    @endif
</script>
</body>
</html>
