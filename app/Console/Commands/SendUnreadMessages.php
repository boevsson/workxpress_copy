<?php

namespace App\Console\Commands;

use App\Mail\UnreadMessages;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendUnreadMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unreadMessages:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails with unread messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();

        $e = 0;

        echo "--------------------------\n";

        foreach ($users as $user) {

            if ($user->newThreadsCount()) {

                $e++;
                Mail::to($user->email)->send(new UnreadMessages($user->newThreadsCount()));
            }
        }

        $now = new \DateTime();

        echo $now->format('Y-m-d H:i:s') . ": DONE. " . $e . " emails sent. \n";
        return;
    }
}
