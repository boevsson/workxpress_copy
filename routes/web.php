<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/register/{subscriptionId?}', 'Auth\RegisterController@showRegistrationForm')->name('register');

//projects
Route::get('/projects/create/{id}', 'ProjectsController@create')->name('projects.create');
Route::post('/projects/store', 'ProjectsController@store')->name('projects.store');

Route::group(['middleware' => ['auth']], function () {

    //profile
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/profile/update', 'ProfileController@update')->name('profile.update');
    Route::get('/profile//password/change', 'Auth\ChangePasswordController@showChangeForm')->name('password.change.show');
    Route::post('/profile/password/change', 'Auth\ChangePasswordController@change')->name('password.change');

    Route::get('/profile/business', 'BusinessSettingsController@index')->name('profile.business');
    Route::post('/profile/business/update', 'BusinessSettingsController@update')->name('profile.business.update');

    Route::get('/profile/projects', 'ProfileController@projects')->name('profile.projects');
    Route::get('/profile/on-going-projects', 'ProfileController@ongoingProjects')->name('profile.ongoingProjects');
    Route::get('/profile/completed-projects', 'ProfileController@completedProjects')->name('profile.completedProjects');

    Route::get('/profile/proposals', 'ProfileController@proposals')->name('profile.proposals');
    Route::get('/profile/proposals/edit/{id}', 'ProjectProposalsController@edit')->name('profile.proposals.edit');
    Route::post('/profile/proposals/update/{id}', 'ProjectProposalsController@update')->name('profile.proposals.update');
    Route::post('/profile/proposals/delete/{id}', 'ProjectProposalsController@destroy')->name('profile.proposals.destroy');

    Route::get('/profile/subscription', 'SubscriptionController@index')->name('profile.subscription');
    Route::get('/profile/subscription/change', 'SubscriptionController@change')->name('profile.subscription.change');
    Route::get('/profile/subscription/later', 'SubscriptionController@upgradeLater')->name('profile.subscription.upgrade.later');
    Route::get('/profile/subscription/later/delete', 'SubscriptionController@upgradeLaterDelete')->name('profile.subscription.upgrade.later.delete');
    Route::get('/profile/subscription/accept', 'SubscriptionController@accept')->name('profile.subscription.accept');
    Route::get('/profile/subscription/cancel', 'SubscriptionController@cancel')->name('profile.subscription.cancel');
    Route::get('/profile/subscription/delete', 'SubscriptionController@delete')->name('profile.subscription.delete');

    //notifications
    Route::get('/notifications/', 'NotificationsController@index')->name('notifications.index');
    Route::get('/notifications/check-for-notifications',
        'NotificationsController@checkForNotifications')->name('notifications.checkForNotifications');
    Route::get('/notifications/load-notifications', 'NotificationsController@loadNotifications')->name('notifications.loadNotifications');
    Route::get('/notifications/mark-as-read/{id}', 'NotificationsController@markAsRead')->name('notifications.markAsRead');

    //messages
    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('create/{userId}', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    });

    //projects
    Route::post('/projects/store-proposal', 'ProjectProposalsController@store')->name('proposal.store');
    Route::post('/projects/store-message', 'ProjectProposalsController@storeMessage')->name('proposal.storeMessage');
    Route::post('/projects/choose-proposal', 'ProjectsController@chooseProposal')->name('project.chooseProposal');
    Route::post('/projects/cancel-proposal', 'ProjectsController@cancelProposal')->name('project.cancelProposal');
    Route::post('/projects/give-up-project', 'ProjectsController@giveUpProject')->name('project.giveUpProject');
    Route::get('/projects/edit/{id}', 'ProjectsController@edit')->name('projects.edit');
    Route::post('/projects/update/{id}', 'ProjectsController@update')->name('projects.update');
    Route::post('/projects/delete/{id}', 'ProjectsController@destroy')->name('projects.destroy');
    Route::get('/projects/photo/delete/{id}', 'ProjectsController@deletePhoto')->name('projects.photo.delete');
});

Route::group(['middleware' => ['admin']], function () {

    //dashboard
    Route::get('/admin', 'Admin\DashboardController@index')->name('admin');

    //users
    Route::get('/admin/users', 'Admin\DashboardController@users')->name('admin.users');
    Route::post('/admin/users/ban/{id}', 'Admin\DashboardController@banUser')->name('admin.banUser');
    Route::post('/admin/users/search/', 'Admin\DashboardController@searchUser')->name('admin.searchUser');
    Route::post('/admin/users/change-user-subscription/{userId}',
        'Admin\DashboardController@changeSubscription')->name('admin.users.changeSubscription');

    //subscription types
    Route::get('/admin/subscription-types', 'Admin\SubscriptionTypesController@index')->name('admin.subscriptionTypes');
    Route::get('/admin/subscription-types/edit/{id}', 'Admin\SubscriptionTypesController@edit')->name('admin.subscriptionTypes.edit');
    Route::post('/admin/subscription-types/update/{id}', 'Admin\SubscriptionTypesController@update')->name('admin.subscriptionTypes.update');

    //multi messages
    Route::get('/admin/multy-message', 'Admin\MultyMessageController@create')->name('admin.multy-message.create');
    Route::post('/admin/multy-message/store', 'Admin\MultyMessageController@store')->name('admin.multy-message.store');

    //information pages
    Route::get('/admin/information-pages', 'Admin\InformationPagesController@index')->name('admin.informationPages');
    Route::get('/admin/create', 'Admin\InformationPagesController@create')->name('admin.informationPages.create');
    Route::post('/admin/information-pages/store', 'Admin\InformationPagesController@store')->name('admin.informationPages.store');
    Route::get('/admin/information-pages/edit/{id}', 'Admin\InformationPagesController@edit')->name('admin.informationPages.edit');
    Route::post('/admin/information-pages/update/{id}', 'Admin\InformationPagesController@update')->name('admin.informationPages.update');
    Route::post('/admin/information-pages/delete/{id}', 'Admin\InformationPagesController@destroy')->name('admin.informationPages.delete');
    Route::post('/admin/information-pages/upload-photo', 'Admin\InformationPagesController@uploadPhoto')->name('admin.informationPages.uploadPhoto');
});

//contacts
Route::get('/contacts', 'ContactsController@index')->name('contacts.index');
Route::post('/contacts/send-mail', 'ContactsController@sendMail')->name('contacts.sendMail');

//user show
Route::get('/user/{userId}', 'ProfileController@show')->name('profile.show');
Route::get('/user/{userId}/completed-projects/', 'ProfileController@showCompletedProjects')->name('profile.showCompletedProjects');

//catalog
Route::get('/catalog', 'CatalogController@index')->name('catalog.index');
Route::get('/catalog/search-category/{name}', 'CatalogController@searchCategory');
Route::get('/catalog/get-sub-categories/{categoryId}', 'CatalogController@getSubCategories')->name('catalog.getSubCategories');
Route::get('/catalog/{categoryId}', 'CatalogController@category')->name('catalog.category');
Route::post('/catalog/filter', 'CatalogController@filter')->name('catalog.filter');

//projects
Route::get('/projects', 'ProjectsController@index')->name('projects.index');
Route::get('/projects/{id}', 'ProjectsController@show')->name('projects.show');
Route::post('/projects/search/', 'ProjectsController@search')->name('projects.search');
Route::post('/projects/filter/', 'ProjectsController@filter')->name('projects.filter');
Route::get('/projects/filter/{projectCategoryId}/{city}/{deadline}/{budget}', 'ProjectsController@showFilter')->name('projects.showFilter');
Route::get('/projects/proposal/{id}', 'ProjectProposalsController@show')->name('proposal.show');

//information pages
Route::get('/information-page/{id}', 'Admin\InformationPagesController@show')->name('informationPages.show');
