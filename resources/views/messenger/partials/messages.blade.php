<div class="media {{($message->user->id == Auth::id()) ? 'pull-left' : 'pull-right'}}">
    <a class=" {{($message->user->id == Auth::id()) ? 'pull-left' : 'pull-right'}}" href="#">
        <img src="{{ URL::to('images/profile/'. $message->user->image) }}"
             alt="{{ $message->user->first_name }} {{ $message->user->last_name }}" class="img-circle">
    </a>
    <div class="media-body">
        <h5 class="media-heading">
            @if($message->user->businessSettings)
                {{$message->user->businessSettings->name}}
            @else
                {{$message->user->first_name}} {{$message->user->last_name}}
            @endif
            @if($message->user->admin)
                <strong>(Administrator)</strong>
            @endif
        </h5>
        <p>{{ $message->body }}</p>
        <div class="text-muted">
            <small>Posted {{ $message->created_at->diffForHumans() }}</small>
        </div>
    </div>
</div>
