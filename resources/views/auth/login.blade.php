@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('auth.login')}}</h1>
    </div>
@endsection

@section('content')
    <section class="banner-login">
        <div class="main-container container">
            <div class="row">
                <main class="main main-signup col-lg-12">
                    @if (session('not_logged'))
                        <div class="alert alert-success">
                            {{ session('not_logged') }}
                        </div>
                    @endif
                    <div class="col-lg-6 col-lg-offset-3 text-center">
                        <div class="form-sign">
                            <form method="post" class="white-form" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-body">

                                    <input name="email" placeholder="{{__('auth.email')}}" class="form-control" type="text">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                    <input name="password" placeholder="{{__('auth.password')}}" class="form-control" type="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>Du skal indtaste din kode her</strong>
                                    </span>
                                    @endif
                                </div><!-- /.form-body -->

                                <div class="checkbox" style="text-align: left">
                                    <label style="color:#777;">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{__('auth.remember')}}
                                    </label>
                                </div>

                                <div class="form-foot">
                                    <div class="form-actions">
                                        <input value="{{__('auth.login')}}" class="form-btn btn-gradient" type="submit">
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ route('register') }}" class="form-btn sign-up-btn btn-gradient">{{__('master.register')}}</a>
                                    </div>
                                    <div class="form-head">
                                        <a href="{{ route('password.request') }}" class="more-link">Har du glemt dit login eller din adgangskode?</a>
                                    </div>
                                </div><!-- /.form-foot -->

                            </form>
                        </div><!-- /.form-sign -->
                    </div><!-- /.col-lg-6 -->
                </main>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /section -->
@endsection
