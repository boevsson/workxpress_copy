<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; ?>

<div style="margin: 0 auto; float: none;" class="media col-lg-6 alert {{ $class }} border-left">
    <h4 class="media-heading">
        <a href="{{ route('messages.show', $thread->id) }}">{{ $thread->subject }}</a>
        <p>
            <small>
                ({{ $thread->userUnreadMessagesCount(Auth::id()) }} unread)
            </small>
        </p>
    </h4>
    <p><i class="fa fa-fw fa-user-o" aria-hidden="true"></i>
        @if($thread->latestMessage->user->businessSettings)
            {{$thread->latestMessage->user->businessSettings->name}}: {{ $thread->latestMessage->body }}
        @else
            {{$thread->latestMessage->user->first_name}} {{$thread->latestMessage->user->last_name}}: {{ $thread->latestMessage->body }}
        @endif
    </p>
    {{--<p>--}}
    {{--<small><strong>Creator:</strong>--}}
    {{--@if($thread->creator()->businessSettings)--}}
    {{--{{$thread->creator()->businessSettings->name}}--}}
    {{--@else--}}
    {{--{{ $thread->creator()->first_name }} {{ $thread->creator()->last_name }}--}}
    {{--@endif--}}
    {{--@if($thread->creator()->admin)--}}
    {{--<strong>(Administrator)</strong>--}}
    {{--@endif--}}
    {{--</small>--}}
    {{--</p>--}}
    {{--<p>--}}
    {{--<small><strong>Participants:</strong> {{ $thread->participantsString(Auth::id()) }}</small>--}}
    {{--</p>--}}
</div>


