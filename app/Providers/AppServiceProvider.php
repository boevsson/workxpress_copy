<?php

namespace App\Providers;

use App\InformationPage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        App::setLocale('dk');
        Carbon::setLocale('da');

        if (Schema::hasTable('information_pages')) {
            $information_pages_footer = InformationPage::visible()->get();
            View::share('information_pages_footer', $information_pages_footer);
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
