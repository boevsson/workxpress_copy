@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.profile')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('projects.create', ['id' => 0]) }}" class="kafe-btn kafe-btn-mint btn-green">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('user.create_task')}}
                    </a>
                    @if($projects->count())
                        <table class="table table-hover table-project">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Titel</th>
                                <th>Beskrivelse</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td>{{$project->id}}</td>
                                    <td><a href="{{route('projects.show', ['id' => $project->id])}}">{{$project->title}}</a></td>
                                    <td>{{$project->description}}</td>
                                    <td style="width: 135px;">
                                        <a href="{{route('projects.show', ['id' => $project->id])}}"
                                           class="pull-left kafe-btn kafe-btn-mint-small"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a style="margin-left: 7px;" href="{{route('projects.edit', ['id' => $project->id])}}"
                                           class="pull-left kafe-btn kafe-btn-mint-small"><i class="fa fa-pencil"
                                                                                             aria-hidden="true"></i></a>
                                        <form onSubmit="return confirm('Er du sikker på, at du vil slette denne opgave?');"
                                              action="{{route('projects.destroy', ['id' => $project->id])}}"
                                              method="post">
                                            {{csrf_field()}}
                                            <button type="submit" class="pull-right kafe-btn kafe-btn-mint-small"><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p style="text-align: center; margin-top: 50px;"><strong>{{__('user.no_tasks')}}</strong></p>
                    @endif
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection