<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Project extends Model
{

    protected $fillable = [
        'title',
        'description',
        'budget',
        'deadline',
        'city_id',
        'user_id',
        'project_category_id',
        'completed'
    ];

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function project_category()
    {
        return $this->belongsTo('App\ProjectCategory', 'project_category_id');
    }

    public function photos()
    {
        return $this->hasMany('App\ProjectPhoto', 'project_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function executor()
    {
        return $this->belongsTo('App\ProjectProposal', 'picked_proposal_id');
    }

    public function proposals()
    {
        return $this->hasMany('App\ProjectProposal', 'project_id');
    }

    public function rating()
    {
        return $this->hasOne('App\Rating', 'project_id');
    }

    public function scopeProjectCategory($query, $project_category_id)
    {
        if ($project_category_id) {
            return $query->where('project_category_id', $project_category_id);
        }
    }

    public function scopeFromUserCategories($query)
    {
        $userProjectCategoriesIds = [];

        foreach (Auth::user()->projectCategories as $category) {
            $userProjectCategoriesIds[] = $category->id;
        }

        return $query->whereIn('project_category_id', $userProjectCategoriesIds);
    }

    public function scopeCity($query, $city_id)
    {
        if ($city_id) {
            return $query->where('city_id', $city_id);
        }
    }

    public function scopeDeadline($query, $deadline)
    {
        if ($deadline) {
            return $query->where('deadline', $deadline);
        }
    }

    public function scopeMinBudget($query, $minBudget)
    {
        if ($minBudget > 0) {
            return $query->where('budget', '>=', $minBudget);
        }
    }

    public function scopeCompletedBy($query, $user_id)
    {
        if ($user_id) {
            return $query->where('completed_by_user_id', $user_id);
        }
    }

    public function scopeNotPickedProposal($query)
    {
        return $query->where('picked_proposal_id', null);
    }

    public function scopeTitle($query, $title)
    {
        return $query->where('title', 'like', '%' . $title . '%');
    }

    public function scopePickedProposal($query)
    {
        return $query->where('picked_proposal_id', '!=', null);
    }

    public function scopeNotCompleted($query)
    {
        return $query->where('completed', null);
    }

    public function scopeCompleted($query)
    {
        return $query->where('completed', true);
    }
}
