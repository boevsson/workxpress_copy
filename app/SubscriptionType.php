<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionType extends Model
{
    protected $fillable = [
        'subscription_name',
        'months',
        'price',
    ];

    public $timestamps = false;

}
