<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;

class InformationPage extends Model
{
    protected $fillable = [
        'title',
        'content',
        'column',
        'visible'
    ];

    public function scopeVisible($query)
    {
        return $query->where('visible', true);
    }
}
