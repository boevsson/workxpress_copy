<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Message;
use App\Participant;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class MultyMessageController extends Controller
{
    public function create()
    {
        return view('admin.multy-message.create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'subject' => 'required',
            'message' => 'required'
        ]);

        $input = Input::all();

        $users = User::all();

        foreach ($users as $user) {

            if ($user->id != Auth::id()) {
                $thread = Thread::create([
                    'subject' => $input['subject'],
                ]);
                // Message
                Message::create([
                    'thread_id' => $thread->id,
                    'user_id' => Auth::id(),
                    'body' => $input['message'],
                ]);
                // Sender
                Participant::create([
                    'thread_id' => $thread->id,
                    'user_id' => Auth::id(),
                    'last_read' => new Carbon,
                ]);
                // Recipients
                $thread->addParticipant($user->id);
            }
        }

        return redirect()->route('admin.multy-message.create')->with('status', __('admin.multy_msg_sent'));
    }
}
