<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewProject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $projectCategory)
    {
        $this->project = $project;
        $this->projectCategory = $projectCategory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM'))->subject('Ny opgave i ' . $this->projectCategory->category_name)->markdown('emails.project',
            ['project' => $this->project, 'projectCategory' => $this->projectCategory]);
    }
}
