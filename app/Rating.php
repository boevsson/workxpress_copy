<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'rating',
        'comment',
        'user_id',
        'user_evaluator_id'
    ];

    public function userEvaluator()
    {
        return $this->belongsTo('App\User', 'user_evaluator_id');
    }

    public function scopeLastTwoMonths($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subMonths(2));
    }
}
