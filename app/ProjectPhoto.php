<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model
{
    protected $fillable = [
        'large_image',
        'thumb_image',
        'project_id'
    ];

    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }
}
