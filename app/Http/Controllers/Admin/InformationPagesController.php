<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\InformationPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class InformationPagesController extends Controller
{
    public function index()
    {
        $information_pages = InformationPage::paginate(15);

        return view('admin.information-pages.index', compact('information_pages'));
    }

    public function show($id)
    {
        $information_page = InformationPage::findOrFail($id);

        return view('information-pages.show', compact('information_page'));
    }

    public function create()
    {
        $information_page = new InformationPage();

        return view('admin.information-pages.create', compact('information_page'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string'
        ]);

        $information_page = new InformationPage();
        $information_page->title = $request->get('title');
        $information_page->content = $request->get('content');
        $information_page->column = $request->get('column');
        $information_page->visible = ($request->get('visible') == 'on') ? true : false;
        $information_page->save();

        return redirect()->route('admin.informationPages')->with('status', __('admin.info_page_created'));
    }

    public function edit($id)
    {
        $information_page = InformationPage::findOrFail($id);

        return view('admin.information-pages.edit', compact('information_page'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string'
        ]);

        $information_page = InformationPage::findOrFail($id);
        $information_page->title = $request->get('title');
        $information_page->content = $request->get('content');
        $information_page->column = $request->get('column');
        $information_page->visible = ($request->get('visible') == 'on') ? true : false;
        $information_page->save();

        return redirect()->route('admin.informationPages')->with('status', __('admin.info_page_updated'));
    }

    public function destroy($id)
    {
        $information_page = InformationPage::findOrFail($id);
        $information_page->delete();

        return redirect()->route('admin.informationPages')->with('status', __('admin.info_page_deleted'));
    }

    public function uploadPhoto(Request $request)
    {
        $uploadedPhotoPath = $request->file('image')->getPathName();
        $uploadedPhotoExtension = $request->file('image')->getClientOriginalExtension();

        //handle the photo upload
        $new_filename = uniqid(rand()) . '.' . $uploadedPhotoExtension;
        $imagesDir = public_path() . '/images/pages/';

        //move original
        File::exists($imagesDir) or File::makeDirectory($imagesDir,
            0775, true, true);

        $newFileFullPath = $imagesDir . $new_filename;

        File::copy($uploadedPhotoPath, $newFileFullPath);

        $img = Image::make($newFileFullPath)->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();

        $data = new \stdClass();
        $data->link = 'https://' . $_SERVER['HTTP_HOST'] . '/images/pages/' . $new_filename;
        $data->success = true;

        return json_encode($data);
    }
}
