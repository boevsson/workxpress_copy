<?php

return [
    'subs_choose' => 'Vælg abonnement',
    'subs_upgrade_save' => 'Opgradering gemt til senere slettes!',
    'subs_successfully' => 'Abonnementet er opgraderet!',
    'subs_payment_info_saved' => 'Betalingsoplysninger gemt!',
    'subs_cant_changed' => 'Abonnementet kunne ikke ændres!',
    'subs_cant_canceled' => 'Abonnementet kunne ikke annulleres!',
    'subs_canceled' => 'Abonnementet er annulleret!',
    'subs_type_updated' => 'Opdatering af abonnementstype',
];
