@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('admin.sub_type')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                
                @foreach($subscription_types as $subscription_type)
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <ul class="pricing">
                            <li>
                                <div class="price-heading  {{($subscription_type->id == 1) ? 'blue' : 'red'}}">
                                    <span><i class="fa fa-cogs" aria-hidden="true"></i></span>
                                    <h2>{{$subscription_type->subscription_name}}</h2>
                                </div>
                            </li>
                            @if($subscription_type->months == 1)
                                <li>Modtag ubegrænset opgaver</li>
                                <li>Direkte kontakt til dine kunder</li>
                                <li>Opgave styring</li>
                                <li>&nbsp;</li>
                            @else
                                <li>Modtag ubegrænset opgaver</li>
                                <li>Direkte kontakt til dine kunder</li>
                                <li>Opgave styring</li>
                                <li>Spar 20% ved køb af 1 år.</li>
                            @endif
                            <li class="price-figures"><h2 class=" {{($subscription_type->id == 1) ? 'blue' :
                            'red'}}">{{$subscription_type->price}}</h2>
                                <small>Pr. Måned</small>
                            </li>
                            <li><a href="{{route('admin.subscriptionTypes.edit', ['id' => $subscription_type->id])}}"
                                   class="kafe-btn kafe-btn-mint full-width  {{($subscription_type->id == 1) ? 'btn-blue-bg' : ''}}">{{__('admin.edit')}}</a></li>
                        </ul>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
@endsection
