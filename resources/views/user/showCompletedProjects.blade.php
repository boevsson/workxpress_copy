@extends('layouts.master')

@section('content')
    @include('user.header', ['user' => $user])
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.profile_sidebar')
                <div class="col-lg-8 white-2">
                    <div class="about">
                        <div class="col-lg-12 top-sec">
                            <h3>{{__('user.completed_tasks')}}</h3>
                            <div class="col-lg-12">
                                <div class="row">
                                    @foreach($user->completedProjects as $project)
                                        <div class="job">
                                            <div class="row top-sec">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-2 col-xs-12">
                                                        <a href="{{route('profile.show', ['userId' => $project->user->id])}}">
                                                            <img class="img-responsive img-circle"
                                                                 src="{{ URL::to('images/profile/'. $project->user->image) }}"
                                                                 alt="">
                                                        </a>
                                                    </div><!-- /.col-lg-2 -->
                                                    <div class="col-lg-10 col-xs-12">
                                                        <h4><a href="{{route('projects.show', ['id' => $project->id])}}">{{$project->title}}</a></h4>
                                                        @if($project->user->businessSettings)
                                                            <h5>
                                                                <a href="{{route('profile.show', ['id' => $project->user->id])}}">{{$project->user->businessSettings->name}}</a>
                                                                <small>{{$project->user->businessSettings->email}}</small>
                                                            </h5>
                                                        @else
                                                            <h5>
                                                                <a href="{{route('profile.show', ['id' => $project->user->id])}}">{{$project->user->first_name}}
                                                                    {{$project->user->last_name}}</a>
                                                                <small>{{$project->user->email}}</small>
                                                            </h5>
                                                        @endif
                                                    </div><!-- /.col-lg-10 -->
                                                </div><!-- /.col-lg-12 -->
                                            </div><!-- /.row -->

                                            <div class="row mid-sec">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <hr class="small-hr">
                                                        <p style="white-space: pre-wrap; margin-bottom: 0;">{{strlen($project->description) > 300 ? substr($project->description, 0, 300) . "..." : $project->description}}</p>
                                                    </div><!-- /.col-lg-12 -->
                                                </div><!-- /.col-lg-12 -->
                                            </div><!-- /.row -->

                                            <div class="row bottom-sec">
                                                <div class="col-lg-12">

                                                    <div class="col-lg-12">
                                                        <hr class="small-hr">
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <h5> {{__('user.location')}} </h5>
                                                        <p><i class="fa fa-map-marker"></i> {{$project->city->city_name}}</p>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <h5> {{__('user.budget')}} </h5>
                                                        <p>{{$project->budget}}kr</p>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <h5> {{__('user.category')}} </h5>
                                                        <p>{{$project->project_category->category_name}}</p>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <h5> {{__('user.deadline')}} </h5>
                                                        <p>{{$project->deadline}}</p>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <h5>{{__('user.given_rating')}}</h5>
                                                        <p><span class="rating-icon">
                                @if($project->rating->rating == 1)
                                                                    <img src="{{asset('img/rating_icons/1.png')}}">
                                                                @elseif($project->rating->rating == 2 ||$project->rating->rating == 0)
                                                                    <img src="{{asset('img/rating_icons/2.png')}}">
                                                                @else
                                                                    <img src="{{asset('img/rating_icons/3.png')}}">
                                                                @endif
                            </span></p>
                                                    </div>
                                                </div><!-- /.col-lg-12 -->
                                            </div><!-- /.row -->

                                        </div><!-- /.job -->
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    @if( $user->completedProjects->count() > 5)
                        <div class="page text-center">
                            {{ $user->completedProjects->links() }}
                        </div><!-- /.page -->
                    @endif
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection
