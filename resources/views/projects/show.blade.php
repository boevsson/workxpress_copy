@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{$project->title}}</h1>
    </div>
@endsection

@section('content')
    <section class="jobpost">
        <div class="main-container container project-show">
            <div class="row">
                <div class="col-lg-8 white">
                    {{--@include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])--}}
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row post-top-sec">
                        <div class="col-lg-2">
                            <h5> {{ __('project.posted') }} </h5>
                            <p>{{ $project->created_at->diffForHumans()}}</p>
                        </div>
                        <div class="col-lg-2">
                            <h5> {{ __('project.location') }} </h5>
                            <p><i class="fa fa-map-marker"></i> {{$project->city->city_name}}</p>
                        </div>
                        <div class="col-lg-2">
                            <h5> {{__('project.budget')}} </h5>
                            <p>{{$project->budget}}kr</p>
                        </div>
                        <div class="col-lg-2">
                            <h5> {{__('project.category')}} </h5>
                            <p style="word-break: break-all;">{{$project->project_category->category_name}}</p>
                        </div>
                        <div class="col-lg-2">
                            <h5> {{__('project.deadline')}} </h5>
                            <p>{{$project->deadline}}</p>
                        </div>
                        <div class="col-lg-2">
                            <h5> {{__('project.applicants')}} </h5>
                            <p>{{$project->proposals->count()}}</p>
                        </div>

                        <div class="col-lg-12">
                            <hr class="small-hr">
                        </div> <!-- /.col-lg-12 -->
                    </div><!-- /.row -->

                    <div class="post-bottom-sec">
                        <h4> {{__('project.task_description')}}</h4>
                        <p style="white-space: pre-wrap; margin-bottom: 0;">{{$project->description}}</p>
                    </div><!-- /.post-bottom-sec --> <br>

                    @if(Auth::check())
                        @if($project->user->id != Auth::id())
                            @if(Auth::user()->subscription_type_id == 1 || Auth::user()->subscription_type_id == 2)
                                @if($canSubmitProposal)
                                    <h4>{{__('project.make_proposal')}}</h4>
                                    <form method="POST" class="white-form" action="{{route('proposal.store')}}"
                                          style="margin-bottom: 20px;">
                                        {{csrf_field()}}
                                        <input name="bid" type="text" class="form-control"
                                               placeholder="Bud pris">
                                        @if ($errors->has('bid'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('bid') }}</strong>
                                    </span>
                                        @endif

                                        <textarea name="content" class="form-control" placeholder="Indhold"
                                                  rows="4"></textarea>
                                        @if ($errors->has('content'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                        @endif

                                        <input type="hidden" name="project_id" value="{{$project->id}}">
                                        <button type="submit"
                                                class="kafe-btn kafe-btn-mint full-width"> {{__('project.submit_proposal')}}</button>
                                    </form>
                                @else
                                    <p><strong>{{__('project.already_submit')}}</strong></p>
                                @endif
                            @else
                                <p><strong>Du skal have et <a href="{{ route('profile.subscription') }}">abonnement</a>
                                        for at kunne byde på denne opgave.</strong>
                                </p>
                            @endif
                        @endif
                    @else
                        <p><a href="/login">Login</a> to submit proposals.</p>
                    @endif

                    @if($project->proposals->count())
                        <h4>{{__('project.proposals')}}</h4>

                        @foreach($project->proposals as $proposal)
                            @if($proposal->user_id == Auth::id() || $project->user_id == Auth::id())
                                <div class="job">
                                    <div class="row top-sec">
                                        <div class="col-lg-12 relative">
                                            <div class="user-img col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                <a href="{{route('profile.show', ['userId' => $proposal->user->id])}}">
                                                    <img class="img-responsive"
                                                         src="{{ URL::to('images/profile/'. $proposal->user->image) }}"
                                                         alt="">
                                                </a>
                                            </div><!-- /.col-lg-2 -->
                                            <div class="user-email col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                @if($proposal->user->businessSettings)
                                                    <h4>
                                                        <a href="{{route('profile.show', ['userId' => $proposal->user->id])}}">{{$proposal->user->businessSettings->name}}</a>
                                                    </h4>

                                                @else
                                                    <h4>
                                                        <a href="{{route('profile.show', ['userId' => $proposal->user->id])}}">{{$proposal->user->first_name}} {{$proposal->user->last_name}}</a>
                                                    </h4>

                                                @endif
                                            </div><!-- /.col-lg-10 -->
                                            @if(Auth::check() && Auth::user()->admin)
                                                <form onSubmit="return confirm('Are you sure you wish to delete this proposal?');"
                                                      method="POST"
                                                      class="pull-right" style="margin-left: 15px;"
                                                      action="{{route('profile.proposals.destroy',
                                         ['id' =>
                                        $proposal->id])
                                        }}">
                                                    {{csrf_field()}}
                                                    <button type="submit"
                                                            class="delete-proposal kafe-btn kafe-btn-mint-small"><i
                                                                class="fa fa-trash-o"
                                                                aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            @endif
                                        </div><!-- /.col-lg-12 -->
                                    </div><!-- /.row -->

                                    <div class="row mid-sec">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <hr class="small-hr">
                                                <p>{{strlen($proposal->content) > 300 ? substr($proposal->content, 0, 300) . "..." : $proposal->content}}</p>
                                            </div><!-- /.col-lg-12 -->
                                        </div><!-- /.col-lg-12 -->
                                    </div><!-- /.row -->

                                    <div class="row bottom-sec">
                                        <div class="col-lg-12">

                                            <div class="col-lg-12">
                                                <hr class="small-hr">
                                            </div>

                                            <div class="col-lg-3">
                                                <h5> Sendt </h5>
                                                <p> {{ $proposal->created_at->diffForHumans()}}</p>
                                            </div>
                                            <div class="col-lg-2">
                                                <h5> Bud </h5>
                                                <p>{{ $proposal->bid}}kr</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <h5> {{__('project.ratings')}} ({{$proposal->user->ratings->count()}}
                                                    )</h5>
                                                <p>
                                               <span class="rating-icon">
                                @if($proposal->user->avg_rating == 1)
                                                       <img src="{{asset('img/rating_icons/1.png')}}">
                                                   @elseif($proposal->user->avg_rating == 2)
                                                       <img src="{{asset('img/rating_icons/2.png')}}">
                                                   @elseif($proposal->user->avg_rating == 0)
                                                       <span>{{__('project.no_rating_yet')}}</span>
                                                   @else
                                                       <img src="{{asset('img/rating_icons/3.png')}}">
                                                   @endif
                            </span>
                                                </p>
                                            </div>
                                            <div class="col-lg-4 text-center">
                                                <a href="{{route('proposal.show', ['id' => $proposal->id])}}" class="kafe-btn kafe-btn-mint-small
                                            view-proposal"><i
                                                            class="fa fa-align-right"></i> {{__('project.view_proposal')}}
                                                </a>
                                            </div>
                                            {{--<div class="col-lg-2">--}}

                                            {{--</div>--}}

                                        </div><!-- /.col-lg-12 -->
                                    </div><!-- /.row -->

                                </div><!-- /.job -->
                            @endif
                        @endforeach
                    @endif
                </div><!-- /.col-lg-8 -->

                <div class="col-lg-4">

                    <div class="panel user revealOnScroll" data-animation="slideInUp" data-timeout="200">
                        <div class="row text-center">
                            <a href="{{route('profile.show', ['id' => $project->user_id])}}">
                                <img src="{{asset('img/bg/2.jpg')}}" class="img-responsive panel-img" alt="">

                                <div class="col-xs-12 user-avatar">
                                    <img src="{{ URL::to('images/profile/'. $project->user->image) }}" alt="Image"
                                         class="img-thumbnail
                                    img-responsive">
                                    @if($project->user->businessSettings)
                                        <h4>{{$project->user->businessSettings->name}}</h4>
                                    @else
                                        <h4>{{$project->user->first_name}} {{$project->user->last_name}}</h4>
                                    @endif
                                </div><!-- /.col-xs-12 -->
                            </a>
                        </div><!-- /.row -->
                        @if($project->user->receive_calls && Auth::user()->subscription)
                            <div class="list-group">
                                <div class="list-group-item">Jeg vil gerne ringes op af virksomhederne
                                </div><!-- /.list-group-item -->
                                <div class="list-group-item phone-calls">
                                    <i class="fa fa-phone-square" aria-hidden="true"></i>
                                    {{$project->user->phone}}
                                </div><!-- /.list-group-item -->
                            </div>
                        @endif
                    </div><!-- /.list-group-item -->

                    @if($project->photos->count())
                        <div class="list">
                            <div class="list-group">
           <span class="list-group-item active cat-top">
            <i class="fa fa-picture-o" aria-hidden="true"></i> {{__('project.photos')}}
		   </span>
                                <div class="list-group-item masonry" style="overflow: hidden">
                                    @foreach($project->photos as $photo)
                                        <div class="project-image-holder">
                                            <div class="img-thumbnail project-photo">
                                                <a href="{{ URL::to('images/projects/'. $photo->large_image) }}"
                                                   data-lightbox="roadtrip"><img
                                                            src="{{ URL::to('images/projects/'. $photo->thumb_image) }}"></a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div><!-- /.list-group -->
                        </div><!-- /.list -->
                    @endif

                    @if(Auth::user()->admin)
                        <form onSubmit="return confirm('Er du sikker på, at du vil slette denne opgave?');"
                              action="{{route('projects.destroy', ['id' => $project->id])}}"
                              method="post">
                            {{csrf_field()}}
                            <input type="hidden" value="true" name="redirectToProjectsIndex">
                            <button type="submit" title="Slet opgave"
                                    class="pull-right kafe-btn kafe-btn-mint-small"><i
                                        class="fa fa-trash"
                                        aria-hidden="true"></i>
                            </button>
                        </form>
                    @endif

                </div><!-- /.col-lg-4 -->

            </div><!-- /.row-->
        </div><!-- /.container -->
    </section><!-- /section -->
@endsection

@section('scripts')
    <script>
        lightbox.option({
            'albumLabel': 'Billede %1 af %2',
            'resizeDuration': 200,
            'wrapAround': true,
            'fadeDuration': 200
        })
    </script>
@endsection