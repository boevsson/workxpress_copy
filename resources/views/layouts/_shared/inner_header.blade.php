<header class="header-jobs">
    <div class="container">
        <div class="content">
            <div class="row">
                @yield('inner_header')
            </div><!-- /.row -->
        </div><!-- /.content -->
    </div><!-- /.container -->
</header><!-- /header -->