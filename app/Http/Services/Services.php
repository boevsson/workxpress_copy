<?php

namespace App\Services;

use App\SubscriptionOrderPayment;

class Services
{
    public static function authorizePayment($subscription_order, $user_id, $price)
    {
        $random_order_id = self::generateRandomString();
        $price *= 100;

        $epay_params = array();
        $epay_params[ 'merchantnumber' ] = getenv('EPAY_MERCHANT_NUMBER');
        $epay_params[ 'subscriptionid' ] = (string)$subscription_order->subscriptionid;
        $epay_params[ 'orderid' ] = $random_order_id;
        $epay_params[ 'amount' ] = $price;
        $epay_params[ 'currency' ] = "208";
        $epay_params[ 'instantcapture' ] = "1"; //We use instacapture instead of capturing afterwards
        $epay_params[ 'fraud' ] = "0";
        $epay_params[ 'transactionid' ] = "0";
        $epay_params[ 'pbsresponse' ] = "0";
        $epay_params[ 'epayresponse' ] = "0";

        $client = new \SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/subscription.asmx?WSDL');

        $result = $client->authorize($epay_params);

        if ($result->authorizeResult == true) {

            $transactionId = $result->transactionid;

            //Save the payment to our database
            SubscriptionOrderPayment::create([
                'status' => 'success',
                'amount' => $price,
                'currency' => "208",
                'orderid' => $random_order_id,
                'transactionid' => $transactionId,
                'subscription_order_id' => $subscription_order->id,
                'user_id' => $user_id
            ]);

            //We use instacapture instead of capturing afterwards
            //$result = $this->capturePayment($transactionId, $user);

            return true;
        } else {

            var_dump('Payment for subscription: ' . $subscription_order->subscriptionid . ' was unsuccessful. Response code is: ' . $result->epayresponse);
            return false;
        }
    }

    public static function epayDeleteSubscription($subscriptionOrder)
    {
        $epay_params = array();
        $epay_params[ 'merchantnumber' ] = getenv('EPAY_MERCHANT_NUMBER');
        $epay_params[ 'subscriptionid' ] = $subscriptionOrder->subscriptionid;
        $epay_params[ 'epayresponse' ] = "0";

        $client = new \SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/subscription.asmx?WSDL');

        $response = $client->deletesubscription($epay_params);

        return $response;
    }

    //    private static function capturePayment($transactionId, $user)
    //    {
    //        $epay_params = array();
    //
    //        $epay_params['merchantnumber'] = getenv('EPAY_MERCHANT_NUMBER');
    //        $epay_params['transactionid'] = $transactionId;
    //        $epay_params['amount'] = $user->subscription->price;
    //        $epay_params['pbsResponse'] = "-1";
    //        $epay_params['epayresponse'] = "-1";
    //
    //        $client = new \SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/payment.asmx?WSDL');
    //
    //        $result = $client->capture($epay_params);
    //
    //        if ($result->captureResult == true) {
    //
    //            //Successfully captured
    ////            var_dump($result);
    //        } else {
    //
    //            //Error
    ////            var_dump($result);
    //        }
    //
    //        return array($result);
    //    }

    private static function generateRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters_length = strlen($characters);
        $random_string = '';

        for ($i = 0; $i < $length; $i++) {

            $random_string .= $characters[ rand(0, $characters_length - 1) ];
        }

        return $random_string;
    }
}