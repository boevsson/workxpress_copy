<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ChangePasswordController extends Controller
{
    use RedirectsUsers;

    protected $redirectTo = '/profile';

    public function showChangeForm()
    {
        return view('user.password.change');
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

    public function check($value, $hashedValue, array $options = [])
    {
        if (strlen($hashedValue) === 0) {
            return false;
        }

        return password_verify($value, $hashedValue);
    }

    public function change(Request $request)
    {
        $user = Auth::user();

        if(!$this->check($request->get('current_password'), $user->getAuthPassword())){

            return $this->sendChangeFailedResponse(__('auth.pass_not_correct'));
        }

        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        $this->changePassword($user, $request->get('password'));

        return $this->sendChangeResponse(__('auth.pass_changed_successfully'));
    }

    protected function rules()
    {
        return [
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

    protected function validationErrorMessages()
    {
        return [];
    }

    protected function credentials(Request $request)
    {
        return $request->only(
            'current_password', 'password', 'password_confirmation'
        );
    }

    protected function changePassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        $this->guard()->login($user);
    }

    protected function sendChangeResponse($response)
    {
        return redirect($this->redirectPath())
            ->with('status', trans($response));
    }

    protected function sendChangeFailedResponse($response)
    {
        return redirect()->back()->withErrors(['current_password' => trans($response)]);
    }

    public function broker()
    {
        return Password::broker();
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
