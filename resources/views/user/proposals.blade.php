@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.your_proposals')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(!Auth()->user()->proposals->count())
                        <p>{{__('user.no_sub_prop')}}</p>
                    @endif
                    @foreach(Auth()->user()->proposals as $proposal)
                        @if(!$proposal->project->completed_by_user_id)
                            <div class="job">
                                <div class="top-sec">
                                    <div class="col-lg-12">
                                        <h4>
                                            <a href="{{route('projects.show', ['id' => $proposal->project->id])}}">{{$proposal->project->title}}</a>
                                        </h4>
                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.row -->

                                <div class="row mid-sec">
                                    <div class="col-lg-12">
                                        <div class="col-lg-12">
                                            <hr class="small-hr">
                                            <p>{{strlen($proposal->content) > 300 ? substr($proposal->content, 0, 300) . "..." : $proposal->content}}</p>
                                        </div><!-- /.col-lg-12 -->
                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.row -->

                                <div class="row bottom-sec">
                                    <div class="col-lg-12">

                                        <div class="col-lg-12">
                                            <hr class="small-hr">
                                        </div>

                                        <div class="col-lg-3 col-sm-3">
                                            <h5> {{__('user.posted')}} </h5>
                                            <p> {{ $proposal->created_at->diffForHumans()}}</p>
                                        </div>
                                        <div class="col-lg-2 col-sm-3">
                                            <h5> {{__('user.bid')}} </h5>
                                            <p>{{ $proposal->bid}}kr</p>
                                        </div>
                                        <div class="col-lg-5 col-sm-6 pull-right buttons-holder">
                                            <form onSubmit="return confirm('Are you sure you wish to delete this proposal?');" method="POST"
                                                  class="pull-right" style="margin-left: 15px;"
                                                  action="{{route('profile.proposals.destroy',
                                         ['id' =>
                                        $proposal->id])
                                        }}">
                                                {{csrf_field()}}
                                                <button type="submit" class="kafe-btn kafe-btn-mint-small"><i class="fa fa-trash"
                                                                                                              aria-hidden="true"></i> {{__('user.delete')}}
                                                </button>
                                            </form>
                                            <a href="{{route('profile.proposals.edit', ['id' => $proposal->id])}}" class="kafe-btn kafe-btn-mint-small
                                        pull-right
"><i
                                                        class="fa
                                        fa-pencil-square-o"
                                                        aria-hidden="true"></i>
                                                {{__('user.edit_proposal')}}</a>

                                        </div>
                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.row -->

                            </div><!-- /.job -->
                        @endif
                    @endforeach
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection