<?php

return [
    'moto' => 'Hjælpen er kun få taster væk',
    'hire' => 'Opret opgave',
    'hire_the_best' => 'Her får du professionel hjælp.',
    'work' => 'Søg arbejde',
    'browse_freelance' => 'Hvad søger du?',
    'fc' => 'Virksomheder',
    'view_all' => 'VIS ALLE KATEGORIER',
    'text' => 'Vi hjælper dig med at finde kompetent arbejdskræft til din opgave!',
];
