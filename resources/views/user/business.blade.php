@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.profile')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    <form class="form-horizontal white-form" method="POST" action="{{ route('profile.business.update') }}">
                        {{ csrf_field() }}

                        <input id="cvr" type="text" class="form-control" name="cvr" placeholder="CVR"
                               value="{{ $businessSettings ? $businessSettings->cvr : ''}}"
                               required autofocus>

                        @if ($errors->has('cvr'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('cvr') }}</strong>
                                    </span>
                        @endif

                        <input id="name" type="text" class="form-control" name="name"
                               value="{{ $businessSettings ? $businessSettings->name : ''}}"
                               required autofocus placeholder="{{__('user.company_name')}}">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif

                        <input id="owner_first_name" type="text" class="form-control"
                               name="owner_first_name"
                               value="{{ $businessSettings ? $businessSettings->owner_first_name : ''}}"
                               required autofocus placeholder="{{__('user.owner_first_name')}}">

                        @if ($errors->has('owner_first_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('owner_first_name') }}</strong>
                                    </span>
                        @endif

                        <input id="owner_last_name" type="text" class="form-control"
                               name="owner_last_name"
                               value="{{ $businessSettings ? $businessSettings->owner_last_name : ''}}"
                               required autofocus placeholder="{{__('user.owner_last_name')}}">

                        @if ($errors->has('owner_last_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('owner_last_name') }}</strong>
                                    </span>
                        @endif

                        <input id="address" type="text" class="form-control" name="address"
                               value="{{ $businessSettings ? $businessSettings->address : ''}}" required placeholder="{{__('user.address')}}">

                        @if ($errors->has('address'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                        @endif

                        <input id="email" type="email" class="form-control" name="email"
                               value="{{ $businessSettings ? $businessSettings->email : ''}}" required placeholder="{{__('auth.email')}}">

                        <input id="phone" type='tel' class="form-control" name="phone"
                               value="{{ $businessSettings ? $businessSettings->phone : ''}}"
                               required autofocus placeholder="{{__('auth.phone')}}">

                        @if ($errors->has('phone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif

                        <div class="text-center">
                            <!-- Send button -->
                            <button type="submit" class="kafe-btn kafe-btn-mint full-width">{{__('user.update')}}</button>
                        </div>
                    </form>
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection
