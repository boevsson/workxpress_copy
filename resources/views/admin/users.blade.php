@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('admin.users')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <h3>{{__('admin.users')}}</h3>
                <form method="post" action="{{route('admin.searchUser')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4 search-field">
                            <input value="{{(isset($id_criteria)) ? $id_criteria : ''}}" name="user_id" type="text"
                                   class="form-control"
                                   placeholder="Bruger ID">
                        </div>
                        <div class="col-md-4 search-field">
                            <input value="{{(isset($email_criteria)) ? $email_criteria : ''}}" name="email" type="text"
                                   class="form-control"
                                   placeholder="E-mail">
                        </div>
                        <div class="col-md-4">
                            <button class="kafe-btn kafe-btn-mint-small" type="submit">Søg</button>
                            <a href="{{route('admin.users')}}" class="kafe-btn kafe-btn-mint-small">Nulstil</a>
                        </div>
                    </div>
                </form>
                @if($users->count())
                    <table class="table table-responsive table-hover table-project">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Navn</th>
                            <th>E-mail</th>
                            <th>Telefon</th>
                            <th>Abonnement</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>
                                    {{$user->first_name}} {{$user->last_name}}
                                </td>
                                <td>
                                    {{$user->email}}
                                </td>
                                <td>
                                    {{$user->phone}}
                                </td>
                                <td class="subscription-column">
                                    @if($user->subscription != 'null' && isset($user->subscription->subscription_name))
                                        <form style="max-width: 160px; float: left;" method="POST" action="{{route('admin.users.changeSubscription',
                                    ['userId' =>
                                    $user->id])}}">
                                            {{ csrf_field() }}
                                            <select class="form-control" onchange="this.form.submit()"
                                                    name="subscription_type_id">
                                                <option value="">{{__('admin.no_active')}}</option>
                                                @foreach($subscription_types as $subscription_type)
                                                    <option value="{{$subscription_type->id}}" {{($subscription_type->id == $user->subscription->id) ? 'selected' : ''

                                                    }}>{{$subscription_type->subscription_name}}</option>
                                                @endforeach
                                            </select>
                                        </form>
                                    @else
                                        <form style="max-width: 160px; float: left;" method="POST" action="{{route('admin.users.changeSubscription',
                                    ['userId' => $user->id])}}">
                                            {{ csrf_field() }}
                                            <select class="form-control" onchange="this.form.submit()"
                                                    name="subscription_type_id">
                                                <option value="" selected>{{__('admin.no_active')}}</option>
                                                @foreach($subscription_types as $subscription_type)
                                                    <option value="{{$subscription_type->id}}">{{$subscription_type->subscription_name}}</option>
                                                @endforeach
                                            </select>
                                        </form>
                                    @endif
                                    <form class="ban-user" action="{{route('admin.banUser', ['id' => $user->id])}}"
                                          method="post">
                                        {{ csrf_field() }}
                                        @if($user->suspended)
                                            <button class="kafe-btn kafe-btn-mint-small"
                                                    type="submit">{{__('admin.unban')}}</button>
                                        @else
                                            <button class="kafe-btn kafe-btn-mint-small"
                                                    type="submit">{{__('admin.ban')}}</button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p style="text-align: center; margin-top: 50px;"><strong>{{__('admin.not_found')}}</strong></p>
                @endif
                <div class="page text-center">
                    {{ $users->links() }}
                </div><!-- /.page -->
            </div>
        </div>
    </div>
@endsection
