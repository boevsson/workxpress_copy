@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12 contact-header">
        <h1 style="text-align: center"> {{ $thread->subject }}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container messenger-show">
        <div class="col-md-12 white-2">
            <h2>{{ $thread->subject }}</h2>
            <div class="col-lg-6">
                @each('messenger.partials.messages', $thread->messages, 'message')
            </div>
            <div class="col-lg-6">
                @include('messenger.partials.form-message')
            </div>
        </div>
    </div>
@stop
