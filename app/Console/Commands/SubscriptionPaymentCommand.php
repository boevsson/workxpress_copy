<?php

namespace App\Console\Commands;

use App\LaterSubscriptionOrder;
use App\Services\Services;
use App\SubscriptionOrder;
use App\SubscriptionType;
use App\User;
use Faker\Provider\DateTime;
use Illuminate\Console\Command;

class SubscriptionPaymentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:pay';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscriptions payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        $paymentsDone = 0;

        for ($i = 0; $i < count($users); $i++) {

            $user = $users[$i];

            if (!$user->subscription) {
                continue;
            }

            $subscription_order = SubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();

            if ($subscription_order) {

                $expirationDate = $subscription_order->expire_at;

                if (!$expirationDate) {
                    continue;
                }

                $expirationDate = new \DateTime($expirationDate);
                $expirationDate = $expirationDate->format('d/m/Y');

                $today = new \DateTime();
                $today = $today->format('d/m/Y');;

                if ($expirationDate === $today) {

                    $price = $user->subscription->price;
                    $later_subscription_order = LaterSubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();

                    if ($subscription_order->type === 'payed') {

                        if ($later_subscription_order) {

                            $price = $later_subscription_order->subscription->price;
                        }

                        $authorizePayment = Services::authorizePayment($subscription_order, $user->id, $price);

                        if ($authorizePayment) {

                            $paymentsDone++;

                            $this->upgradeSubscription($subscription_order, $later_subscription_order, $user);
                        }
                    } else if ($subscription_order->type === 'free') {

                        if ($later_subscription_order) {

                            $price = $later_subscription_order->subscription->price;

                            $authorizePayment = Services::authorizePayment($subscription_order, $user->id, $price);

                            if ($authorizePayment) {

                                $paymentsDone++;

                                $this->upgradeSubscription($subscription_order, $later_subscription_order, $user);
                            }
                        } else {

                            //Remove subscription if free and expired
                            $subscription_order->status = 'canceled';
                            $subscription_order->save();

                            $user->subscription_type_id = null;
                            $user->save();
                        }
                    }
                }
            }
        }

        $now = new \DateTime();

        echo $now->format('Y-m-d H:i:s') . ": $paymentsDone Payments done! \n";
        return;
    }

    public function upgradeSubscription($subscription_order, $later_subscription_order, $user)
    {
        $months = $user->subscription->months;

        if($later_subscription_order){

            $user->subscription_type_id = $later_subscription_order->subscription->id;
            $user->save();

            $later_subscription_order->status = 'done';
            $later_subscription_order->save();

            $months = $later_subscription_order->subscription->months;
        }

        //Set new expiration date
        $intervalString = 'P' . $months . 'M';
        $newExpirationDate = new \DateTime();
        $newExpirationDate = $newExpirationDate->add(new \DateInterval($intervalString));

        $subscription_order->expire_at = $newExpirationDate;
        $subscription_order->type = 'payed';
        $subscription_order->save();
    }
}
