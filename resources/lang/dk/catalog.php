<?php

return [
    'catalog' => 'Kategorier',
    'bfs' => 'Vælg en kategori',
    'freelancers' => 'Virksomheder',
    'filters' => 'Sorter',
    'filter' => 'Sorter',
    'choose_category' => 'Vælg en kategori',
    'select_category' => 'Vælg venligst kategori',
    'choose_city' => 'Vælg region',
    'no_freelancers' => 'Der er ingen virksomheder fundet',
];