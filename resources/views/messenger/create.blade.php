@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12 contact-header">
        <h1 style="text-align: center"> {{__('messenger.create')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container create-message">

            <div class="col-md-12 white-2">
                <form action="{{ route('messages.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-6 col-md-offset-3">
                        <!-- Subject Form Input -->
                        @if($user->businessSettings)
                            <h3>{{__('messenger.send_msg')}} {{$user->businessSettings->name}}</h3>
                        @else
                            <h3>{{__('messenger.send_msg')}} {{$user->first_name}} {{$user->last_name}}</h3>
                        @endif
                        <div class="form-group">
                            <label class="control-label">Emne</label>
                            <input type="text" class="form-control" name="subject" placeholder="Emne"
                                   value="{{ old('subject') }}">
                        </div>

                        <!-- Message Form Input -->
                        <div class="form-group">
                            <label class="control-label">{{__('messenger.msg')}}</label>
                            <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                        </div>

                        <input type="hidden" value="{{$user->id}}" name="recipient"/>

                        <!-- Submit Form Input -->
                        <div class="form-group">
                            <button type="submit" class="kafe-btn kafe-btn-mint btn-green full-width white-no-margin">{{__('messenger.submit')}}</button>
                        </div>
                    </div>
                </form>
            </div>
    </div>
@stop
