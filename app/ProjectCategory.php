<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProjectCategory extends Model
{
    protected $fillable = [
        'category_name',
    ];

    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function scopeTopLevel($query)
    {
        return $query->where('parent_id', 0);
    }

    public function scopeUserCategories($query)
    {
        $userProjectCategoriesIds = [];

        foreach (Auth::user()->projectCategories as $category) {
            $userProjectCategoriesIds[] = $category->id;
        }

        return $query->whereIn('id', $userProjectCategoriesIds);
    }

    public function scopeName($query, $name)
    {
        return $query->where('category_name', 'like', '%' . $name . '%');
    }

    public function scopeChildren($query, $parentId)
    {
        if ($parentId) {
            return $query->where('parent_id', $parentId);
        }
    }
}
