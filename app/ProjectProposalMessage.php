<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectProposalMessage extends Model
{
    protected $fillable = [
        'proposal_id',
        'user_id',
        'message'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project', 'proposal_id');
    }
}
