<div id="sidebar" class="col-lg-4">
    <div class="list">
        <div class="list-group">
            <span class="list-group-item active cat-list">
                <em class="fa fa-fw fa-user-o"></em> {{__('user.profile')}}
            </span>
            <a href="{{ route('profile.show', ['id' => $user->id]) }}" class="list-group-item cat-list">
                <em class="fa fa-fw fa-tachometer"></em>&nbsp;&nbsp;&nbsp;{{__('user.overview')}}
            </a>
            <a href="{{ route('profile.showCompletedProjects', ['id' => $user->id]) }}" class="list-group-item cat-list">
                <em class="fa fa-fw fa-hourglass-end"></em>&nbsp;&nbsp;&nbsp;{{__('user.completed_tasks')}}
            </a>
        </div><!-- ./.list-group -->
    </div><!-- ./.list -->
</div><!-- ./.col-lg-4 -->