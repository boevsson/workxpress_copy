@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('admin.info')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <a href="{{route('admin.informationPages.create')}}" class="kafe-btn kafe-btn-mint-small">{{__('admin.create_page')}}</a>

                @if($information_pages->count())
                    <table class="table table-hover table-project information-page-table">
                        <thead>
                        <tr>
                            <th>{{__('admin.title')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($information_pages as $page)
                            <tr>
                                <td>
                                    {{$page->title}}
                                </td>
                                <td style="width: 170px">
                                    <form style="margin-left: 10px; float: right;" onSubmit="return confirm('Are you sure you wish to delete this ' +
                                     'page?');"
                                          action="{{route('admin.informationPages.delete', ['id' => $page->id])}}" method="post">
                                        {{csrf_field()}}
                                        <button type="submit" class="pull-right kafe-btn kafe-btn-mint-small"><i class="fa fa-trash"
                                                                                                                 aria-hidden="true"></i>
                                        </button>
                                    </form>
                                    <a href="{{route('admin.informationPages.edit', ['id' => $page->id])}}" class="kafe-btn kafe-btn-mint-small
                                    pull-right">{{__('admin.edit')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p style="text-align: center; margin-top: 50px;"><strong> {{__('admin.no_infopage')}}</strong></p>
                @endif
                @if($information_pages->count() > 15)
                    <div class="page text-center">
                        {{ $information_pages->links() }}
                    </div><!-- /.page -->
                @endif
            </div>
        </div>
    </div>
@endsection
