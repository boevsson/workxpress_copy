<?php

namespace App\Http\Controllers\Auth;

use App\BusinessSetting;
use App\City;
use App\LaterSubscriptionOrder;
use App\Mail\Welcome;
use App\ProjectCategory;
use App\SubscriptionOrder;
use App\SubscriptionType;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm($subscriptionTypeId = null)
    {
        $subscription_types = SubscriptionType::all();

        $cities = City::all();
        $project_categories = ProjectCategory::topLevel()->get();

        return view('auth.register', compact('subscription_types', 'subscriptionTypeId', 'cities', 'project_categories'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'first_name.required' => __('validate.first_name.required'),
            'first_name.max:255' => __('validate.first_name.max:255'),
            'last_name.required' => __('validate.last_name.required'),
            'last_name.max:255' => __('validate.last_name.max:255'),
            'email.required' => __('validate.email.required'),
            'email.max:255' => __('validate.email.max:255'),
            'phone.required' => __('validate.phone.required'),
            'phone.max:255' => __('validate.phone.max:255'),
            'cities.required' => 'Region er obligatorisk.',
            'password.required' => __('validate.password.required'),
            'password.min:6' => __('validate.password.min:6'),
            'password.confirmed' => __('validate.password.confirmed'),
            'company_cvr.required' => __('validate.cvr.required'),
            'company_cvr.numeric' => __('validate.cvr.numeric'),
            'company_cvr.size' => __('validate.cvr.size'),
            'company_name.required' => __('validate.name.required'),
            'company_name.max:255' => __('validate.name.max:255'),
            'owner_first_name.required' => __('validate.owner_first_name.required'),
            'owner_first_name.max:255' => __('validate.owner_first_name.max:255'),
            'owner_last_name.required' => __('validate.owner_last_name.required'),
            'owner_last_name.max:255' => __('validate.owner_last_name.max:255'),
            'company_address.required' => __('validate.address.required'),
            'company_address.max:255' => __('validate.address.max:255'),
            'company_email.required' => __('validate.email.required'),
            'company_email.max:255' => __('validate.email.max:255'),
            'company_phone.required' => __('validate.phone.required'),
            'company_phone.max:255' => __('validate.phone.max:255'),
        ];

        if (isset($data['isCompany']) && $data['isCompany'] == 'on') {
            return Validator::make($data, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'phone' => 'required|string|max:255',
                'password' => 'required|string|min:6|confirmed',
                'cities' => 'required',
                'categories' => 'required',
                'company_cvr' => 'required|numeric|digits:8',
                'company_name' => 'required|string|max:255',
                'owner_first_name' => 'required|string|max:255',
                'owner_last_name' => 'required|string|max:255',
                'company_address' => 'required|string|max:255',
                'company_email' => 'required|string|max:255',
                'company_phone' => 'required|string|max:15',
            ], $messages);
        } else {
            return Validator::make($data, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'phone' => 'required|string|max:255',
                'password' => 'required|string|min:6|confirmed',
            ], $messages);
        }
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return redirect()->route('profile.subscription')->with('subscriptionTypeId', $user->subscription->id);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->password = bcrypt($data['password']);
        $user->receive_calls = (isset($data['receive_calls']) && $data['receive_calls'] == 'on') ? 1 : 0;
        $user->save();

        if (isset($data['isCompany']) && $data['isCompany'] == 'on') {

            $businessSettings = new BusinessSetting();
            $businessSettings->cvr = $data['company_cvr'];
            $businessSettings->name = $data['company_name'];
            $businessSettings->owner_first_name = $data['owner_first_name'];
            $businessSettings->owner_last_name = $data['owner_last_name'];
            $businessSettings->address = $data['company_address'];
            $businessSettings->email = $data['company_email'];
            $businessSettings->phone = $data['company_phone'];
            $businessSettings->user_id = $user->id;
            $businessSettings->save();

            $business = true;
        }

        $user->subscription_type_id = $data['subscription_type_id'];

        if ($data['categories']) {

            $user->projectCategories()->sync($data['categories']);
        }

        if ($data['cities']) {

            $user->cities()->sync($data['cities']);
        }

        $user->save();

        if ($business) {
            $user->business = true;
        }

        $expirationDate = new \DateTime();
        $expirationDate = $expirationDate->add(new \DateInterval('P1M'));

        SubscriptionOrder::create([
            'type' => 'free',
            'status' => 'active',
            'user_id' => $user->id,
            'expire_at' => $expirationDate
        ]);

        Mail::to($data['email'])->send(new Welcome($user));

        return $user;
    }
}
