@extends('layouts.master')

@section('content')
    <div class="main-container container" style="min-height: 350px;">
        <h1 style="text-align: center; margin-top: 100px;">{{__('errors.error')}}</h1>
    </div>
@endsection
