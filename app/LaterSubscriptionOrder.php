<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaterSubscriptionOrder extends Model
{
    protected $fillable = [
        'status',
        'user_id',
        'subscription_type_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subscription()
    {
        return $this->belongsTo('App\SubscriptionType', 'subscription_type_id');
    }
}
