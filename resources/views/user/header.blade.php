<header class="header-freelancer">
    @if($user->image != 'default.jpg')
        <div class="header-image-overlay"></div>
        <div style="background: url({{ URL::to('images/profile/'. $user->image) }});" class="header-freelancer-blur-image"></div>
    @endif
    <div class="container header-container">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <img src="{{ URL::to('images/profile/'. $user->image) }}" class="img-thumbnail img-responsive revealOnScroll"
                         data-animation="fadeInDown" data-timeout="200" alt="">
                    <h1 class="revealOnScroll" data-animation="bounceIn" data-timeout="200">
                        @if($user->businessSettings)
                            {{$user->businessSettings->name}}
                        @else
                            {{$user->first_name}} {{$user->last_name}}
                        @endif
                    </h1>
                    <p class="revealOnScroll" data-animation="fadeInUp" data-timeout="400">
                    <p>(Reviews: {{$user->ratings->count()}}) <br>
                        <span style="display: inline-block; margin-top: 10px;" class="rating-icon">
                                @if($user->avg_rating == 1)
                                <img src="{{asset('img/rating_icons/1.png')}}">
                            @elseif($user->avg_rating == 2)
                                <img src="{{asset('img/rating_icons/2.png')}}">
                            @elseif($user->avg_rating == 0)
                                <span>{{__('user.no_rating')}}</span>
                            @else
                                <img src="{{asset('img/rating_icons/3.png')}}">
                            @endif
                            </span>
                    </p>
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.content -->
    </div><!-- /.container -->
</header><!-- /header -->