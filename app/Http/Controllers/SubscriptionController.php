<?php

namespace App\Http\Controllers;

use App\LaterSubscriptionOrder;
use App\Mail\NewProject;
use App\Mail\SubscriptionPaid;
use App\Services\Services;
use App\SubscriptionOrder;
use App\SubscriptionOrderPayment;
use App\SubscriptionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SubscriptionController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $subscription_types = SubscriptionType::all();
        $subscription_order = SubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();
        $later_subscription_order = LaterSubscriptionOrder::where('user_id', Auth::id())->where('status', 'active')->first();

        return view('user.subscription', compact('user', 'subscription_types', 'subscription_order', 'later_subscription_order'));
    }

    public function upgradeLater(Request $request)
    {
        $laterSubscriptionOrder = LaterSubscriptionOrder::where('user_id', Auth::id())->where('status', 'active')->first();

        if ($laterSubscriptionOrder) {

            $laterSubscriptionOrder->status = 'canceled';
            $laterSubscriptionOrder->save();
        }

        LaterSubscriptionOrder::create([
            'status' => 'active',
            'user_id' => Auth::id(),
            'subscription_type_id' => $request->get('subscription_type_id')
        ]);

        return back()->with('status', __('subscription.subs_upgrade_save'));
    }

    public function upgradeLaterDelete()
    {
        $laterSubscriptionOrder = LaterSubscriptionOrder::where('user_id', Auth::id())->where('status', 'active')->first();

        if ($laterSubscriptionOrder) {

            $laterSubscriptionOrder->status = 'canceled';
            $laterSubscriptionOrder->save();
        }

        return back()->with('status', __('subscription.subs_canceled'));
    }

    public function change(Request $request)
    {
        $user = Auth::user();
        $subscription_type = SubscriptionType::where('id', $request->get('subscription_type_id'))->first();
        $subscription_order = SubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();

        $prev_subscription_price = $user->subscription->price;

        $laterSubscriptionOrder = LaterSubscriptionOrder::where('user_id', Auth::id())->where('status', 'active')->first();

        if ($laterSubscriptionOrder) {

            $laterSubscriptionOrder->status = 'canceled';
            $laterSubscriptionOrder->save();
        }

        $price = $subscription_type->price - $prev_subscription_price;

        $authorizePayment = Services::authorizePayment($subscription_order, $user->id, $price);

        if ($authorizePayment) {

            $user->subscription_type_id = $subscription_type->id;
            $user->save();

            $subscription_order->type = 'payed';
            $subscription_order->save();

            return back()->with('status', __('subscription.subs_successfully'));
        } else {

            return back()->withErrors(['response' => __('subscription.subs_cant_changed')]);
        }
    }

    public function accept(Request $request)
    {
        $user = Auth::user();

        $subscription_order = SubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();

        if ($request->get('amount') === '0') {

            if ($subscription_order) {

                $subscription_order->date = $request->get('date');
                $subscription_order->time = $request->get('time');
                $subscription_order->txnfee = $request->get('txnfee');
                $subscription_order->paymenttype = $request->get('paymenttype');
                $subscription_order->cardno = $request->get('cardno');
                $subscription_order->hash = $request->get('hash');
                $subscription_order->orderid = $request->get('orderid');
                $subscription_order->subscriptionid = $request->get('subscriptionid');
                $subscription_order->save();
            }

            LaterSubscriptionOrder::create([
                'status' => 'active',
                'user_id' => $user->id,
                'subscription_type_id' => $user->subscription->id
            ]);

            return redirect()->route('profile.subscription')->with('status', __('subscription.subs_payment_info_saved'));
        } else {

            if ($subscription_order) {

                $subscription_order->status = 'canceled';
                $subscription_order->save();
            }

            $subscription_type = SubscriptionType::where('price', $request->get('amount') / 100)->first();

            $intervalString = 'P' . $subscription_type->months . 'M';
            $expirationDate = new \DateTime();
            $expirationDate = $expirationDate->add(new \DateInterval($intervalString));

            $subscription_order = SubscriptionOrder::create([
                'date' => $request->get('date'),
                'time' => $request->get('time'),
                'txnfee' => $request->get('txnfee'),
                'paymenttype' => $request->get('paymenttype'),
                'cardno' => $request->get('cardno'),
                'hash' => $request->get('hash'),
                'status' => 'active',
                'type' => 'payed',
                'orderid' => $request->get('orderid'),
                'subscriptionid' => $request->get('subscriptionid'),
                'user_id' => Auth::id(),
                'expire_at' => $expirationDate
            ]);

            if ($subscription_type) {

                $user->subscription_type_id = $subscription_type->id;
                $user->save();
            }

            $subscription_order_payment = SubscriptionOrderPayment::create([
                'status' => 'success',
                'amount' => $request->get('amount'),
                'currency' => $request->get('currency'),
                'orderid' => $request->get('orderid'),
                'transactionid' => $request->get('txnid'),
                'subscription_order_id' => $subscription_order->id,
                'user_id' => Auth::id()
            ]);

            Mail::to($user->email)->send(new SubscriptionPaid($subscription_type));

            return redirect()->route('profile.subscription')->with('status', __('subscription.subs_successfully'));
        }
    }

    public function cancel(Request $request)
    {
        return redirect()->route('profile.subscription');
    }

    public function delete()
    {
        $user = Auth::user();

        if ($user && $user->subscription) {

            $subscription_order = SubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();

            if ($subscription_order) {

                //If the subscription is not a test one
                if ($subscription_order->type == 'payed') {

                    $response = Services::epayDeleteSubscription($subscription_order);

                    if ($response->deletesubscriptionResult) {

                        return $this->removeSubscription($subscription_order, $user);
                    } else {

                        return back()->withErrors(['response' => __('subscription.subs_cant_canceled')]);
                    }
                } else {
                    if ($subscription_order->type == 'free') {

                        return $this->removeSubscription($subscription_order, $user);
                    }
                }
            }
        }

        return back();
    }

    public function removeSubscription($subscription_order, $user)
    {
        $subscription_order->status = 'canceled';
        $subscription_order->save();

        $user->subscription_type_id = null;
        $user->save();

        return back()->with('status', __('subscription.subs_canceled'));
    }
}
