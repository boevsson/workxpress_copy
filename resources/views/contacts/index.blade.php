@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12 contact-header">
        <h1 style="text-align: center"> {{__('contacts.contact')}}</h1>
    </div>
@endsection

@section('content')
    <div class="map">
        <div class="container-fluid">
            <div class="row">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50961.954612157715!2d12.103588128894307!3d55.632961028902216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46525e51463bcadd%3A0xf39b80754851d38f!2sLink%C3%B8pingvej+22%2C+4000+Roskilde%2C+Denmark!5e0!3m2!1sen!2sbg!4v1519045198574" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.map -->
    </div>

    <section class="contact">
        <div class="main-container container contact-page">
            <div class="row">
                @include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
                <div class="text-center">
                    <h3>{{__('contacts.questions')}}</h3>
                    <hr class="mint">
                    <p class="top-p">{{__('contacts.info')}}</p>
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- The contactform -->
                <form method="post" action="{{route('contacts.sendMail')}}" name="contactform" id="contactform"
                      class="white-form animations fade-up d3">
                    {{csrf_field()}}
                    <fieldset>
                        <div class="col-lg-6">
                            <!-- Name -->
                            <input name="name" placeholder="Navn*" type="text" id="name" size="30" value=""/>

                            <!-- Email -->
                            <input name="email" type="text" placeholder="E-mail*" id="email" size="30" value=""/>

                            <!-- Phone -->
                            <input name="phone" placeholder="Telefon (valgfri)" type="text" id="phone" size="30" value=""/>
                        </div>
                        <div class="col-lg-6">
                            <!-- Comments / Message -->
                            <textarea placeholder="Besked*" name="message" id="comments"></textarea>
                            <!-- Send button -->
                            <button type="submit" class="kafe-btn kafe-btn-mint full-width">Send</button>
                        </div>
                    </fieldset>
                </form>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /section -->
@endsection