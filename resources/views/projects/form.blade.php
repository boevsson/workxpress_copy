<div id="errors-block" class="alert alert-danger">
    <ul id="errors">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        @endif
    </ul>
</div>
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
{{csrf_field()}}
<div class="row">

    <div id="create-project">
        <div class="col-md-6">
            <input value="{{$project->title ?: old('title')}}" name="title" type="text" class="form-control"
                   id="projectTitle"
                   placeholder="{{__('project.project_title')}}">

            <textarea name="description" id="projectDescription" class="form-control"
                      rows="4">{{$project->description ?: old('description')}}</textarea>

            @if(isset($project->project_category))
                <select onchange="getSubCategories()" name="project_category_id" class="form-control"
                        id="projectCategory">
                    <option value="0">{{__('project.select_task_category')}}</option>
                    @foreach($project_categories as $projectCategory)
                        <option value="{{$projectCategory->id}}" {{($projectCategory->id == $project->project_category_id || $projectCategory->id == old('project_category_id')) || $projectCategory->id == $project->project_category->parent_id ? 'selected' : ''}}>{{$projectCategory->category_name}}</option>
                    @endforeach
                </select>
            @else
                @if(isset($categoryId))
                    <select onchange="getSubCategories()" name="project_category_id" class="form-control"
                            id="projectCategory">
                        <option value="0">{{__('project.select_task_category')}}</option>
                        @foreach($project_categories as $projectCategory)
                            <option value="{{$projectCategory->id}}" {{($categoryId == $projectCategory->id || $projectCategory->id == old('project_category_id'))  ? 'selected' : ''}}>{{$projectCategory->category_name}}</option>
                        @endforeach
                    </select>
                @else
                    <select onchange="getSubCategories()" name="project_category_id" class="form-control"
                            id="projectCategory">
                        <option value="0">{{__('project.select_task_category')}}</option>
                        @foreach($project_categories as $projectCategory)
                            <option value="{{$projectCategory->id}}" {{($projectCategory->id == $project->project_category_id || $projectCategory->id == old('project_category_id'))  ? 'selected' : ''}}>{{$projectCategory->category_name}}</option>
                        @endforeach
                    </select>
                @endif
            @endif

            <select name="project_sub_category_id" class="form-control" id="subCategorySelect">

            </select>

            <select name="city_id" class="form-control" id="projectCity">
                <option value="">{{__('project.select_city')}}</option>
                @foreach($cities as $city)
                    <option value="{{$city->id}}" {{($city->id == $project->city_id || $city->id == old('city_id') ) ? 'selected' : ''}}>{{$city->city_name}}</option>
                @endforeach
            </select>

            <input value="{{$project->budget ?: old('budget')}}" name="budget" type="text" class="form-control"
                   id="projectBudget"
                   placeholder="{{__('project.budget_placeholder')}}">

            <select name="deadline" class="form-control" id="projectDeadline">
                <option value="as_soon_as_possible" {{($project->deadline == 'as_soon_as_possible') ? 'selected' : ''}}>
                    {{__('project.as_soon_as_possible')}}
                </option>
                <option value="within_one_month" {{($project->deadline == 'within_one_month') ? 'selected' : ''}}>
                    {{__('project.within_one_month')}}
                </option>
                <option value="within_two_months" {{($project->deadline == 'within_two_months') ? 'selected' : ''}}>
                    {{__('project.within_two_months')}}
                </option>
                <option value="within_three_months" {{($project->deadline == 'within_three_months') ? 'selected' : ''}}>
                    {{__('project.within_three_months')}}
                </option>
                <option value="more_than_three_months" {{($project->deadline == 'more_than_three_months') ? 'selected' : ''}}>
                    {{__('project.more_than_three_months')}}
                </option>
            </select>

            @if($project->id && !$project->rating && $project->executor)
                <label>{{__('project.task_completed')}} <input onchange="toggleRatingForm()" id="completed" type="checkbox"
                                                               name="completed" {{
            ($project->completed) ?
            'checked' :
            ''}}></label>

                <div id="rating-form">
                    <p>{{__('project.rate_executor')}}</p>
                    <div class="clearfix"></div>
                    <ul class="rate-area">
                        <label id="star-1" title="sad"><img src="{{asset
                    ('img/rating_icons/3.png')}}"><br><input type="radio" name="rating" value="3"/></label>
                        <label id="star-3" title="happy"><img src="{{asset
                    ('img/rating_icons/2.png')}}"><br><input type="radio" name="rating" value="2"/></label>
                        <label id="star-3" title="happy"><img src="{{asset
                    ('img/rating_icons/1.png')}}"><br><input type="radio" name="rating" value="1"/></label>
                    </ul>
                    <div class="clearfix"></div>
                    <textarea name="rating_comment" class="form-control"
                              rows="4" placeholder="Rating comment (optional)"></textarea>
                </div>
            @endif

            @if(Auth::check())
                @if($project->id)
                    <button id="submit-button" type="submit" class="kafe-btn kafe-btn-mint full-width">{{__('admin.save')}}</button>
                @else
                    <button id="submit-button" type="submit" class="kafe-btn kafe-btn-mint full-width">Send</button>
                @endif
            @else
                <button type="button" onclick="showRegisterForm()" class="kafe-btn kafe-btn-mint full-width">Næste</button>
            @endif
        </div>
        <div class="col-md-6">
            <div>
                <label for="file-upload" type="file" class="kafe-btn kafe-btn-mint">
                    <i class="fa fa-cloud-upload"></i> {{__('project.upload_photo')}}
                </label>
                <input onchange="photosToUpload()" id="file-upload" type="file" name="photos[]" class="file-upload"
                       multiple/>
            </div>
            <div id="project-photos" class="masonry">
                @foreach($project->photos as $photo)
                    <div class="project-image-holder">
                        <div class="img-thumbnail project-photo text-center">
                            <div>
                                <img src="{{ URL::to('images/projects/'. $photo->thumb_image) }}">
                            </div>
                            <a class="remove-img" href="{{route('projects.photo.delete', ['id' => $photo->id])}}"><i
                                        class="fa fa-trash-o"
                                        aria-hidden="true"></i> {{__('project.remove_img')}}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div id="category-description"></div>
        </div>
    </div>
    @if(!Auth::check())
        <div id="register-from-project">
            <div class="col-md-6">
                <input onchange="bindFirstName(this)" value="{{old('first_name')}}" name="first_name" placeholder="{{__('auth.first_name')}}"
                       class="form-control" type="text">
                @if ($errors->has('first_name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                @endif

                <input onchange="bindLastName(this)" value="{{old('last_name')}}" name="last_name" placeholder="{{__('auth.last_name')}}"
                       class="form-control" type="text">
                @if ($errors->has('last_name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                @endif

                <input value="{{old('phone')}}" name="phone" placeholder="{{__('auth.phone')}}" class="form-control" type="text">
                @if ($errors->has('phone'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                @endif

                <input value="{{old('email')}}" name="email" placeholder="{{__('auth.email')}}" class="form-control" type="text">
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif

                <input name="password" placeholder="{{__('auth.password')}}" class="form-control" type="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif

                <input name="password_confirmation" placeholder="{{__('auth.confirm_password')}}" class="form-control" type="password">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                @endif

                <div class="form-row" style="height: 18px;">

                    <label style="float: left; color:#777;" class="label dark-label">Jeg vil gerne ringes op af virksomhederne. <input
                                type="checkbox"
                                name="receive_calls"></label>
                </div>
                <div class="clearfix"></div>
                <label style="display: none;" class="company-label">Dette er en firmakonto. <input {{($errors->has('company_cvr') ||
                                        $errors->has('company_name') || $errors->has('owner_first_name') || $errors->has('owner_last_name') ||
                                        $errors->has('company_address') || $errors->has('company_email') || $errors->has('company_phone')) ?
                                        'checked'
                                        : ''}}
                                                                                                   id="isCompany" onchange="toggleCompanyForm()"
                                                                                                   name="isCompany"
                                                                                                   type="checkbox"></label>
            </div>
            <div class="col-md-6 responsive-firm-form">
                <div id="company-form">
                    <input id="cvr" type="text" class="form-control" value="{{old('company_cvr')}}" name="company_cvr"
                           placeholder="CVR">
                    @if ($errors->has('company_cvr'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('company_cvr') }}</strong>
                                    </span>
                    @endif

                    <input id="name" type="text" class="form-control" value="{{old('company_name')}}" name="company_name"
                           placeholder="{{__('user.company_name')}}">

                    @if ($errors->has('company_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('company_name') }}</strong>
                        </span>
                    @endif

                    <input id="owner_first_name" type="text" class="form-control" value="{{old('owner_first_name')}}" name="owner_first_name"
                           placeholder="{{__('user.owner_first_name')}}">

                    @if ($errors->has('owner_first_name'))
                        <span class="help-block">
                             <strong>{{ $errors->first('owner_first_name') }}</strong>
                        </span>
                    @endif

                    <input id="owner_last_name" type="text" class="form-control"
                           value="{{old('owner_last_name')}}" name="owner_last_name"
                           placeholder="{{__('user.owner_last_name')}}">

                    @if ($errors->has('owner_last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('owner_last_name') }}</strong>
                        </span>
                    @endif


                    <input id="address" type="text" class="form-control" value="{{old('company_address')}}"
                           name="company_address"
                           placeholder="{{__('user.address')}}">

                    @if ($errors->has('company_address'))
                        <span class="help-block">
                             <strong>{{ $errors->first('company_address') }}</strong>
                        </span>
                    @endif

                    <input id="email" type="email" class="form-control" value="{{old('company_email')}}"
                           name="company_email"
                           placeholder="{{__('auth.email')}}">
                    @if ($errors->has('company_email'))
                        <span class="help-block">
                             <strong>{{ $errors->first('company_email') }}</strong>
                        </span>
                    @endif

                    <input id="phone" type='tel' class="form-control" value="{{old('company_phone')}}" name="company_phone"
                           placeholder="{{__('auth.phone')}}">

                    @if ($errors->has('company_phone'))
                        <span class="help-block">
                             <strong>{{ $errors->first('company_phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <button id="submit-button" type="submit" class="kafe-btn kafe-btn-mint full-width">Send</button>
            </div>
        </div>
    @endif
</div>

@section('scripts')
    <script>

        @if(!Auth::check())
        function toggleCompanyForm() {
            var completedCheckbox = document.getElementById("isCompany").checked;

            var ratingForm = document.getElementById("company-form");
            if (completedCheckbox) {
                ratingForm.style.display = 'block';
            } else {
                ratingForm.style.display = 'none';
            }
        }

        toggleCompanyForm();

        @endif

        function showRegisterForm() {

            console.log('showRegisterForm');

            var registerForm = document.getElementById('register-from-project');
            var projectForm = document.getElementById('create-project');
            var errorsList = document.getElementById('errors-block');

            //validate the project form fields if correct, display the register form

            var titleField = document.getElementById('projectTitle');
            var descriptionField = document.getElementById('projectDescription');
            var categoryField = document.getElementById('projectCategory');
            var cityField = document.getElementById('projectCity');
            var budgetField = document.getElementById('projectBudget');

            titleValue = (titleField.value.length) ? titleField.value : null;
            descriptionValue = (descriptionField.value.length) ? descriptionField.value : null;
            categoryValue = (categoryField.value.length && categoryField.value != 0) ? categoryField.value : null;
            cityValue = (cityField.value.length) ? cityField.value : null;
            budgetValue = (budgetField.value.length) ? budgetField.value : null;

            var constraints = {
                title: {
                    presence: {message: "^Titelfeltet er obligatorisk."},
                    length: {
                        maximum: 255,
                        message: "^Titlen må ikke være længere end 255 tegn."
                    }
                },
                description: {
                    length: {
                        maximum: 2500,
                        message: "^Titlen må ikke være længere end 2500 tegn."
                    }
                },
                category: {
                    presence: {message: "^Kategorien feltet er obligatorisk."},
                },
                city: {
                    presence: {message: "^Region er obligatorisk."},
                },
                budget: {
                    numericality: {message: "^Budgettet skal være tal."}
                }
            };


            var errors = validate({
                title: titleValue,
                description: descriptionValue,
                category: categoryValue,
                city: cityValue,
                budget: budgetValue
            }, constraints)

            console.log(errors);

            $('#errors').empty();

            if (errors) {

                errorsList.style = 'display: block;';

                if (errors.budget) {
                    //display budget errors
                    for (var i = 0; i < errors.budget.length; i++) {
                        $('#errors').append("<li>" + errors.budget[i] + "</li>");
                    }
                    console.log(errors.budget)
                }

                if (errors.category) {
                    //display category errors
                    for (var i = 0; i < errors.category.length; i++) {
                        $('#errors').append("<li>" + errors.category[i] + "</li>");
                    }
                    console.log(errors.category)
                }

                if (errors.city) {
                    //display city errors
                    for (var i = 0; i < errors.city.length; i++) {
                        $('#errors').append("<li>" + errors.city[i] + "</li>");
                    }
                    console.log(errors.city)
                }

                if (errors.title) {
                    //display title errors
                    for (var i = 0; i < errors.title.length; i++) {
                        $('#errors').append("<li>" + errors.title[i] + "</li>");
                    }
                    console.log(errors.title)
                }
            }


            if (!errors) {
                errorsList.style = 'display: none;';
                projectForm.style = 'display: none;';
                registerForm.style = 'display: block;';
            }

        }

        @if($errors->has('password') || $errors->has('phone') || $errors->has('email') || $errors->has('last_name') || $errors->has('first_name') || $errors->has('company_cvr') || $errors->has('company_name') || $errors->has('owner_first_name') || $errors->has('owner_last_name') ||
        $errors->has('company_address') || $errors->has('company_email') || $errors->has('company_phone'))
        console.log('32');
        showRegisterForm();

        @endif

        function getSubCategories() {
            var subCategorySelect = document.getElementById('subCategorySelect');
            var select = document.getElementById('projectCategory');
            showCategoryDescription(parseInt(select.value));
                    @if($project->project_category_id)
            var projectCategoryId = {{$project->project_category_id}};
                    @else
            var projectCategoryId = null;
            @endif

            $('#subCategorySelect').empty();

            //check for sub categories
            if (select.value > 0) {
                axios.get('/catalog/get-sub-categories/' + select.value)
                    .then(function (response) {
                        if (response.data.length) {
                            for (var i = 0; i < response.data.length; i++) {
                                if (projectCategoryId == response.data[i].id) {
                                    $('#subCategorySelect').prepend(
                                        '<option selected value=' + response.data[i].id + ' class="project-image-holder">' +
                                        response.data[i].category_name
                                        +
                                        '</option>'
                                    );
                                } else {
                                    $('#subCategorySelect').prepend(
                                        '<option value=' + response.data[i].id + ' class="project-image-holder">' +
                                        response.data[i].category_name
                                        +
                                        '</option>'
                                    );
                                }

                            }
                            subCategorySelect.style = "display:block;";

                        } else {
                            subCategorySelect.style = "display:none;";
                            $('#subCategorySelect').empty();
                        }
                    })
                    .catch(function (error) {
                    });
            } else {
                //hide the subcategories <select>
                subCategorySelect.style = "display:none;";
                $('#subCategorySelect').empty();
            }
        }

        getSubCategories();

        function showCategoryDescription(categoryId) {
            var descriptionContainer = document.getElementById("category-description");
            console.log(categoryId);
            switch (categoryId) {
                case 1: {
                    descriptionContainer.innerHTML = '<p><strong>Har du brug for en advokat?</strong></p>\n' +
                        '                <p>\n' +
                        '                Der kan være mange forskellige årsager til hvorfor man har brug for en advokat. Det kan være\n' +
                        '                    firmarelaterede årsager, hvor du måske har brug for hjælp til at gennemskue en kontrakt, eller skal\n' +
                        '                    indhente en udeblivende betaling. Det kan også tænkes at du har brug for juridisk støtte i din\n' +
                        '                    skilsmisse, eller der er opstået problematikker i forhold til dit lejemål. Ligegyldig din problemstilling,\n' +
                        '                    kan WorkXpress finde den rigtige advokat, der er tilpasset dit behov og ønske. Upload din opgave\n' +
                        '                    direkte på siden og dermed informere de bedste advokater til præcis din problemstilling.\n' +
                        '                    Alle kan få brug for en advokat. Advokaten sørger blandt andet for at du får den rette rådgivning alt\n' +
                        '                    efter hvilket behov, du har. Om det drejer sig om konfliktmægling, dødsboskifte,\n' +
                        '                    tvangsanbringelse, familietjek eller køb af fast ejendom, så sørger vi for at du får den advokat, der\n' +
                        '                    kan hjælpe dig med din problemstilling.</p>\n' +
                        '                <p><strong>Få hurtig juridisk hjælp</strong></p>\n' +
                        '                <p>\n' +
                        '                Det kan være svært at gennemskue hvilken advokat, der er den bedste til at håndtere din opgave -\n' +
                        '                og det kan måske være uoverskueligt at sætte sig ind i under visse omstændigheder. Derfor vil\n' +
                        '                    WorkXpress rigtig gerne hjælpe dig, så du hurtigt og nemt kommer videre i dit liv, uden yderligere\n' +
                        '                    problematikker. Det eneste, du skal gøre, er, at udfylde skemaet med dine oplysninger, samt hvad\n' +
                        '                    du har brug for juridisk rådgivning til, og vi hjælper dig med at finde den rette til opgaven.\n' +
                        '                    Dit behov for en advokat behøver ikke kun at tage udgangspunkt i dit privatliv førend WorkXpress\n' +
                        '                    kan hjælpe. Vi kan nemt og hurtigt hjælpe dig med at få kontakt til en ekspert ud i\n' +
                        '                    erhvervsmæssige anliggender. Udfyld skemaet med dine oplysninger og selve opgaven, og vi\n' +
                        '                    finder frem til den advokat, der kan hjælpe dig hurtigt og enkelt.</p>';
                }
                    break;
                case 8: {
                    descriptionContainer.innerHTML = "<p><strong>Alarm og tyverisikring - så er dit hjem " +
                        "sikret!</strong></p><p>Har du prøvet at have ubudne gæster? Eller bliver du nervøs, når du læser og hører om andres\n" +
                        "besøg af fremmede mennesker i deres hjem? Så bør du bestemt få en alarm i og tyverisikring af\n" +
                        "dit hjem med det samme. Intet er vigtigere end at føle sig tryg og tilpas i sit eget hjem. Vi kan\n" +
                        "hjælpe dig med at sikre dit hjem, så du ikke skal bekymre dig om uinviterede besøgende.</p><p>For at undgå tyveri i dit private hjem, er det en god idé at få installeret en alarm inden uheldet er\n" +
                        "ude. Dette gælder ikke kun dine døre, men også dine vinduer. Det er ikke til at vide, hvilken\n" +
                        "indgang tyven vælger at tage i brug. Det skaber en større tryghed. Vi sikre dig, at du får en ekspert\n" +
                        "til, at byde ind på netop din opgave.</p><p><strong>Skab tryghed for dig og din familie</strong></p><p>Der er ingen tvivl om at en af grundende til antallet af indbrud i de danske hjem skyldes\n" +
                        "danskernes manglende alarmer og tyverisikringer i deres egne hjem. Med over 40.000 indbrud om\n" +
                        "året er der ingen, der kan føle sig sikre uden en tyverialarm. Man har fundet ud af at over\n" +
                        "halvdelen af alle registrerede indbrud foregår i dagtimerne mellem 8 og 16, som er tidsrummet,\n" +
                        "hvor husejerne er på arbejde - og det er tyvene også. Der er altså rigeligt grundlag for, at få alarm\n" +
                        "og tyverisikret sit hjem, hvilket kan ske her på WorkXpress.</p><p>Vil du også skabe tryghed i dit hjem, så du og din familie kan føle jer sikre? Udfyld skemaet, og lad\n" +
                        "WorkXpress finde den helt rigtige løsning til dig og dit hjem, så I længere behøver at spekulere på\n" +
                        "ubudne gæster.</p>";
                }
                    break;
                case 10: {
                    descriptionContainer.innerHTML = "<p><strong>Find en kvalificeret arkitekt hos WorkXpress!</strong></p>\n" +
                        "<p>Har du fanget dig selv i at skitsere huse på linjeret papir? Måske du allerede har en god\n" +
                        "byggegrund for øje? Du behøver ikke vente længere! WorkXpress kan finde den helt rigtige\n" +
                        "arkitekt til dit byggeprojekt. Med en arkitekt sikrer du dig god faglig viden, som samtidig kan spæde\n" +
                        "op med rådgivning, og ideer til udseende og funktioner i dit nybyggeri! Når arkitekten er færdige\n" +
                        "med tegningerne, fortsætter han med at engagere sig i selve byggeriet ved at have en løbende\n" +
                        "dialog med bygherre og håndværkere. Alt sammen for at sikre dig den bedst mulige kvalitet.</p>\n" +
                        "<p>\n" +
                        "Du overvejer måske at lave en tilbygning til dit hjem, men er i tvivl om hvor præcis tilbygningen\n" +
                        "skal ligge - eller hvordan det skal foregå. Så har du også brug for en arkitekt til at hjælpe dig med\n" +
                        "skitsering og rådgivning. WorkXpress kan sørge for at dine boligdrømme bliver til virkelig - hurtigt\n" +
                        "og nemt! Udfyld skemaet med dine ideer og tanker omkring opgaven, så finder vi frem til den\n" +
                        "arkitekt, der passer bedst til din opgave!</p>\n" +
                        "<p><strong>Arkitekter gør drømme til virkelighed!</strong></p>\n" +
                        "<p>\n" +
                        "Kommer du tit til at drømme over de flotte arkitekttegnede hjem i boligmagasinerne? Og hver gang\n" +
                        "slår det hen som en uopnåelig fantasi? Det behøver det ikke være! Hvis du fortæller os om dine\n" +
                        "mange drømme, kan vi sætte dig i kontakt med en arkitekt, der kan rådgive dig. På den måde\n" +
                        "kommer du hurtigt et skridt videre med din boligdrøm!</p>\n" +
                        "<p>\n" +
                        "Arkitekter sørger for at du får mulighed for at dine drømme ned på papir. Ikke nok med at de\n" +
                        "kommer med skitseforslag og hustegninger, men de kan også udforme 3-D visualiseringer af din\n" +
                        "boligdrøm. På den måde bliver det også nemmere for dig at se og forestille dig din husdrøm. Lad\n" +
                        "WorkXpress sørge for at finde den perfekte arkitekt til dit projekt!</p>";
                }
                    break;
                case 41: {
                    descriptionContainer.innerHTML = "<p><strong>Autoværksted, når det passer dig!</strong></p>\n" +
                        "<p>Har din bil brug for en kærlig hånd til bremserne? Måske bilen skal eftersynes, eller have sat\n" +
                        "vinterdæk på? Ligegyldigt hvad din bil har brug for, så kan vi hjælpe dig med at finde det helt\n" +
                        "rigtige autoværksted til din bil.</p>\n" +
                        "<p>\n" +
                        "Vi sørger for at din bil kommer til netop dén automekaniker, der er bedst til at håndtere din bil.\n" +
                        "Trænger bilen til et olieskift, skal automatgearet skyldes, eller måske trænger bilen til at få renset\n" +
                        "indsugninger eller dieseldysen? WorkXpress finder autoværkstedet til dig til favorable priser uden\n" +
                        "at du behøver at gå ned på kvaliteten. WorkXpress fungerer altså også, som en serviceportal der\n" +
                        "ikke kun finder de bedste priser, men også de mest kvalificerede til opgaven. Det skaber så meget\n" +
                        "mere overblik for dig og arbejdet i, at ligge en opgave op er minimal. Derfor skal du ikke tøve med,\n" +
                        "at få en professionel til, at se på din opgave.</p>\n" +
                        "<p><strong>Kun det bedste er godt nok til din bil</strong></p>\n" +
                        "<p>\n" +
                        "Det er ikke altid lige nemt at finde det rigtige autoværksted til at klare reparationen af din bil. Det\n" +
                        "kan både være tidskrævende og besværligt at skulle ringe rundt og forhøre sig hos de forskellige\n" +
                        "værksteder. Vi gør det nemmere for dig! Udfyld skemaet med info om din bil, samt hvad der skal\n" +
                        "laves på bilen, så finder vi frem til autoværkstedet, der passer bedst til dine behov.</p>\n" +
                        "<p>\n" +
                        "Det behøver ikke være mere besværligt. Vi sørger for det hele. Det eneste, du skal gøre, er at køre\n" +
                        "bilen hen til autoværkstedet. Så bliver det ikke nemmere. Udfyld skemaet nu, og så er du næsten\n" +
                        "allerede klar med en nysynet bil!</p>";
                }
                    break;
                case 11: {
                    descriptionContainer.innerHTML = "<p><strong>Søger du en brolægger?</strong></p>\n" +
                        "<p>En brolægger kan udføre mange opgaver inden for brolægning. Det er blandt andet dem, der\n" +
                        "sørger for udsmykningen af veje, gader og indkørsler med udgangspunkt i naturens egne sten\n" +
                        "med blik for mønstre og former. Ønsker du en ny terrasse? Eller måske skal du have lagt en\n" +
                        "gangsti? Så har du brug for en brolægger - og vi kan hjælpe dig med at finde den helt rigtige! Vi\n" +
                        "gør det overskueligt for dig at finde den helt rigtige brolægger til at udføre din opgave!</p>\n" +
                        "<p>\n" +
                        "Brolæggeren skaber ikke kun nye gangarealer, men de reparerer også. Står du med knækkede\n" +
                        "fliser, eller er din indkørsel skæv og ujævn, så kan en brolægger klare opgaven. Det kan bare\n" +
                        "være svært at finde den rigtige til jobbet, så der hjælper vi dig naturligvis. Udfyld skemaet med din\n" +
                        "forestående opgave samt dine oplysninger, og så finder WorkXpress frem til en kvalificeret\n" +
                        "brolægger til dig.</p>\n" +
                        "<p><strong>Brolæggeren gør dig klar til sommer!</strong></p>\n" +
                        "<p>\n" +
                        "Sten og fliser kan nemt komme til at se trætte og slidte ud efter mange år som terasse. Dette\n" +
                        "skyldes især det danske vejr, der slider på fliserne. Der er ikke meget ved at sidde på en slidt\n" +
                        "terrasse med algepletter og mos i kanterne, når du skal nyde solen i sommerferien. Så er det nu\n" +
                        "du skal have fat i en brolægger, så du er klar til sommerens fester og søndagsbrunch på din nye\n" +
                        "terrasse!</p>\n" +
                        "<p>\n" +
                        "Brolæggeren kan også sørge for din indkørsel. Indkørslen er noget af det første, man bemærker,\n" +
                        "når man kommer på besøg. Det er derfor vigtigt at den er flot, vedligeholdt og dermed indbydende,\n" +
                        "når du får gæster, så de føler sig velkomne. Brolæggeren kan lægge en helt traditionel indkørsel,\n" +
                        "men hvorfor ikke lege lidt med fantasien og få lagt et flot mønster? Vi forsikrer dig om at mønsteret\n" +
                        "vil tiltrække opmærksomhed og mange komplimenter mange år frem! Udfyld skemaet med din\n" +
                        "opgave og informationer, så finder vi en dygtig og kvalificeret brolægger til dig!</p>";
                }
                    break;
                case 46: {
                    descriptionContainer.innerHTML = "<p><strong>Skal du bygge et nyt hus?</strong></p><p>Har du kastet dig over projekt ‘byg nyt hus’, så har du mulighed for at få tilknyttet en byggesagkyndig, der holder tilsyn med byggeriet. Det er nødvendigt for at undgå fatale fejl inden de bliver indbyggede i dit hus, og efterfølgende kan resultere i skadelige følger for dit hus i fremtiden. Det behøver ikke være et større projekt end en til- eller ombygning, eller måske bare et nyt tag, førend at det kan være en god idé med et byggetilsyn.</p><p>Med et byggetilsyn fra en byggesagkyndig får du hjælp med alt lige fra idéfase til indhentning af tilbud fra forskellige entreprenører, samtidig med at de sørger for at kvaliteten er i orden under byggeriet. Det er også byggetilsynet, der sørger for dialogen med entreprenøren under byggeriet, således at du sikrer dig kvalitet og sikkerhed. </p><p><strong>Find det rigtige byggetilsyn til dit behov</strong></p><p>WorkXpress hjælper dig med at finde det rette byggetilsyn til dit boligprojekt. Hvadenten du skal i gang med at lægge et tag eller bygge en ny udestue, kan vi sørge for at du får det helt rigtige tilsyn til dit byggeprojekt, så du kan være tryg og sikker på kvalitet. Du skal blot udfylde skemaet med din forestående opgave, dine ønsker samt oplysninger, og WorkXpress finder hurtigt og nemt dit byggetilsyn!</p><p>Ved at få fat i det perfekte boligtilsyn, sparer du dig selv for forsinkelser, fejl og mangler. Du slipper også for pludselige overraskelser i byggerier og ekstraregninger. En byggesagkyndig sørger for kvalitet i udførelsen af dit byggeprojekt ved at komme på anmeldte og uanmeldte besøg, så du kan føle dig tryg.</p>";
                }
                    break;
                case 12: {
                    descriptionContainer.innerHTML = "<p><strong>Catering til enhver lejlighed</strong></p><p>Catering er mad ud af huset lavet af professionelle og dygtige kokke. Catering kan bruges til alle formål lige fra bryllupper, barnedåb, festmiddage og firmafester. Det er kun din fantasi, der sætter grænsen. Er der mennesker og er der chance for at folk bliver sultne, vil du få stor glæde af et cateringfirma!</p><p>Et cateringfirma sørger for at du kan nyde dit selskab uden at skulle bruge tid og energi på maden. De tilbyder lækre råvarer og flotte anretninger til lige netop det arrangement, du har i tankerne. Smagen og kvaliteten vil til enhver tid være i topkvalitet hos cateringfirmaerne, så lad os hjælpe dig med at finde den helt perfekte catering til din begivenhed uden alt besværet.</p><p><strong>Catering til dig, der selv vil være til stede til festen</strong></p><p>Er du også en af dem, der bruger flere timer på at svede over den perfekte anretning til din fest? For derefter at løbe frem og tilbage mellem køkken og middagsselskabet for at servere lækker mad til dine gæster uden selv at få glæde af den? Vi kan hjælpe dig med at undgå sådan et scenarie! Hos os kan du bestille catering til din næste fest eller middagsselskab, så DU også kan nyde dine gæsters selskab uden at få sved på panden.</p><p>Alt du skal gøre er at udfylde skemaet med info omkring dit næste spisende selskab, dato og sted, samt andre ønsker til maden, så finder vi det bedste cateringfirma til dig. Vi kan allerede nu forsikre dig om at du kommer til at nyde din næste fest med lækker mad i ro og mag sammen med dine gæster! </p><p>Vil du slippe for tankemylderet og problematikkerne med at finde på og fremtrylle den helt rigtige middag, så udfyld skemaet nu - og vi vil straks gøre alting lettere for dig!</p>";
                }
                    break;
                case 13: {
                    descriptionContainer.innerHTML = "<p><strong>Søger du ejendomsservice?</strong></p><p>Så kan WorkXpress hjælpe dig! Ejendomsservice dækker over mange opgaver. Det kan strække sig fra græsslåning til servicering af de tekniske anlæg i bygninger. Derfor er det vigtigt at du finder frem til de opgaver, du ønsker udført, så vi kan finde den helt rigtige ejendomsservice til dit behov.</p><p>Ejendomservice består af udførelse af den daglige drift og vedligeholdelse af ind- og udvendigt af forskellige typer ejendomme. Fremfor at finde specifikke fagfolk til de enkelte opgaver, kan vi hjælpe dig med formidlingen til leverandører, der kan udføre den samlede opgave for dig. Der er rig mulighed for, at beskrive hvilken form for ejendomsservice du har brug for i vores tilpassede felt, og skulle der være brug for, at uploade billeder eller andet materiale, så gør endelig det. Det vil naturligvis gøre processen i, at finde den rette person til nettop din opgave. Det er vigtigt, at du beskriver opgaven så detaljeret som muligt, da det ofte er lettere, at sortere i de forskellige leverandører. </p><p><strong>Ejendomsservice til din virksomhed</strong></p><p>De fleste virksomheder har et behov for at der bliver vedligeholdt i virksomheden og dennes område, så den til hver en tid står flot og præsentabel. Det kræver stor tiltro og forventning til et godt udført arbejde af ejendomsservicefirmaet. Vi kan hjælpe dig med at finde dét firma, der kan klare din vedligeholdelse, så du slipper for bekymringerne. </p><p>Hvad enten du søger den bedste kvalitet indenfor rengøring, vinduespolering eller viceværtsservice, så står vi klar med videreformidlingen til de aller bedste på markedet. Måske du også har behov for pasning af grønne områder, en handyman ved hånden eller måske facility management til din boligforening, så hjælper vi dig med at finde den rette til at udføre dine opgaver. Udfyld skemaet og fortæl os om dine opgaver, du ønsker udført, og vi finder den bedste kandidat til dig, hurtigt og nemt.</p>";
                }
                    break;
                case 14: {
                    descriptionContainer.innerHTML = "<p><strong>Elektriker, når du har brug for det!</strong></p><p>Har du brug for en elektriker, men har svært ved at finde den helt rigtige til netop din opgave? Det kan WorkXpress hjælpe med. Du skal blot udfylde skemaet med din opgave, samt dine oplysninger, og så kan vi finde elektrikeren, der kan opfylde dit behov. </p><p>Der findes mange elektrikere på markedet, og flere har specialiseret sig i bestemte grene af elinstallation. Det er ikke altid nemt at gennemskue, hvilken der er den rette for dig, og det kan tage lang tid at hænge i røret med de forskellige. Især når der er tale om en akut situation. Vi kan spare dig for de problemer! Du skal kun een gang fortælle om din opgave eller problemstilling i dette skema, så tager WorkXpress den derfra og du kan koncentrere dig om vigtigere ting.</p><p><strong>Elektrikere til erhverv og private</strong></p><p>Måske har din virksomhed brug for reparation eller installering af store eller små elektriske anlæg, eller måske du skal have udskiftet alle dine stikkontakter derhjemme? Det er sjældent et problem for en elektriker. Men det kan dog være svært at gennemskue, hvilken elektriker, der kan udføre din opgave til den ønskede kvalitet og pris. Det er derfor, du har WorkXpress! Vi klarer nemt og hurtigt videreformidlingen til den elektriker, der kan hjælpe dig med din problemstilling.</p><p>Det er vigtigt ikke selv at prøve kræfter med elarbejde. Omgang med elektricitet er risikofyldt, og det er derfor vigtigt at finde en uddannet elektriker, der ved noget om strøm. Det kan derfor også være svært at finde ud af hvilken elektriker, man har brug for, og hvad præcis der skal laves, hvis man ikke kender til det. Bare rolig, vi hjælper dig! Når du har udfyldt skemaet, er det muligt at vedhæfte et billede, så det er nemt og hurtigt for elektrikeren at se problemet.</p>";
                }
                    break;
                case 15: {
                    descriptionContainer.innerHTML = "<p><strong>Har du brug for elevatorservice?</strong></p><p>Skal din elevator have et kvalitetstjek, eller har den måske brug for en reparation? Elevatorer har brug for service for at fungere optimalt i dagligdagen. Derfor er det en god idé at få en aftale med et elevatorservicefirma, der kan sørge for både service og vedligeholdelse af din elevator. WorkXpress hjælper dig gerne med at finde det helt rigtige firma!</p><p>Undgå ubehagelige overraskelser på din elevator, såsom at liften sætter sig fast under brug, ved at få en serviceaftale på elevatoren. Så slipper du for overraskelserne, der kan påvirke den daglige drift og fastlagte budget. Vi kan videreformidle dit behov, så du vil få kvalitetstjek af og -service på elevatoren i fremtiden.</p><p><strong>Lovpligtig service på elevatorer</strong></p><p>Det er lovpligtigt at få ydet service på elevatorer for at sikre at de fungerer optimalt og at de er kvalitetsikrede. På den måde er brugeren af elevatoren sikret, at chancerne for at den sætter sig fast er minimal. Ved et servicetjek af din elevator er det også muligt at finde fejl og mangler, og elevatorservicen kan sørge for en reparation heraf. Det kan også ske, at der er behov for ombygning af styringer og renovering af de mekaniske komponenter.</p><p>Inden at du bliver udsat for unødvendige overraskelse, så spar dig selv problemerne og tiden ved at sende WorkXpress din opgave, samt dine informationer. Så finder vi hurtigt og nemt den helt rigtige samarbejdspartner til at sørge for service og reparation af din elevator.</p><p>Er uheldet allerede ude, men det er svært at gennemskue, hvilken elevatorservice der er den rigtige for dig? Så lad os finde den bedste kvalitetsservice til dig hurtigt. Send os straks dine oplysninger ved at udfylde skemaet, og vi går i gang med det samme.</p>";
                }
                    break;
                case 16: {
                    descriptionContainer.innerHTML = "<p><strong>Facade- og tagvask, når dit hus skal stå som nyt!</strong></p><p>Er dit hus angrebet af mos og alger på facade og tag? Så lad WorkXpress hjælpe dig med at finde den rette professionelle til at slippe af med problemerne. Få nemt og hurtigt uden for meget besvær dit hus til at fremstå flot - enten for din egen skyld eller ved salg. </p><p>Ønsker du at male dit hus, eller har dit tag brug for et nyt lag maling? Som med alt muligt andet er det nødvendigt at rense overfladen grundigt for at malingen kan binde. Dette er også tilfældet, hvis og når du skal male dit hus eller tag. Udfyld skemaet, og lad WorkXpress finde de rette folk til at udføre opgaven for dig, hurtigt og nemt.</p><p><strong>Facade- og tagvask kan fjerne grafitti</strong></p><p>Blev du mødt af grim grafitti henover facaden, da du mødte på arbejde i morges? Det kan være rigtig svært at fjerne grafitti, så spar dig selv besværet og lad WorkXpress finde den rette professionelle til at klare det for dig. En professionel facade- og tagvaskfirma nøjes ikke kun med en højtryksrensning, men sørger også for rengøre facaden helt i bund. Dette kræver særlig ekspertise, så din facade ikke tager varig skade.</p><p>Ligegyldigt hvilken service du har brug for, hvadenten du skal have renset taget, fjernet grafitti, eller blive kvit med mos og alger på dit hus, så er det altid en god íde at rådføre dig med professionelle. Behandlingen og udførelsen af arbejdet tager nemlig udgangspunkt i hvilken type tag eller facade, du har. Udfyld skemaet med din opgave, samt oplysninger om hvilken facade eller hvilket tag, du har, så WorkXpress kan finde det helt rigtige facade- og tagvaskfirma til din opgave!</p>";
                }
                    break;
                case 17: {
                    descriptionContainer.innerHTML = "<p><strong>Klar til flytning?</strong></p><p>Du har pakket hele dit hjem ned i flyttekasserne. Måske har du allerede forsøgt at lokke dine venner med pizza og øl, hvis de giver en hånd med flytningen, men uden held. Måske du tilmed har lagt en udførlig plan for hvor mange ture det kræver med bussen for at få flyttekasserne med i dit nye hjem? Spar dig tiden og besværet, og lad os finde flyttefirmaet, der klarer flytningen for dig. Imens kan du nyde den førnævnte pizza og øl i selskab med dine venner, mens du lader professionelle klare besværet og de tunge kasser. </p><p>Det kan være en opslidende situation at skulle flytte. Een ting er at skulle sortere og pakke hele sit liv ned i flyttekasser, men det kan give lange arme at skulle foretage selve flytningen. Derfor sørger vi for at du finder det helt rigtige flyttefirma, der formår at varetage selve flytningen og bære dine tunge kasser til dit nye hjem. Udfyld skemaet med dine ønsker og din info, og så finder WorkXpress det helt rigtige flyttefirma til at forestå din flytning!</p><p><strong>Flytning - hurtigt og nemt!</strong></p><p>Ved at benytte dig af et flyttefirma til din flytning gør du alting nemmere for dig selv og din familie. Du sparer en masse værdifuld tid, som du i stedet kan bruge på ned- og udpakning i dit nye hjem. WorkXpress sørger for at finde dét flyttefirma, der passer til dine krav og behov. Så forsikrer vi dig at din flytning overstås hurtigt og problemfrit. For vi ved, hvor vigtigt det er at din flytteproces bliver så simpel som muligt, så du kan starte dit nye liv i dit nye hjem med det samme.</p><p>Måske det er din virksomhed, der flytter? Intet problem for WorkXpress! Du udfylder bare skemaet om dine ønsker og behov, samt din generelle information, og vi finder frem til det bedste flyttefirma i dit område.</p>";
                }
                    break;
                case 47: {
                    descriptionContainer.innerHTML = "<p><strong>Fotograf til netop din begivenhed!</strong></p><p>At finde den helt rigtige fotograf til din store dag kan være både besværligt og forvirrende. Der skal afstemmes forventninger og priser, og det kan være svært at finde den rigtige fotograf til netop din opgave. Undgå stressen, og lad os sørge for at finde fotografen til din begivenhed!</p><p>Der findes mange forskellige slags fotografer, der har specialiseret sig indenfor forskellige genrer. Der findes portræt-, børne-, bryllups- og eventfotografer, men der findes også fotografer, der er eksperter til at fotografere mad, produkter eller dyr. Hvad enten du søger en fotograf til dit bryllup, barnedåb, event eller noget helt tredje, så sørger vi for formidlingen til lige præcis den specialiserede fotograf, der opfylder dine ønsker.</p><p><strong>Minder for livet</strong></p><p>Valg af fotograf har større betydning end man umiddelbart skulle tro. Fotografen skaber minder til dig og din familie, og det er vigtigt at fotografen er lydhør overfor dine ønsker og behov, og samtidig leverer et godt kvalitetsprodukt. Det kan være en jungle at finde frem til fotografen, professionel som amatør, der er den helt rigtige til din fest eller dit arrangement. Derfor tilbyder WorkXpress at finde den helt rigtige fotograf til at løse din opgave.</p><p>Ved at vælge en dygtige fotograf får du mulighed for at genopleve dit arrangement igen og igen, og mindes det med et smil. Fotografer har en særlig sans for at fange øjeblikket, som vil give dig en varm fornemmelse i maven hver gang, du bladrer billederne igennem. Få mere ud af din bryllups- eller fødselsdag ved at finde fotografen, der sørger for at din dag kommer til at vare evigt.</p><p>Udfyld skemaet med info om dig og dit arrangement, dato og sted, så vi kan finde frem til den bedste fotograf til opgaven. Det er nemt og hurtigt, og vi forsikrer dig at du bliver tilfreds med resultatet. </p>";
                }
                    break;
                case 49: {
                    descriptionContainer.innerHTML = "<p><strong>Gardiner til det personlige hjem</strong></p><p>Der findes mange forskellige typer af gardiner, og det kan være svært at gennemskue præcis hvilket gardin, der passer til dit hjem. Der findes helt almindelige stofgardiner, men du kan også få plisse-, lamel-, og rullegardiner. Eller du ønsker måske persienner? WorkXpress sørger for at skaffe kontakten til en fagperson, der kan hjælpe dig med at finde den helt rigtige type afskærmning til dit vindue.</p><p>Et gardin er ikke bare et gardin. Det kan være med at skabe stemning og personlighed i dit hjem, og derfor bør det ikke være en hurtig og nem løsning. Når du skal vælge det helt rigtige gardin til dit hjem, er faktorer som farve, lys, klima og indemiljø vigtige. Det lyder måske som et større projekt, men det behøver det ikke være. Udfyld dine ønsker og informationer i skemaet, og lad os finde frem til en fagperson, der er specialiseret i gardiner i høj kvalitet til dit hjem.</p><p><strong>Har du brug for nye gardiner eller markiser?</strong></p><p>Der er stor kvalitetsforskel på gardiner og markiser. Derfor er det vigtigt at du får den rette rådgivning, så du kan tage den vigtige beslutning om et produkt, der er med til at sætte tonen og stemningen i dit hjem flere år frem. Vi hjælper dig hurtigt og nemt med at finde den person, der bedst kan rådgive dig.</p><p>Markiser kan gøre en stor forskel på dit udeliv. Det kan skabe et hyggeligt uderum på din terrasse, hvor markisen giver dig mulighed for at afskærme for sol og dug. En markise skal kunne holde til lidt af hvert, eftersom den er ude i alt slags vejr, og det er derfor vigtigt med den rette rådgivning. Udfyld dine ønsker til din nye markise, samt dine informationer, i skemaet og lad WorkXpress hjælpe dig med at finde den helt rigtige rådgivning.</p>";
                }
                    break;
                case 18: {
                    descriptionContainer.innerHTML = "<p><strong>Gartneren sørger for din have</strong></p><p>Finder du din have uoverskuelig? Eller har du svært ved at finde tiden til at sørge for at din have står pænt hele sommeren? Eller vil du hellere prioritere tiden på at nyde haven uden at skulle bruge tid på at holde den? Ligegyldig din årsag, så har vi løsningen! Lad os finde din havemand, så du ikke behøver at spekulere på om græsset er slået, planterne sået, og æbletræerne beskårede.</p><p>En gartner kan sørge for mange ting i din have. Lige fra at anlægge haven fra bunden til at passe og pleje den efter dine ønsker og behov. Skal du have klippet din hæk, ordnet dine frugttræer, plantet en køkkenhave, eller gravet de gamle buske op, så kan WorkXpress hjælpe dig med at finde den helt rigtige havemand til jobbet. </p><p><strong>Har du brug for hjælp til din have?</strong></p><p>Der er mange, der nyder deres have ved at gå tur i den, dufte til blomsterne, smage de friskplukkede pærer og jordbær, og samle krydderierne til aftensmaden. Det kan du også komme til uden at skulle bekymre dig om alt besværet ved vedligeholdelsen. Du kan komme til at nyde livet i fulde drag i din nye have.</p><p>Måske din have er forsømt? Eller ønsker du at din forhave står flot og indbydende til dine gæster? Så er det en gartner, du skal have fat i. Gartneren sørger for mange forskellige opgaver i din have. Om du ønsker høj- eller staudebede, belægning i din havegang, gårdsanretninger eller noget helt fjerde, så kan vi hjælpe dig med formidlingen til din nye havemand.</p><p>Alt du skal gøre er at udfylde skemaet med din opgave i haven, samt dine informationer, så finder vi den helt perfekte gartner til at hjælpe dig med din have, indgangsparti, gård eller køkkenhave.</p>";
                }
                    break;
                case 19: {
                    descriptionContainer.innerHTML = "<p><strong>FInd en dygtig glarmester her!</strong></p><p>WorkXpress kan hjælpe dig med at finde en dygtig glarmester hurtigt og nemt! Du skal blot udfylde skemaet med din opgave, hvadenten du skal have udskiftet dine vinduer eller har brug for en glarmester til dit næste byggeprojekt. Vi hjælper dig med at finde en kvalificeret glarmester, der klarer din opgave til bravour. </p><p>Det kan være godt givet ud at sikre sig kvalitet, når man skal have nye vinduer i sit hjem. Det kan have stor betydning på din varmeregning, da det ofte er vinduerne, der er skyld i varmeudslip. Få rådgivning hos glarmesteren, så I sammen kan finde frem til en løsning, der passer dit behov. Vi skal nok hjælpe dig med at finde en dygtig glarmester, der har den rette ekspertise.</p><p><strong>Skal du have udskiftet dine vinduer</strong></p><p>Måske dine ruder er punkterede, eller naboens søn fik sparket en bold ind gennem terassedøren. Det kan også tænkes at du er ved at lave nyt badeværelse, og ønsker brusekabinen indrammet i glas. Alt sammen kræver en glarmester, så hvorfor ikke vælge kvalitet fra starten? Det kan være besværligt at navigere rundt i de mange forskellige glarmestrer, der findes, for slet ikke at tale om at finde frem til det bedste tilbud. Lad os gøre det for dig, så er du hurtigt og nemt videre i processen, så glarmesteren kan komme i gang med at montere og opsætte dit nye vindue!</p>";
                }
                    break;
                case 20: {
                    descriptionContainer.innerHTML = "<p><strong>Nyt gulv eller gulvafslibning?</strong></p><p>Det kan være svært at vælge om man skal nøjes med en afslibning af sit gamle gulv i københavnerlejligheden, eller om det måske ligefrem er nødvendigt med en grundig gulvafhøvling i stedet. Eller måske det bare var smartere at få lagt et nyt gulv? WorkXpress kan formidle kontakten til flere fagmænd med speciale i gulve, så du kan få kvalificeret rådgivning.</p><p>Har du slidt lakken af dit gulv, eller er du ved at istandsætte dit studio, så det fremstår råt og industrielt som en newyorkerlejlighed? Et afslebet gulv kan være med til at skabe den helt rigtige stemning i dit hjem, som var det taget ud af Boligmagasinet. Vi forsikrer dig om at dine gæster vil bemærke det straks, når de kommer på besøg.</p><p><strong>Få kvalificeret ekspertise til dit gulv</strong></p><p>Det er selvfølgelig muligt selv at lægge sit gulv, men ofte er det både nemmere og smartere at få en fagmand til at gøre det, så du er sikker på at det er udført med stor ekspertise. På den måde undgår du også revner og buler i dit gulv. </p><p>Mange gulve kan leveres som såkaldte klikgulve, hvilket kan få de fleste til at kaste sig over projektet selv. Selvom det synes nemt, kan det hurtigt gå galt. Der skal tages højde for mange ting, såsom ujævnheder i det eksisterende gulv og varme/kuldepåvirkning, og det kræver specificeret og kvalificeret ekspertise fra en fagmand.</p><p>WorkXpress hjælper dig med at finde den rette gulvlægger eller gulvsliber til at udføre dit projekt. Vi gør processen nemmere for dig ved at du blot skal udfylde skemaet, hvor du beskriver dine gulve - eller de ønskede gulve, og så finder vi dygtige fagfolk til dig. </p>";
                }
                    break;
                case 26: {
                    descriptionContainer.innerHTML = "<p><strong>Få en professionel isolatør til at isolere dit hjem</strong></p><p>Står du midt i et byggeprojet og har brug for at få isoleret dit hus? Eller har din teenagesøn endelig fået lov til at spille med sit band i kælderen, og du har nu indset at du får behov for lydisolering? Så er det en isolatør, du skal have fat i. </p><p>Oplever du træk i hjemmet, er det muligt for isolatøren at finde frem til områderne, hvorfra vinden slipper ind, og udbedre problemet. Samtidigt sørger han også for at dit indeklima bliver bedre, og din varmeregning billigere.</p><p><strong>Isolatør med fokus på energiforbrug</strong></p><p>I dagens Danmark får vi mere og mere fokus på miljø og energiforbrug. Vi er blevet mere opmærksomme på vores generelle forbrug og miljøet, og til at nedsætte dette forbrug, er det en god idé at hente hjælp hos en professionel isolatør. En isolatør kan isolere rør, beholder og kedel- og ventilationsanlæg i alle typer boliger. Det er altså isolatøren, der står for isolation for kulde, varme, lyd og vibrationer, som alle kræver forskelligt materiale og ekspertise at gøre korrekt.</p><p>Er det uoverskueligt at finde frem til den rette isolatør, så lad WorkXpress hjælpe dig. Ligegyldig om du har en stor eller lille isoleringsopgave, kan vi finde en dygtig isolatør til at udbedre din bolig, hvor du føler dig tryg og velinformeret. Udfyld blot skemaet med din opgave, og dine informationer, og så finder vi din isolatør hurtigt og nemt!</p>";
                }
                    break;
                case 27: {
                    descriptionContainer.innerHTML = "<p><strong>Kloakservice, så du slipper for bøvl med kloakken!</strong></p><p>Er din kloak tilstoppet igen? Eller er du nervøs for om du har rotter? WorkXpress kan hjælpe dig med at finde den rette kloakservice, der kan hjælpe dig med udbedringen af tilstoppede kloakker. Det eneste, du skal gøre, er, at skrive din opgave og informationer til os, så kan vi hurtigt og nemt finde frem til en kvalificeret kloakmester til at udføre dit beskidte arbejde.</p><p>En kloakmester sørger for mange ting, når der skal udføres kloakservice. Her indgår alle opgaver, såsom rensning og slamsugning af kloak, faldstammer, toiletter og afløb. Der er ikke den opgave, der er for lille eller stor. Det kan være uoverskueligt at finde den helt rigtige kloakservice til at håndtere dit kloakproblem. Lad os hjælpe dig med processen, så alt bliver nemmere og mindre besværligt for dig. Udfyld skemaet med din beskidte opgave, og WorkXpress klarer resten. Vi finder hurtigt og nemt en professionel og kvalificeret kloakmester til at hjælpe dig!</p><p><strong>Kan man ikke bare skylle ud?</strong></p><p>God kloakservice kan ikke klares selv. Det kræver en autoriseret og professionel mester at udføre kloakservice. Det kan være påkrævet i forbindelse med bygning af nyt hus, mistanke om brud eller revner i ledninger, eller hvis du er ramt af skadedyr, der har forårsaget ødelæggelser i dit kloaksystem. </p><p>Med andre ord, står du overfor kloakproblemer, så lad WorkXpress hjælpe dig med at finde frem til en dygtig kloakmester, der kan udføre alle former for kloakarbejde for både private og virksomheder ligegyldig størrelsen. Du skal blot udfylde skemaet med din opgave, og vi sørger for at videreformidle og finde frem til den helt rigtige mester til dig! </p>";
                }
                    break;
                case 28: {
                    descriptionContainer.innerHTML = "<p><strong>Har du brug for nye kontorforsyninger?</strong></p><p>Er du ved at lave nye kontorlokaler, eller skal du erstatte det gamle inventar? Hos WorkXpress har du mulighed for at indhente tilbud med kontorforsyninger til lige netop dét, du har brug for nu og her. Ved at udfylde skemaet med dine behov og informationer, finder vi frem til den bedst kvalificerede leverandør til at opfylde dette.</p><p>Der skal indkøbes utroligt mange ting til et nyt kontorområde, og det kan være besværligt at finde frem til de helt rigtige produkter med den rette pris og rette kvalitet. Det er tidskrævende, og vi er sikre på, at den tid kan du sagtens bruge andetsteds. Derfor vil vi gerne hjælpe dig med at finde frem til lige netop de kontorartikler, -maskiner og -møbler, du har brug for i dit kontorlandskab.</p><p><strong>Det personlige kontor</strong></p><p>Et kontor behøver ikke at være kedeligt og sterilt. WorkXpress hjælper dig med at finde frem til den rette leverandør, der kan formå at gøre dit kontor til noget helt særligt. Det kan være vigtigt for både forretningen og dig selv, at den hyggelige stemning bibeholdes i arbejdsmiljøet. Det skaber glæde og positivitet i hverdagen.</p><p>Kontorforsyning dækker over mange forskellige ting, såsom affaldssortering, skriveborde, slik og kaffe, kontorartikler og andet pynt, der er med til at skabe en stemning på dit arbejde. Lad os hjælpe dig med at finde frem til netop den leverandør, der dækker dine behov for kontorforsyning. Udfyld skemaet med dine præcise ønsker og behov, så finder WorkXpress frem til en leverandør, der kan matche dette. Vi glæder os til at hjælpe dig!</p>";
                }
                    break;
                case 29: {
                    descriptionContainer.innerHTML = "<p><strong>Har du brug for en maler?</strong></p><p>Er du lige flyttet i et nyt hus, eller trænger dit hjem bare til en ny farvepalette? Så kan vi hjælpe dig med videreformidlingen til den helt rigtige maler! </p><p>En maler står ikke kun for malingen af dine vægge og lofter, men også meget mere. Om du skal have fixet din stuk, have spartlet, sat væv, filt eller tapet op, så kan en maler klare jobbet for dig. </p><p>De sørger endda for at afdække, så du ikke får maling alle andre steder end på dine vægge. Slip for besværet  og lad de professionelle tage sig af arbejdet, så du kan nøjes med at bruge tiden på at udtænke den helt rigtige farve til dine vægge, mens du nyder en kop kaffe. Du kan uploade en opgave her og dermed sende din opgave ud til et hav af kompetente malere, som alle vil være i stand til, at hjælpe dig. Vi sørger for, at du hurtigt får svar på opgaven og ikke mindst får løst problemet hurtigt. Det er gratis, at benytte WorkXpress.</p><p><strong>Undgå helligdage</strong></p><p>Vi kender det alle. Nogle gange er det nemmere lige at gøre tingene selv. Man kaster sig over malerarbejdet uden at have haft en pensel eller malerrulle i hånden, og går i gang med arbejdet. Måske man er tilfreds, når det sidste penselstrøg er udført. I alt fald lige indtil malingen tørrer og lyset fra solen falder således at du pludselig får øje på alle de steder, hvor malingen ikke dækker helt. De frygtede helligdage. Du spotter også nu, at du i processen ikke har opdaget at malingen har klattet på gulvet, og panelerne har interessante men uønskede mønstre. </p><p>Både besværet og frustrationen kan vi spare dig for! Inden du kaster dig ud i at påføre malingen, så udfyld skemaet med din maleropgave og fortæl os, hvilken type malerarbejde, du ønsker udført. Så kan vi hjælpe dig med at finde maleren, du har brug for. Vi forsikrer dig at du vil undgå helligdage og malerpletter på sofaen, når du vælger en maler igennem WorkXpress. </p>";
                }
                    break;
                case 30: {
                    descriptionContainer.innerHTML = "<p><strong>Skal du bruge en murer?</strong></p><p>En murer kan være med til at realisere din boligdrøm. Står du overfor et større boligprojekt, eller skal du have renoveret dit hus? Så kan det godt tænkes at du har brug for en murermester, der kan fremtrylle vægge i mursten og mørtel. WorkXpress hjælper dig med at finde frem til en kvalificeret murermester eller et murerfirma, der kan løse opgaven.</p><p>En murer bygger ikke bare mure. En murer står også for facaderenovering på bygninger. Her er det i særdeleshed vigtigt at finde den helt rigtige til at udføre jobbet, særligt hvis der er tale om en facadeskade. Det kan være et tidskrævende stykke arbejde, der kræver høj ekspertise, så det udføres korrekt. Det kan næsten være ligeså tidskrævende at finde frem til mureren, som det er for mureren at renovere en facade. Det gør WorkXpress nemmere for dig nu. Upload din mureropgave i skemaet, og vi vil straks gå i gang med at finde den bedst kvalificerede fagmand til at udføre din opgave.</p><p><strong>Vælg den helt rigtige murermester til dit projekt</strong></p><p>Faglært bygningshåndværker kan man også kalde mureren. Selve håndværket kan vi spore helt tilbage til de gamle danske stenkirker fra det 11. Århundrede. Med andre ord kan det korrekt opsatte murværk holde i mange år, hvis det udføres professionelt og med stor præcision. De formår typisk at mestre både traditionelle og komplicerede projekter. Vi sørger for at du får forbindelse til den helt rigtige murermester til dit projekt.</p><p>Skal du have lavet en tilbygning eller ombygning? Måske et nyt badeværelse, renoveret dit eksisterende hus eller ordnet facaden på din forretning? Så er det et murerfirma, du skal have fat i. Udfyld skemaet med din bygningsopgave, og vi vil straks tage hånd om din opgave og finde den bedst kvalificerede murer til at udføre projektet. Så bliver det ikke nemmere!</p>";
                }
                    break;
                case 31: {
                    descriptionContainer.innerHTML = "<p><strong>Nedrivning af bygninger</strong></p><p>Når man skal have revet et hus ned, handler det ikke bare om at vælte en mur. Det er et større projekt, der kræver specialister til at udføre korrekt. Dette skyldes blandt andet at der skal sikres, at ingen kommer til skade under processen, men også for at undgå at der ikke er visse dele af bygningen, som ikke skal nedbrydes. </p><p>Man skal aldrig give sig i kast med at rive vægge og hele bygninger ned selv, selvom det måske kan være fristende. Der er mange regler, der skal overholdes, så nedbrydningen foregår så sikker og hurtigt som muligt. Vil du gerne i gang med det samme? Så lad os skabe kontakten til nedrivningsfirmaerne, så vi kan gøre din nedrivning til en realitet med det samme!</p><p><strong>Find nemt og hurtigt dit nedbrydningsfirma</strong></p><p>At finde frem til det helt rigtige nedrivningsfirma kan være besværligt og tidskrævende. Særligt hvis du ønsker at handle ansvarsbevidst og gå efter et firma, der også forestå sorteringen af de forskellige nedbrudte materialer med henblik på genanvendelse. Desuden er det vigtigt at sørge for at nedbrydningsfirmaet laver kontrolleret nedrivning, således at de også sørger for at der ikke ligger nedbrudte materialer spredt rundt omkring. En kontrolleret nedrivningsfirma har også for øje, at du har naboer og måske virksomheder i området, der skal forstyrres mindst muligt, samtidig med at de forestår nedrivningen således at bygningsdele ikke rammer ting, de ikke bør.</p><p>WorkXpress kan hurtigt finde frem til dét nedbrydningsfirma, der er bedst kvalificerede til at stå for dit nedrivningsprojekt. Det gør vi ved at du uploader din nedbrydningsopgave i skemaet med præcision og information herom, og så finder vi det tilbud, der vil være bedst til at løse netop din opgave!</p>";
                }
                    break;
                case 32: {
                    descriptionContainer.innerHTML = "<p><strong>Få hjælp til rengøringen her!</strong></p><p>Rengøring kan være svært at nå i en travl hverdag. Den bliver nemt nedprioriteret, da der ofte er bedre ting at give sig til i livet. Vi kan dog ikke komme udenom at rengøring er en nødvendighed for at have det godt. Men det kan være svært at gennemskue, hvilket rengøringsfirma, der bedst forestår sådan en opgave. Det må gerne foregå hurtigt og til perfektion, som du selv havde gjort det, samtidig med du måske også kæmper med tanken om at lukke fremmede ind i dit hjem. Dét hjælper WorkXpress dig med nu!</p><p>WorkXpress gør dit liv nemmere! Godt nok udfører vi ikke rengøringen selv, men vi kan finde frem til det rengøringsfirma, der tager bedst hånd om dit hjem eller din virksomhed. Du skal blot udfylde skemaet med dine rengøringsopgaver, så kommer vi med et bud på de bedst kvalificerede rengøringsfolk i dit område.</p><p><strong>Brug tid på de vigtigere ting i livet!</strong></p><p>Slut med alle skænderierne om hvis tur det er til at skrubbe toiletterne, give stuen en overhaling med støvsugeren, eller pudse vinduerne! Med rengøringshjælp vil du få tid til de vigtigere ting i livet, så du både kan nyde hverdagen og et rent hjem på samme tid. </p><p>Der er meget forskellig rengøring, der skal sørges for i de danske hjem. Med et rengøringsfirma har du mulighed for at planlægge præcis hvor ofte, du ønsker rengøring. Måske du gerne vil klare det meste selv, og kun har brug for hjælp een gang om måneden? Eller vil livet blive bedre, hvis der blev gjort rent ugentligt? Find den bedste løsning med dit nye rengøringsfirma! WorkXpress sørger for at du får tilbudt den bedst kvalificerede rengøring til dit hjem!</p>";
                }
                    break;
                case 33: {
                    descriptionContainer.innerHTML = "<p><strong>Revisor til din virksomhed</strong></p><p>Alle virksomheder har brug for en revisor til at tjekke op på regnskaberne og måske endda lave dem, bogføre og rådgive. Der er mange love at holde styr på, og det kan gå hen og blive problematisk for den enkelte virksomhed at sørge for bogføringen selv. Derfor er det en god idé at finde en revisor, der kan hjælpe dig med at løse de mange regnskabsopgaver i dit firma. </p><p>Det kan være svært at gennemskue, hvilken revisor, du har brug for - og om den revisor, du overvejer, nu er god nok til at løse din opgave. De mange spekulationer kan du straks bogføre selv, for WorkXpress sørger for at du ikke længere behøver at bekymre dig om dette! Udfyld skemaet med din regnskabsopgave, og vi sørger for at finde revisoren, der både leverer et kvalificeret og dygtigt stykke arbejde til en pris, du kan og vil betale.</p><p><strong>Revision nu og her</strong></p><p>Når vi nærmer os tiden for indlevering af årsopgørelsen, er det virkelig svært at opspore den helt rigtige revisor - hvis det overhovedet er muligt at finde nogen, der er ledige. Nogle gange kan en revisionsopgave ikke vente, og du har brug for en revisor nu og her. Det skal WorkXpress nok hjælpe dig med. Vi sørger for videreformidlingen af din regnskabsopgave, så du får hjælp til dit revisionsarbejde hurtigst muligt. </p><p>De færreste går faktisk glip af mulige besparelser på flere områder, fordi de har fravalgt en autoriseret og dygtig revisor til at hjælpe dem. Du kan altså ofte opleve at du kan spare rigtig mange penge ved at hyre en revisor til at gennemgå dit regnskab. Grib chancen med det samme, og lad WorkXpress finde en god revisor til dig!</p>";
                }
                    break;
                case 34: {
                    descriptionContainer.innerHTML = "<p><strong>Har du brug for skadedyrsbekæmpelse?</strong></p><p>Har du fået ubudne gæster i form af væggelus, myrer, møl, rotter eller noget helt femte? Hører du pibelyde under køkkenvasken, huller i uldtøjet, muldvarpeskud i haven eller summende lyde fra hvepse lige udenfor din hoveddør? Så har du brug for skadedyrsbekæmpelse. WorkXpress finder nemt og hurtigt frem til den bedst kvalificerede skadedyrsbekæmper i dit område, der kan klare dit skadedyrsproblem. Alt, du skal gøre, er, at skrive dit problem ind i vores skema, og lad os hjælpe dig hurtigt.</p><p>Skadedyrsbekæmpelse tager ikke kun udgangspunkt i at fjerne allerede eksisterende skadedyr fra din matrikel. De formår også at skabe et bekæmpelsesprogram, der har en forebyggende virkning, så du undgår skadedyr i fremtiden. Det kan ske når som helst og for hvem som helst.</p><p>Er du bekymret for hvordan skadedyrsbekæmpelsen foregår? Oplys os om det, når du udfylder skemaet med dit skadedyrsproblem, så vi kan tage højde for det i søgningen efter en kvalificeret skadedyrsbekæmper i dit område. Skadedyrsbekæmperne sørger for at benytte sig af midler til bekæmpelsen, som alle er godkendt af Miljøstyrelsen. </p><p><strong>Skadedyr - eller bare dyr?</strong></p><p>Det kan være svært at afgøre, hvornår der er tale om et skadedyr eller et helt almindeligt dyr, der skal have lov til at være der. Når der er tale om et skadedyr, er det dem, der laver fysisk skade på dit hjem eller i din have ved at spise planter eller ødelægge jorden. Hvis du føler dig plaget af et bestemt dyr eller insekt, så er der med stor sandsynlighed tale om et skadedyr, og du har derfor brug for en skadedyrsbekæmper.</p><p>Lad WorkXpress hjælpe dig med at gøre processen hurtigere, så du kan komme af med dine plageånder! Udfyld skemaet med dit skadedyrsproblem, og vi vil hurtigt finde den bedst kvalificerede skadedyrsbekæmper i dit nærområde, så du kan blive skadedyrene kvit.</p>";
                }
                    break;
                case 35: {
                    descriptionContainer.innerHTML = "<p><strong>Skrædderen garanterer funktionalitet, holdbarhed og komfort</strong></p><p>En skrædder kan mange flere ting end man lige skulle tro. Alle kan få brug for en skrædder. Står du med den ultimative drøm af en kjole, som du vil have på til din grankusines sommerbryllup, men kan ikke finde den i en fysisk butik? Eller kan du aldrig finde et jakkesæt, der sidder perfekt over skuldrene? Så kan en skrædder hjælpe dig! De lytter til dine ideer, supplerer med rådgivning, måler dig og sørger for at skræddersy dit næste sæt tøj. </p><p>Det kan dog være svært at gennemskue, hvilken skrædder der er bedst til formålet. Det er tidskrævende at finde frem til den helt rigtige, der kan gøre din datters konfirmationskjole til virkelighed. Alt det besvær behøver du slet ikke rode med, for det klarer WorkXpress for dig! Du skal blot udfylde skemaet med dine tanker til opgaven og dine informationer, så sørger vi for at du får kontakt til en skrædder, der står klar med målebånd, nål og tråd.</p><p><strong>Skrædder syr tøj efter mål - eller dine nye gardiner!</strong></p><p>Skrædderen nøjes ikke kun med at iklæde dig smukke, håndlavede gavanter. En skrædder kan også sørge for at dine indretningsdrømme får lov til at springe ud i fuldt flor! Hvad enten du ønsker nye gardiner til stuen, hynder til stolene, speciallavede puder til sofaen af det særlige stof, Onkel Thomas havde med hjem fra Indien - så kan en skrædder hjælpe dig! Med særlig opmærksomhed på detaljerne og præcision i deres arbejde, kan skrædderen være med til at skabe dig et hjem fuld af personlighed og hygge.</p><p>Har du lavet hul i trøjen, eller måske haft besøg af et par sultne møllarver? Så send os dit problem, og vi sætter dig straks i forbindelse med en skrædder, der kan hjælpe dig med hullerne. Inkludér gerne et foto af hullet, så skrædderen har mulighed for at afgøre om det er muligt at redde. </p><p>Sidder bukserne perfekt om hofterne, men er en smule lange? Nu behøver du ikke længere bruge oceaner af tid på at lede efter bukser, der både passer i bredde og længde! Sidder bukserne perfekt over numsen, men du falder over længden, når du går, skal du straks skrive til os. WorkXpress finder straks skrædderen til dig, der kan løse alle dine fremtidige bukseproblemer ved at lægge dem op, så de er tilpasset dig.</p>";
                }
                    break;
                case 37: {
                    descriptionContainer.innerHTML = "<p><strong>Tagdækkeren sørger for dit nye tag!</strong></p><p>Er du træt af at skulle samle vand i gryder og skåle i stuen, hver gang det regner? WorkXpress kan gøre dig problemet kvit - hurtigt og nemt. Mens du passer vandstanden i din midlertidige løsning, sørger WorkXpress for at skabe kontakt mellem dig og en kvalificeret tagdækker. Tagdækkeren er en dygtig og professionel fagmand, der ved præcis, hvordan huller i taget skal fixes.</p><p>En tagdækker kan både lægge et nyt tag og reparere dit gamle tag. Tagdækkeren har god erfaring i tagdækning med tagpap, tagfolio og andre materialer, der er med til at sikre og beskytte dit hjem vejr og vind. Det er ikke ligegyldigt om taget hælder eller om det er fladt, da der her skal bruges et særligt materiale til dit tag. Det er derfor vigtigt at du benytter en professionel tagdækker til at lægge taget for det bedste resultat.</p><p><strong>Få dit tag skåret ud i pap!</strong></p><p>WorkXpress sørger for at du får kontakt til det bedst kvalificerede tagdækkerfirma i dit område. Udfyld skemaet med din opgave, herunder hvilket type tag du har eller ønkser, samt hvad der skal gøres, så klarer vi resten! Sammen med tagdækkeren finder du frem til de mere specifikke ting, der skal foretages i forbindelse med lægningen af dit nye tag eller reparationer af det gamle.</p>";
                }
                    break;
                case 48: {
                    descriptionContainer.innerHTML = "<p><strong>Få styr på byggeridrømmene med totalentreprise</strong></p><p>Går du rundt med boligdrømme og byggeideer, men synes der er langt fra tanke til handling? Måske du finder det svært at finde tiden til at nå det hele, når du går med byggetanker? Så er det totalentreprise, du skal have fat i. En totalentreprise sørger for det hele, når du skal bygge hus. De sørger for rådgivningen, samt de forestår hele udvælgelsen af arkitekter og fagentreprenører til dit byggeprojekt. Samtidig tager totalentreprisen sig også af alle planlægnings- og styringsopgaver gennem hele processen. Det eneste, du skal gøre, er at underskrive en kontrakt - og så sørger totalentreprisefirmaet for resten. </p><p>Start allerede nu! WorkXpress sørger for at du får kontakt til det bedst kvalificerede totalentreprisefirma til at forestå dit byggeprojekt. Du skal bare udfylde vores skema med dit projekt og dine ønsker, så skaber vi kontakten.  Det er nemlig vigtigt at vælge det helt rigtige totalentreprise til dit byggeprojekt, så du trygt og roligt kan overlade dit projekt i andres hænder.</p><p><strong>Totalentreprise til typehuse og sommerhusbyggeri</strong></p><p>Totalentreprenører har primært fokus på typehuse og typificeret sommerhusbyggeri. Firmaet har råderum over en række professionelle arkitekter, ingeniører og bygningskonstruktører, og kan derfor sørge for at dit projekt går fra tanke til færdigt projekt. Lad os føre din tanke ud i livet allerede nu! Skriv din forestående opgave ned i skemaet, og lad os hjælpe dig med at dit næste projekt!</p>";
                }
                    break;
                case 38: {
                    descriptionContainer.innerHTML = "<p><strong>Tømreren bygger dit næste projekt!</strong></p><p>Har du en idé, der skal udføres i træ? Eller leger du med tanken om en ny carport? Eller måske er din største drøm en træhytte? Så er det en tømrer, du skal have fat i. De sørger for at dine drømmebyggerier i træ bliver virkelighed, hurtigt og professionelt. En tømrer kan forestå mange typer opgaver, hvor materialet er træ, både i store og små udformninger. Skal vi hjælpe dig med at finde den helt rigtige tømrer til dit projekt? Skriv til WorkXpress ved at udfylde skemaet med din opgave, og vi vil straks finde en kvalificeret og professionel tømrer i dit område. </p><p><strong>Tømreren til alt fra nybyggeri til renovering</strong></p><p>En tømrer kan sørge for meget. Faktisk kan tømreren forestå det meste træ- og konstruktionsarbejde i boligbyggeri, hvadenten der er tale om moderne byggeri eller det tager udgangspunkt i gamle håndværksteknikker. Har du brug for en ny havestue, hvor du kan nyde din søndagsbrunch? Eller har du brug for at få sat en væg op? Opgaven kan du trygt give videre til tømreren. Lad WorkXpress hjælpe dig med at finde den rigtige tømrer til din opgave. Nemt og hurtigt.</p><p>Træ er ikke bare træ. Spørg en hvilken som helst tømrer. Alt efter om dit byggeprojekt skal stå uden- eller indenfor, har materialet noget at sige. Derfor er det vigtigt at du finder en dygtig tømrer, der er til at tale med og som er god til rådgive. Sammen kan I sparre om det helt rigtige materialevalg til projektet, samt kvaliteten heraf. Vi ved dog godt, at det kan være besværligt og tidskrævende at finde netop den tømrer, der er det perfekte match til dig. Så vi hjælper dig meget gerne videre i processen. Du skal blot fortælle om dit projekt i skemaet, så finder vi hurtigt og nemt en god tømrer til dig!</p>";
                }
                    break;
                case 39: {
                    descriptionContainer.innerHTML = "<p><strong>Træfældning, hurtigt og nemt!</strong></p><p>Det er et større projekt at fælde træer. Der er en masse sikkerhedsforanstaltninger, der skal tages, således at der ikke er nogen, der kommer til skade under træfældningen. Eller at træet vælter ned over din nabos hus. Samtidig kræver det også det helt rigtige værktøj og udstyr udover ekspertise for at kunne forestå en træfældning hurtigt og korrekt. </p><p>Man kan selvfølgelig selv klare at fælde et træ. Men ofte er det besværligt og tidskrævende at skulle give sig i kast med. Næsten lige så besværligt som at skulle finde et træfældningsfirma. Så er det godt at du er havnet her hos WorkXpress, for vi kan hjælpe dig videre i processen, så du slipper af med det gamle visne grantræ i baghaven! Udfyld vores skema med din træfældningsopgave, og vi vil hurtigt finde et kvalificeret træfældningsfirma i dit nærområde!</p><p><strong>Træfældning på hele vejen!</strong></p><p>Måske du ikke er den eneste på din vej, der har træer, der skygger, eller som står visne hen. Spørg dine naboer om du ikke skal arrangere at der kommer et træfældningsfirma ud og sørger for jeres problemer! Du vil blive vejens helt med os som din hemmelighed! Vi skal nok finde det rette træfældningsfirma til at fjerne de træer, I ikke længere ønsker på jeres vej. Skriv straks din opgave i vores skema - intet træ er for stort eller småt!</p>";
                }
                    break;
                case 44: {
                    descriptionContainer.innerHTML = "<p><strong>Vinduespudsning til dig, der vil have tid til andet</strong></p><p>Vil du gerne nyde solens stråler igennem dine vinduer? I en hektisk hverdag er det ikke altid at man når at sætte tid af til at pudse sine vinduer. Det kan vi hjælpe med! Hvis du har snavsede vinduer, har vi den rette vinduespudser til at gøre din udsigt bedre!</p><p>Vi er selv overraskede over hvor hurtigt vinduer bliver beskidte. Osen fra de forbikørende biler, pollen fra forårets skønne blomster, fugleklatter, og måske endda aftryk af fugle, der ved et uheld er fløjet ind i dit vindue. Alt sammen er med til at gøre dine vinduer snavsede, og med beskidte vinduer følger et gråt og trist udsyn på livet udenfor. Det er der råd for hos os. Hos os slipper du for at gøre det selv, for vi står klar til at sørge for at dit udsyn bliver skinnende og lyst!</p><strong>Spar dig selv besværet ved at gøre det selv</strong><p>Vi forstår dig. Vinduesvask og -pudsning er ikke en nem opgave at klare selv. Selvom man overholder de mange gode husmoderråd, bliver det bare aldrig helt ligeså godt som det resultat en professionel kan præstere. Vinduerne ender måske med striber eller fedtpletter, som du i frustration ihærdigt forsøger at skrubbe væk. Vi kan spare dig alt besværet! Send din opgave videre til os, og vi sørger for at dine vinduer bliver blanke, så du kan koncentrere dig om vigtigere ting i livet.</p><strong>Lad os hjælpe dig - nemt og hurtigt!</strong><p>Det eneste du skal gøre er at udfylde skemaet med antallet af vinduer, du ønsker at få pudset, samt dine informationer, og så klarer vi resten. </p>";
                }
                    break;
                case 40: {
                    descriptionContainer.innerHTML = "<p><strong>Har du brug for en trappevask? </strong></p><p>Bor du et sted, hvor trapperne skal vaskes, men du ikke selv har tiden eller kræfterne til det? Bare rolig - Du kan her på WorkXpress let og simpelt uploade en opgave, hvor vi finder en, som kan hjælpe dig. En trappevask kan være ulidelig og i visse tilfælde også være en nødvendighed for det sted du bor. Så hvorfor ikke slippe for opgave og lade en komme og hjælpe dig? Når opgaven er indsendt af dig, vil vi finde en kompetent person til opgaven og dermed vil alle dine problemer være ude af verdenen. Det er gratis, at benytte WorkXpress til, at uploade din opgave og du vil få besked så snart, at vi har en til at udføre opgaven. </p><p>WorkXpress er din serviceportal, hvor du let og enkelt kan lægge din opgave ud til mange, som hjerteligt gerne vil løse opgaven. Du får mulighed for, at få flere tilbud på samme opgave, som betyder at du har frit slag i udvælgelsen af den rette kandidat. WorkXpress er skabt for dig, som ikke vil ligge for mange kræfter i de daglige gøremål, men komme let og elegant rundt om dem. Det betyder ikke, at du får et dårligere resultat, men blot at du ikke behøver, at ligge den samme energi i tingene. Du ligger blot din opgave på WorkXpress og så får du uforpligtigende tilbud tilsendt direkte i din mail indbakke. </p>";
                }
                    break;
                case 45: {
                    descriptionContainer.innerHTML = "<p><strong>Mangler du en VVS’er?</strong></p><p>Det sker jævnligt, at man har brug for en VVS’er, som er dygtig og er til at betale. Det kan være svært, at finde en god og dygtig VVS’er som også er til at betale. Derfor kan du nu beskrive din opgave godt og grundigt, så vi har mulighed for, at dele opgaven med vores dygtige underleverandører. Du skal helst komme godt ind på opgaven og ikke mindst på en pris. Derfra vil det kunne blive vurderet af udlærte fagfolk. Står du og mangler en VVS’er akut, så hjælper vi dig hurtigt videre. </p><p>Der er mange forskellige opgaver, som kan udføres af en VVS’er også erhvervsmæssigt. Derfor kan alle lægge en opgave op og få et seriøst bud på udførelsen heraf.Det er let og simpelt.</p><p>Med en god og dygtig VVS’er er de fleste opgaver hurtigt løst. Det er derfor en billig løsning, da du ikke skal bruge krudt på, at der sker fejl og mangler. Alle vores underleverandører er udvalgte samarbejdspartnere, som alle vil dig det bedste. Er du i tvivl om opgavens omfang, så notér blot dette. Så kan det tænkes, at en VVS’er kommer ud til stedet og vurderer opgaven. Derfor er det vigtigt, med en god opgavebeskrivelse, som gør denne proces simpel. </p><p>WorkXpress samler tilbud fra de mest kompetente og dygtigste samarbejdspartnere, som alle er verificeret. Det betyder, at du trygt og roligt kan stole på, at din opgave fortroligt og professionelt bliver distribueret ud i vores netværk. Det sikre, kvalitet og et godt resultat hver gang. </p>";
                }
                    break;
                case 36: {
                    descriptionContainer.innerHTML = "<p><strong>Få hjælp til, at fjerne sneen eller salte fortorvet. </strong></p><p>Det er lovpligtigt, at rydde sin indkørsel eller opgang, så postvæsenet eller andre ikke falder grundet is eller sne foran dit hjem. Det betyder dog ikke, at lysten eller kræfterne til dette er der, og derfor kan du igennem WorkXpress let og elegant uploade en opgave. Det er langt nemmere, end at skulle rydde dit fortov eller indkørsel selv. Det er samtidigt en vigtighed for sikkerheden, at folk ikke kommer til skade ved, at bevæge sig ude foran dit hjem. Mange skader kunne være undgået med denne service. </p><p>Snerydning udbydes af mange og naturligvis kun i vinterhalvåret, hvor det er relevant. Derfor er det ekstra vigtigt, at du udformer en god og beskrivende opgave, hvor det er overskueligt, hvor mange meter det drejer sig om, samt hvor stor en grund er. Det skal også fremgå, hvornår på dagen du ønsker, at få ryddet din grund. </p><p>WorkXpress har gjort det ekstremt nemt, at komme ud med din opgave til vores mange følgere. Det betyder blandt andet, at snerydning ikke længere er en pine, som du behøver at tænke på. Lad os klare den, alt imens du bruger tid på noget, som er langt sjovere og mere værd for dig. Sut med de kolde vintermorgener, hvor man skovler sne inden dagen rigtig begynder. </p><p>Når den kolde periode kommer, så vær ude i god tid med din opgave. Det kan starte med saltning og derfra snerydning, hvis det bliver nødvendigt. Er du for sent ude, kan det være, at du ikke slipper alligevel, da det som sagt er obligatorisk ved sne eller isfald. Skulle du være så uheldig, at en fremmede væltede på din grund, vil du være i gode hænder ved en advokat. Derfor er det en langt bedre løsning, at du uploader din opgave til os, og vi sikrer, at der kommer styr på snerydningen.</p>";
                }
                    break;
            }
        }

        showCategoryDescription({{$categoryId}});

        function toggleRatingForm() {
            var completedCheckbox = document.getElementById("completed").checked;

            var ratingForm = document.getElementById("rating-form");
            if (completedCheckbox) {
                ratingForm.style.display = 'block';
            } else {
                ratingForm.style.display = 'none';
            }
        }

        function readmultifiles(files) {
            var reader = new FileReader();

            function readFile(index) {
                if (index >= files.length) return;
                var file = files[index];
                reader.onload = function (e) {
                    // get file content
                    var bin = e.target.result;

                    $('#project-photos').append(
                        '<div class="project-image-holder">' +
                        '<div class="img-thumbnail project-photo text-center">' +
                        '<div>' +
                        '<img src="' + bin + '">' +
                        '</div>' +
                        '<p>Klar til upload</p>' +
                        '</div>' +
                        '</div>'
                    );

                    // do sth with bin
                    readFile(index + 1)
                };
                reader.readAsDataURL(file);
            }

            readFile(0);
        }

        function photosToUpload() {
            var files = document.querySelector('input[type=file]').files;
            readmultifiles(files);
        }

        function lockSubmit() {
            console.log(111);
            var submitButton = document.getElementById('submit-button');

            submitButton.disabled = true;
            submitButton.innerText = 'Sender, Vent venligst ...';
        }

        function bindFirstName(firstNameInput) {
            var ownerFirstName = document.getElementById('owner_first_name');
            ownerFirstName.value = firstNameInput.value;
        }

        function bindLastName(lastNameInput) {
            var ownerLastName = document.getElementById('owner_last_name');
            ownerLastName.value = lastNameInput.value;
        }
    </script>
@endsection


