<div class="col-lg-4">
    <div class="list">
        <div class="list-group">
            <a href="#" class="list-group-item active cat-list">Menu</a>
            <a class="list-group-item cat-list" href="{{route('admin.users')}}">
                <i class="fa fa-users" aria-hidden="true"></i> {{__('admin.users')}}</a>
            <a class="list-group-item cat-list" href="{{route('admin.subscriptionTypes')}}"><i class="fa fa-address-card-o" aria-hidden="true"></i>
                {{__('admin.types')}}</a>
            <a class="list-group-item cat-list" href="{{route('admin.multy-message.create')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                {{__('admin.send_messages')}}</a>
            <a class="list-group-item cat-list" href="{{route('admin.informationPages')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i>
                {{__('admin.info')}}</a>
        </div>
    </div>
</div>