@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">Erhverv</h1>
    </div>
@endsection

@section('content')
    <section id="services" class="services">
        <div class="main-container container text-center">

            <div class="row">
                <div class="col-lg-12 services-holder">
                    <h3>Vælg et erhverv</h3>
                    <hr class="mint">

                    @foreach($project_categories as $category)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="{{route('projects.create', ['id' => $category->id])}}" class="project-category">
                                <div class="icon-circle">
                                    <div class="project-category-icon" style="background-image: url(../img/cat/{{$category->icon}});"></div>
                                </div>
                                <h4>{{strlen($category->category_name) > 20 ? substr($category->category_name, 0, 20) . "..." : $category->category_name}}</h4>
                            </a>
                        </div>
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">--}}
                        {{--<a href="{{route('catalog.category', ['categoryId' => $category->id])}}">--}}
                        {{--<div class="features category_icon_{{$category->id}}">--}}
                        {{--<h4>{{strlen($category->category_name) > 20 ? substr($category->category_name, 0, 20) . "..." : $category->category_name}}</h4>--}}
                        {{--</div><!-- /.features -->--}}
                        {{--<div class="spacer">--}}

                        {{--<p class="count">{{$category->users->count()}}--}}
                        {{--<span>{{__('catalog.freelancers')}}</span>--}}
                        {{--</p>--}}
                        {{--</div> <!-- /.spacer -->--}}
                        {{--<div class=" mint-hr"></div>--}}
                        {{--</a>--}}
                        {{--</div><!-- /.col-lg-12 -->--}}
                    @endforeach
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection