<?php

use Illuminate\Database\Seeder;


class CitiesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dk_regions = [
            'Nordsjælland',
            'København',
            'Stor København',
            'Midtsjælland',
            'Vestsjælland',
            'Sydsjælland',
            'Lolland-Falster',
            'Fyn',
            'Nordjylland',
            'Midtjylland',
            'Østjylland',
            'Vestjylland',
            'Sønderjylland',
            'Bornholm',
            'Andre øer'
        ];

        foreach ($dk_regions as $region) {
            DB::table('cities')->insert([
                'city_name' => $region,
            ]);
        }
    }
}
