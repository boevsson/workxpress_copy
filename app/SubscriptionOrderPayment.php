<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionOrderPayment extends Model
{
    protected $fillable = [
        'amount',
        'currency',
        'status',
        'orderid',
        'transactionid',
        'subscription_order_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
