@extends('layouts.master')

@section('inner_header')

    <div class="col-md-12">
        <h1>{{__('project.jobs')}}</h1>
    </div>

@endsection

@section('content')
    <div class="main-container container-fluid project-index">
        <div class="row-fluid">

            <div class="col-lg-3 filter-project">

                <div class="list">
                    <div class="list-group">

           <span class="list-group-item active cat-top">
            <em class="fa fa-fw fa-coffee text-white"></em>&nbsp;&nbsp;&nbsp;{{__('project.filters')}}
		   </span>
                        <form method="post" action="{{route('projects.filter')}}">
                            {{csrf_field()}}
                            <div class="list-group-item cat-list">
                                <div class="form-group ">
                                    <select onchange="getSubCategories()" name="project_category_id"
                                            class="form-control" id="projectCategory">
                                        <option value="0"> {{__('project.choose_category')}}</option>
                                        @foreach($project_categories as $projectCategory)
                                            <option value="{{$projectCategory->id}}" {{(isset($filterFields['project_category_id']) && $projectCategory->id ==
                                    $filterFields['project_category_id']) ?
                                    'selected' :
                                    ''}}>{{$projectCategory->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="project_sub_category_id" class="form-control" id="subCategorySelect">

                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="city_id" class="form-control" id="projectCity">
                                        <option value="0">{{__('project.choose_city')}}</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}" {{(isset($filterFields['city_id']) && $city->id == $filterFields['city_id']) ?
                                    'selected' :
                                    ''}}>{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="deadline" class="form-control" id="projectDeadline">
                                        <option value="0">{{__('project.select_deadline')}}</option>
                                        <option value="as_soon_as_possible" {{(isset($filterFields['deadline']) && $filterFields['deadline'] ==
                                'as_soon_as_possible') ?
                                'selected' : ''}}>
                                            {{__('project.as_soon_as_possible')}}
                                        </option>
                                        <option value="within_one_month" {{(isset($filterFields['deadline']) && $filterFields['deadline'] ==
                                'within_one_month') ?
                                'selected' : ''}}>
                                            {{__('project.within_one_month')}}
                                        </option>
                                        <option value="within_two_months" {{(isset($filterFields['deadline']) && $filterFields['deadline'] ==
                                'within_two_months') ?
                                'selected' : ''}}>
                                            {{__('project.within_two_months')}}
                                        </option>
                                        <option value="within_three_months" {{(isset($filterFields['deadline']) && $filterFields['deadline'] ==
                                'within_three_months') ?
                                'selected' : ''}}>
                                            {{__('project.within_three_months')}}
                                        </option>
                                        <option value="more_than_three_months" {{(isset($filterFields['deadline']) && $filterFields['deadline'] ==
                                'more_than_three_months') ?
                                'selected' : ''}}>
                                            {{__('project.more_than_three_months')}}
                                        </option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input value="{{(isset($filterFields['budget'])) ?
                                $filterFields['budget'] : ''}}" name="budget" type="text" class="form-control input-sm"
                                                   id="projectBudget"
                                                   placeholder="Min Budget">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <button type="submit" class="btn btn-primary btn-sm pull-left btn-filter">
                                                Søg
                                            </button>
                                            <a href="{{route('projects.index')}}" style="margin-right: 15px;"
                                               type="submit" class="btn
                                        btn-danger btn-clear btn-sm
                                    pull-right">{{__('project.clear')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div><!-- /.list-group -->
                </div><!-- /.list -->

            </div><!-- /.col-lg-4 -->
            <div class="col-lg-9 white">

                <form action="{{route('projects.search')}}" method="post" class="list-search revealOnScroll"
                      data-animation="fadeInDown"
                      data-timeout="200">
                    {{csrf_field()}}
                    <button><i class="fa fa-search"></i></button>
                    <input type="text" class="form-control" name="title" placeholder="Opgave titel"
                           value="{{(isset($title) ? $title : '')}}"/>
                    <div class="clearfix"></div>
                </form>
                @include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
                @if(!count(Auth::user()->projectCategories))
                    <h4>For at se opgaver, skal du først angive hvad du laver i <a href="{{route('profile')}}">Profil
                            indstillinger</a></h4>
                @elseif(!count($projects))

                    <h3>{{__('project.job_not_found')}}</h3>
                @endif
                @foreach($projects as $project)
                    <div class="job">
                        @if(Auth::user()->admin)
                            <form class="delete-project"
                                  onSubmit="return confirm('Er du sikker på, at du vil slette denne opgave?');"
                                  action="{{route('projects.destroy', ['id' => $project->id])}}"
                                  method="post">
                                {{csrf_field()}}
                                <input type="hidden" value="true" name="redirectToProjectsIndex">
                                <button type="submit" title="Slet opgave"
                                        class="pull-right kafe-btn kafe-btn-mint-small"><i
                                            class="fa fa-trash"
                                            aria-hidden="true"></i>
                                </button>
                            </form>
                        @endif
                        <div class="row top-sec">
                            <div class="col-lg-12">
                                <div class="col-lg-2 col-xs-12">
                                    <a href="{{route('profile.show', ['userId' => $project->user->id])}}">
                                        <img class="img-responsive img-circle"
                                             src="{{ URL::to('images/profile/'. $project->user->image) }}" alt="">
                                    </a>
                                </div><!-- /.col-lg-2 -->
                                <div class="col-lg-10 col-xs-12">
                                    <h4>
                                        <a href="{{route('projects.show', ['id' => $project->id])}}">{{$project->title}}</a>
                                    </h4>
                                    @if($project->user->businessSettings)
                                        <h5>
                                            <a href="{{route('profile.show', ['id' => $project->user->id])}}">{{$project->user->businessSettings->name}}</a>
                                        </h5>
                                    @else
                                        <h5>
                                            <a href="{{route('profile.show', ['id' => $project->user->id])}}">{{$project->user->first_name}}
                                                {{$project->user->last_name}}</a>
                                        </h5>
                                    @endif
                                </div><!-- /.col-lg-10 -->
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->

                        <div class="row mid-sec">
                            <div class="col-lg-12">
                                <div class="col-lg-12">
                                    <hr class="small-hr">
                                    <p style="white-space: pre-wrap; margin-bottom: 0;">{{$project->description}}</p>
                                </div><!-- /.col-lg-12 -->
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->

                        <div class="row bottom-sec">
                            <div class="col-lg-12">

                                <div class="col-lg-12">
                                    <hr class="small-hr">
                                </div>

                                <div class="col-lg-2">
                                    <h5> {{__('project.posted')}}</h5>
                                    <p>{{ $project->created_at->diffForHumans()}}</p>
                                </div>
                                <div class="col-lg-2">
                                    <h5> {{__('project.location')}} </h5>
                                    <p><i class="fa fa-map-marker"></i> {{$project->city->city_name}}</p>
                                </div>
                                <div class="col-lg-2">
                                    <h5> {{__('project.budget')}} </h5>
                                    <p>{{$project->budget}}kr</p>
                                </div>
                                <div class="col-lg-2">
                                    <h5> {{__('project.category')}}</h5>
                                    <p>{{$project->project_category->category_name}}</p>
                                </div>
                                <div class="col-lg-2">
                                    <h5> {{__('project.deadline')}} </h5>
                                    <p>{{$project->deadline}}</p>
                                </div>
                                <div class="col-lg-2">
                                    <h5> {{__('project.applicants')}}</h5>
                                    <p>{{$project->proposals->count()}}</p>
                                </div>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->

                    </div><!-- /.job -->
                @endforeach

                <div class="page text-center">
                    {{ $projects->links() }}
                </div><!-- /.page -->

            </div><!-- /.col-lg-8 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </section><!-- /section -->
@endsection

@section('scripts')
    <script>
        function getSubCategories() {
            var subCategorySelect = document.getElementById('subCategorySelect');
            var select = document.getElementById('projectCategory');

                    @if(isset($filterFields['project_sub_category_id']))
            var projectCategoryId = {{$filterFields['project_sub_category_id']}};
                    @else
            var projectCategoryId = null;
            @endif

            $('#subCategorySelect').empty();

            //check for sub categories
            if (select.value > 0) {
                axios.get('/catalog/get-sub-categories/' + select.value)
                    .then(function (response) {
                        if (response.data.length) {
                            for (var i = 0; i < response.data.length; i++) {
                                if (projectCategoryId == response.data[i].id) {
                                    $('#subCategorySelect').prepend(
                                        '<option selected value=' + response.data[i].id + ' class="project-image-holder">' +
                                        response.data[i].category_name
                                        +
                                        '</option>'
                                    );
                                } else {
                                    $('#subCategorySelect').prepend(
                                        '<option value=' + response.data[i].id + ' class="project-image-holder">' +
                                        response.data[i].category_name
                                        +
                                        '</option>'
                                    );
                                }

                            }
                            subCategorySelect.style = "display:block;";
                        } else {
                            subCategorySelect.style = "display:none;";
                            $('#subCategorySelect').empty();
                        }
                    })
                    .catch(function (error) {
                    });
            } else {
                //hide the subcategories <select>
                subCategorySelect.style = "display:none;";
                $('#subCategorySelect').empty();
            }
        }

        getSubCategories();
    </script>
@endsection