<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Services;
use App\SubscriptionOrder;
use App\SubscriptionType;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function users()
    {
        $users = User::paginate(15);

        $subscription_types = SubscriptionType::all();

        return view('admin.users', compact('users', 'subscription_types'));
    }

    public function changeSubscription(Request $request, $userId)
    {
        $user = User::find($userId);
        $user->subscription_type_id = $request->get('subscription_type_id');
        $user->save();

        $subscription_order = SubscriptionOrder::where('status', 'active')->where('user_id', $user->id)->first();

        $newExpirationDate = new \DateTime();
        $newExpirationDate = $newExpirationDate->add(new \DateInterval('P30D'));

        if ($subscription_order) {

            if ($request->get('subscription_type_id')) {

                $subscription_order->type = 'free';
                $subscription_order->expire_at = $newExpirationDate;
                $subscription_order->save();
            } else {

                //If the subscription is not a test one
                if ($subscription_order->type == 'payed') {

                    $response = Services::epayDeleteSubscription($subscription_order);

                    if ($response->deletesubscriptionResult) {

                        $this->removeSubscription($subscription_order, $user);
                    } else {

                        return back()->withErrors(['response' => __('admin.subs_cant_canceled')]);
                    }
                } else if ($subscription_order->type == 'free') {

                    $this->removeSubscription($subscription_order, $user);
                }
            }
        } else {

            $subscription_order = SubscriptionOrder::create([
                'date' => '',
                'time' => '',
                'txnfee' => '',
                'paymenttype' => '',
                'cardno' => '',
                'hash' => '',
                'status' => 'active',
                'type' => 'free',
                'orderid' => '',
                'subscriptionid' => '',
                'user_id' => $user->id,
                'expire_at' => $newExpirationDate
            ]);
        }

        return redirect()->route('admin.users')->with('status', __('admin.subs_type_updated', ['name' => $user->first_name]));
    }

    public function removeSubscription($subscription_order, $user)
    {
        $subscription_order->status = __('admin.canceled');
        $subscription_order->save();

        $user->subscription_type_id = null;
        $user->save();
    }

    public function banUser($id)
    {
        $user = User::findOrFail($id);

        if ($user->suspended) {
            //remove the ban
            $user->suspended = null;
            $user->save();
            return redirect()->back()->with('status', $user->first_name . ' ' . $user->last_name . __('admin.is_unbanned'));
        } else {
            $user->suspended = true;
            $user->save();
            return redirect()->back()->with('status', $user->first_name . ' ' . $user->last_name . __('admin.is_banned'));
        }

    }

    public function searchUser(Request $request)
    {
        $id_criteria = $request->get('user_id');
        $email_criteria = $request->get('email');

        $users = User::id($id_criteria)->email($email_criteria)->paginate(15);
        $subscription_types = SubscriptionType::all();

        return view('admin.users', compact('users', 'subscription_types', 'id_criteria', 'email_criteria'));
    }
}
