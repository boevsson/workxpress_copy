
<form class="message-form" action="{{ route('messages.update', $thread->id) }}" method="post">
    <h3>{{__('messenger.add_msg')}}</h3>
{{ method_field('put') }}
{{ csrf_field() }}

<!-- Message Form Input -->
    <div class="form-group">
        <textarea name="message" class="form-control">{{ old('message') }}</textarea>
    </div>

    <!-- Submit Form Input -->
    <div class="form-group">
        <button type="submit" class="kafe-btn kafe-btn-mint full-width">{{__('messenger.submit')}}</button>
    </div>
</form>