@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('proposal.view_proposal')}}</h1>
    </div>
@endsection

@section('content')
    <section class="jobpost">
        <div class="main-container container proposal-show">
            <div class="row">
                <div class="col-lg-8 col-md-8 white">
                    @include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row post-top-sec">
                        <div class="col-lg-3">
                            <h5> {{__('proposal.posted')}} </h5>
                            <p> {{ $proposal->created_at->diffForHumans()}}</p>
                        </div>
                        <div class="col-lg-3">
                            <h5> {{__('proposal.bid')}} </h5>
                            <p>{{ $proposal->bid}}kr</p>
                        </div>

                        <div class="col-lg-3 choose-proposal">
                            @if(Auth::check())
                                @if($proposal->project->picked_proposal_id == $proposal->id)
                                    <p>Dette forslag er valgt</p>
                                @elseif($proposal->project->user_id == Auth::id())
                                    <form method="POST" action="{{route('project.chooseProposal')}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="project_id" value="{{$proposal->project->id}}">
                                        <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                                        <button type="submit" class="kafe-btn kafe-btn-mint-small"><i
                                                    class="fa fa-hand-pointer-o"
                                                    aria-hidden="true"></i>
                                            {{__('proposal.choose_proposal')}}
                                        </button>
                                    </form>
                                @endif
                            @endif
                        </div>
                        @if($proposal->user_id == Auth::id())
                            <div class="col-lg-3">
                                <a href="{{route('profile.proposals.edit', ['id' => $proposal->id])}}" class="kafe-btn kafe-btn-mint-small
                                        pull-right
"><i
                                            class="fa
                                        fa-pencil-square-o"
                                            aria-hidden="true"></i>
                                    {{__('user.edit_proposal')}}</a>
                            </div>
                        @endif
                        <div class="col-lg-12">
                            <hr class="small-hr">
                        </div> <!-- /.col-lg-12 -->
                    </div><!-- /.row -->

                    <div class="post-bottom-sec">
                        <h4>{{__('proposal.task_desc')}}</h4>
                        <p style="white-space: pre-wrap; margin-bottom: 0;">{{$proposal->content}}</p>
                    </div><!-- /.post-bottom-sec --> <br>

                    <div class="messages">
                        <h4>Beskeder</h4>
                        <div class="messages-list" id="messages-list">
                            @foreach($proposal->messages as $message)
                                <div class="media {{($message->user->id == Auth::id()) ? 'pull-left' : 'pull-right'}}">
                                    <a class=" {{($message->user->id == Auth::id()) ? 'pull-left' : 'pull-right'}}"
                                       href="#">
                                        <img src="{{ URL::to('images/profile/'. $message->user->image) }}"
                                             alt="{{ $message->user->first_name }} {{ $message->user->last_name }}"
                                             class="img-circle">
                                    </a>
                                    <div class="media-body">
                                        <h5 class="media-heading">
                                            @if($message->user->businessSettings)
                                                {{$message->user->businessSettings->name}}
                                            @else
                                                {{$message->user->first_name}} {{$message->user->last_name}}
                                            @endif
                                        </h5>
                                        <p>{{ $message->message }}</p>
                                        <div class="text-muted">
                                            <small>{{__('proposal.posted')}} {{ $message->created_at->diffForHumans() }}</small>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <form method="POST" class="white-form" action="{{route('proposal.storeMessage')}}">
                            {{csrf_field()}}
                            <textarea name="message" class="form-control proposal-textarea" placeholder="Besked"
                                      rows="4"></textarea>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                            @endif
                            <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                            <button type="submit"
                                    class="kafe-btn kafe-btn-mint full-width"> Send
                            </button>
                        </form>
                    </div>

                </div><!-- /.col-lg-8 -->

                <div class="col-lg-4 col-md-4">

                    <div class="panel user revealOnScroll" data-animation="slideInUp" data-timeout="200">
                        <div class="row text-center">
                            <a href="{{route('profile.show', ['id' => $proposal->user->id])}}">
                                <img src="{{asset('img/bg/2.jpg')}}" class="img-responsive panel-img" alt="">

                                <div class="col-xs-12 user-avatar">
                                    <img src="{{ URL::to('images/profile/'. $proposal->user->image) }}" alt="Image"
                                         class="img-thumbnail
                                    img-responsive">
                                    @if($proposal->user->businessSettings)
                                        <h4>{{$proposal->user->businessSettings->name}}</h4>
                                    @else
                                        <h4>{{$proposal->user->first_name}} {{$proposal->user->last_name}}</h4>
                                    @endif
                                </div><!-- /.col-xs-12 -->
                            </a>
                        </div><!-- /.row -->

                        <div class="list-group">
                            <div class="list-group-item">&nbsp;&nbsp;&nbsp;{{__('proposal.rating')}}
                                <span class="rating-icon">
                                @if($proposal->user->avg_rating == 1)
                                        <img src="{{asset('img/rating_icons/1.png')}}">
                                    @elseif($proposal->user->avg_rating == 2)
                                        <img src="{{asset('img/rating_icons/2.png')}}">
                                    @elseif($proposal->user->avg_rating == 0)
                                        <span>{{__('proposal.no_rating')}}</span>
                                    @else
                                        <img src="{{asset('img/rating_icons/3.png')}}">
                                    @endif
                            </span>
                            </div><!-- /.list-group-item -->
                            <div class="list-group-item">&nbsp;&nbsp;&nbsp;{{__('proposal.completed_tasks')}}
                                <span class="badge">{{$proposal->user->ratings->count()}}</span>
                            </div><!-- /.list-group-item -->
                        </div><!-- /.list-group -->

                    </div><!-- /.list-group-item -->
                </div><!-- /.col-lg-4 -->

            </div><!-- /.row-->
        </div><!-- /.container -->
    </section><!-- /section -->
@endsection
@section('scripts')
    <script>
        window.onload = function () {
            var objDiv = document.getElementById("messages-list");
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    </script>
@endsection