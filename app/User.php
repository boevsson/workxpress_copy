<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    use Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password'
    ];

    protected $appends = ['avg_rating'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function subscription()
    {
        return $this->belongsTo('App\SubscriptionType', 'subscription_type_id');
    }

    public function subscriptionOrders()
    {
        return $this->hasMany('App\SubscriptionOrder', 'user_id');
    }

    public function projectCategories()
    {
        return $this->belongsToMany('App\ProjectCategory');
    }

    public function cities()
    {
        return $this->belongsToMany('App\City');
    }

    public function projects()
    {
        return $this->hasMany('App\Project', 'user_id');
    }

    public function proposals()
    {
        return $this->hasMany('App\ProjectProposal', 'user_id');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating', 'user_id');
    }

    public function businessSettings()
    {
        return $this->hasOne('App\BusinessSetting', 'user_id');
    }

    public function getAvgRatingAttribute()
    {
        $ratings = $this->ratings()->lastTwoMonths()->get();

        if ($ratings->count() > 0) {

            $ratingsIds = [];

            foreach ($ratings as $rating) {
                $ratingsIds[] = $rating->rating;
            }

            $score_count = count($ratingsIds);
            $score_sum = array_sum($ratingsIds);


            return ucfirst(round($score_sum / $score_count));

        } else {

            return ucfirst(0);
        }
    }

    public function getLastNotificationsAttribute()
    {
        return Auth()->user()->notifications()->paginate(5);
    }

    public function scopeUserId($query, $userId)
    {
        if ($userId) {
            return $query->where('user_id', $userId);
        }
    }

    public function scopeSubscribedForNotifications($query)
    {
        return $query->where('receive_email_notifications', 1);
    }

    public function scopeId($query, $userId)
    {
        if ($userId) {
            return $query->where('id', $userId);
        }
    }

    public function scopeEmail($query, $email)
    {
        if ($email) {
            return $query->where('email', $email);
        }
    }

    public function getOnGoingProjectsAttribute()
    {
        //get all picked projects
        $pickedProjects = Project::notCompleted()->pickedProposal()->get();

        $onGoingProjects = [];

        foreach ($pickedProjects as $project) {
            if ($project->executor->user_id == Auth::id()) {
                $onGoingProjects[] = $project;
            }
        }

        return $onGoingProjects;
    }

    public function getCompletedProjectsAttribute()
    {
        return Project::completedBy($this->id)->paginate(5);
    }

//    public function scopeFromCity($query, $cityId)
//    {
//        return $query->whereHas('App\City', function ($query, $cityId) {
//            $query->where('id', $cityId);
//        });
//    }
}
