<?php

namespace App\Http\Controllers;

use App\BusinessSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BusinessSettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $user = Auth::user();

        $businessSettings = $user->businessSettings;

        return view('user.business', ['user' => $user, 'businessSettings' => $businessSettings]);
    }

    public function update(Request $request)
    {
        $messages = [
            'cvr.required' => __('validate.cvr.required'),
            'cvr.max:255' => __('validate.cvr.max:255'),
            'name.required' => __('validate.name.required'),
            'name.max:255' => __('validate.name.max:255'),
            'owner_first_name.required' => __('validate.owner_first_name.required'),
            'owner_first_name.max:255' => __('validate.owner_first_name.max:255'),
            'owner_last_name.required' => __('validate.owner_last_name.required'),
            'owner_last_name.max:255' => __('validate.owner_last_name.max:255'),
            'address.max:255' => __('validate.address.required'),
            'address.max:255' => __('validate.address.max:255'),
            'email.max:255' => __('validate.email.required'),
            'email.max:255' => __('validate.email.max:255'),
            'phone.max:255' => __('validate.phone.required'),
            'phone.max:255' => __('validate.phone.max:255'),
        ];

        $validator = Validator::make($request->all(), [
            'cvr' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'owner_first_name' => 'required|string|max:255',
            'owner_last_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'phone' => 'required|string|max:15',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();

        $businessSettings = BusinessSetting::where('user_id', $user->id)->first();

        if (!$businessSettings) {

            $businessSettings = new BusinessSetting();
        }

        $businessSettings->cvr = $request->get('cvr');
        $businessSettings->name = $request->get('name');
        $businessSettings->owner_first_name = $request->get('owner_first_name');
        $businessSettings->owner_last_name = $request->get('owner_last_name');
        $businessSettings->address = $request->get('address');
        $businessSettings->email = $request->get('email');
        $businessSettings->phone = $request->get('phone');
        $businessSettings->user_id = $user->id;

        $businessSettings->save();

        return redirect()->back()->with('status', __('project.business_info'));
    }
}
