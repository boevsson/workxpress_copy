@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('project.edit_task')}}</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container edit-blade">
        <div class="col-md-12 white-2">
            @include('layouts._shared.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
            @if($project->picked_proposal_id)
                <h3>{{__('project.choose_proposal_task')}}</h3>
                <div class="job">
                    <div class="row top-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-2 col-xs-12">
                                <a href="{{route('profile.show', ['userId' => $project->executor->user->id])}}">
                                    <img class="img-responsive img-circle"
                                         src="{{ URL::to('images/profile/'. $project->executor->user->image) }}"
                                         alt="">
                                </a>
                            </div><!-- /.col-lg-2 -->
                            <div class="col-lg-10 col-xs-12">
                                @if($project->executor->user->businessSettings)
                                    <h4>
                                        <a href="{{route('profile.show', ['userId' => $project->executor->user->id])}}">{{$project->executor->user->businessSettings->name}}</a>
                                    </h4>
                                @else
                                    <h4>
                                        <a href="{{route('profile.show', ['userId' => $project->executor->user->id])}}">{{$project->executor->user->first_name}} {{$project->executor->user->last_name}}</a>
                                    </h4>
                                @endif
                            </div><!-- /.col-lg-10 -->

                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->

                    <div class="row mid-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <hr class="small-hr">
                                <p>{{strlen($project->executor->content) > 300 ? substr($project->executor->content, 0, 300) . "..." : $project->executor->content}}</p>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->

                    <div class="row bottom-sec">
                        <div class="col-lg-12">

                            <div class="col-lg-12">
                                <hr class="small-hr">
                            </div>

                            <div class="col-lg-2">
                                <h5> {{__('project.posted')}} </h5>
                                <p> {{ $project->executor->created_at->diffForHumans()}}</p>
                            </div>
                            <div class="col-lg-2">
                                <h5> {{__('project.bid')}} </h5>
                                <p>{{ $project->executor->bid}}kr</p>
                            </div>
                            <div class="col-lg-2">
                                <h5> {{__('project.ratings')}} ({{$project->executor->user->ratings->count()}})</h5>
                                <p>
                                    <span class="rating-icon">
                                @if($project->executor->user->avg_rating == 1)
                                            <img src="{{asset('img/rating_icons/1.png')}}">
                                        @elseif($project->executor->user->avg_rating == 2)
                                            <img src="{{asset('img/rating_icons/2.png')}}">
                                        @elseif($project->executor->user->avg_rating == 0)
                                            <span> {{__('project.no_rating_yet')}}</span>
                                        @else
                                            <img src="{{asset('img/rating_icons/3.png')}}">
                                        @endif
                            </span>
                                </p>
                            </div>
                            <div class="col-lg-4">
                                <a href="{{route('proposal.show', ['id' => $project->executor->id])}}"
                                   class="kafe-btn kafe-btn-mint-small"><i
                                            class="fa
                                        fa-align-right"></i> {{__('project.view_proposal')}}</a>
                                <form style="float: right;" method="POST" action="{{route('project.cancelProposal')}}"
                                      onSubmit="return confirm('Are' +
                                 ' you sure you wish ' +
                                 'to' +
                                 ' cancel this proposal?');">
                                    {{csrf_field()}}
                                    <input type="hidden" name="project_id" value="{{$project->id}}">
                                    <button class="kafe-btn kafe-btn-mint-small">  {{__('project.cancel')}}</button>
                                </form>
                            </div>
                            <div class="col-lg-2">
                                @if(Auth::user()->id !=$project->executor->user->id)
                                    <a href="{{route('messages.create', ['userId' => $project->executor->user->id])}}"
                                       class="kafe-btn kafe-btn-mint-small"><i class="fa fa-paper-plane"
                                                                               aria-hidden="true"></i> {{__('project.send_message')}}
                                    </a>
                                @endif
                            </div>

                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->

                </div><!-- /.job -->
                <hr class="small-hr">
            @endif
            <form onsubmit="lockSubmit()" method="POST" enctype="multipart/form-data" class="white-form" action="{{route('projects.update', ['id' =>
            $project->id])}}">
                @include('projects.form', ['project' => $project])
            </form>
        </div>
    </div>
@endsection