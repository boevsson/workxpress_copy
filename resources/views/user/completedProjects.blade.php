@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">{{__('user.your_completed_projects')}}</h1>
    </div>
@endsection

@section('content')
    <section class="overview" id="overview">
        <div class="main-container container">
            <div class="row">
                @include('user.head')
                <div class="col-lg-8 white-2">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-hover table-project">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Titel</th>
                            <th>Beskrivelse</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Auth()->user()->completedProjects as $project)
                            <tr>
                                <td>{{$project->id}}</td>
                                <td>{{$project->title}}</td>
                                <td>{{strlen($project->description) > 50 ? substr($project->description, 0, 50) . "..." : $project->description}}</td>
                                <td><a href="{{route('projects.show', ['id' => $project->id])}}"
                                       class="btn btn-primary btn-xs">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="page text-center">
                        {{ Auth()->user()->completedProjects->links() }}
                    </div><!-- /.page -->
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- End section-->
@endsection