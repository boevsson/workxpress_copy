<?php

return [
    'view_proposal' => 'Se forslag',
    'posted' => 'Sendt',
    'bid' => 'Bud',
    'choose_proposal' => 'Vælg forslag',
    'send' => 'Send',
    'task_desc' => 'Opgavebeskrivelse',
    'rating' => 'Bedømmelse',
    'no_rating' => 'Ingen bedømmelse endnu',
    'completed_tasks' => 'Afsluttede opgaver',
];