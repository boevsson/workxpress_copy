<?php

namespace App\Http\Controllers;

use App\City;
use App\ProjectCategory;
use App\User;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index()
    {
        $project_categories = ProjectCategory::topLevel()->orderBy('category_name', 'asc')->get();

        return view('catalog.index', compact('project_categories'));
    }

    public function category($categoryId)
    {
        $category = ProjectCategory::findOrFail($categoryId);
        $cities = City::all();
        $project_categories = ProjectCategory::all();

        $users = $category->users()->orderBy('subscription_type_id', 'desc')->paginate(10);

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.catalog'),
                'link' => route('catalog.index')
            ],
            [
                'name' => $category->category_name,
                'link' => ''
            ]
        ];

        return view('catalog.category', compact('category', 'cities', 'project_categories', 'categoryId', 'users', 'breadcrumbs'));
    }

    public function filter(Request $request)
    {

        $cityId = $request->get('city_id');

        if ($request->get('project_sub_category_id')) {
            $project_category_id = $request->get('project_sub_category_id');
        } else {
            $project_category_id = $request->get('project_category_id');
        }

        $categoryId = $project_category_id;
        $category = ProjectCategory::findOrFail($project_category_id);
        $cities = City::all();
        $project_categories = ProjectCategory::topLevel()->get();

        if ($_REQUEST['city_id']) {
            $users = User::whereHas('cities', function ($query) {
                $query->where('city_id', $_REQUEST['city_id']);
            })->whereHas('projectCategories', function ($query) {
                $query->where('project_category_id', $_REQUEST['project_category_id']);
            })->paginate(10);
        } else {
            $users = $category->users()->paginate(10);
        }

        //breadcrumbs
        $breadcrumbs = [
            [
                'name' => __('project.home'),
                'link' => url('/')
            ],
            [
                'name' => __('project.catalog'),
                'link' => route('catalog.index')
            ],
            [
                'name' => $category->category_name,
                'link' => ''
            ]
        ];

        return view('catalog.category', compact('category', 'cities', 'project_categories', 'categoryId', 'users', 'cityId', 'breadcrumbs'));
    }

    public function getSubCategories($categoryId)
    {
        $categories = ProjectCategory::children($categoryId)->get();

        return json_encode($categories);
    }

    public function searchCategory($name)
    {
        $categories = ProjectCategory::topLevel()->name($name)->get();

        return json_encode($categories);
    }
}
