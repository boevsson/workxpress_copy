@extends('layouts.master')

@section('inner_header')
    <div class="col-md-12">
        <h1 style="text-align: center">Dashboard</h1>
    </div>
@endsection

@section('content')
    <div class="main-container container admin-container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-lg-8 white-2">
                <h3>{{__('admin.welcome')}}</h3>
            </div>
        </div>
    </div>
@endsection
